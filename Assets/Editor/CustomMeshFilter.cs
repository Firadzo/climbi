﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SpriteRenderer))]
[CanEditMultipleObjects]
public class CustomMeshFilter : Editor {

	public SpriteRenderer Target
	{
		get {  return (SpriteRenderer)target; }
	}

	public Transform transform
	{
		get{ return Target.transform; }
	}
	[SerializeField]
	static bool optionVisible = false;

	public override void OnInspectorGUI() {
		DrawDefaultInspector ();
		GUILayout.Space ( 5f );
		if(GUILayout.Button( ( optionVisible? "Hide":"More" ), GUILayout.ExpandWidth(true), GUILayout.ExpandWidth(true))) 
		{
			optionVisible = !optionVisible;
		}
		if ( optionVisible ) {
			if (GUILayout.Button ("SaveMesh", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				MyEditorUtilities.SaveMesh ( Target, EditorUtility.SaveFilePanel ("Save", "Assets/", Target.sprite.name, "asset"));
			}
			if (GUILayout.Button ("SaveMeshWithScale", GUILayout.ExpandWidth (true), GUILayout.ExpandWidth (true))) {
				MyEditorUtilities.SaveMeshWithScale (Target, EditorUtility.SaveFilePanel ("Save", "Assets/", Target.sprite.name, "asset"), transform);
			}
		}
	}
}

public static class MyEditorUtilities
{
	/// <summary>
	/// Сохранение меша как ассета внутри проекта, если путь задан вне папки ассетов то сохранение не сработает
	/// </summary>
	/// <param name="source">Source.</param>
	/// <param name="path">Path.</param>
	public static void SaveMesh( Mesh source, string path )
	{
		if ( source == null || string.IsNullOrEmpty (path) || !path.Contains("Assets" ) ) {
			return;
		}
		path = path.Substring ( path.IndexOf("Assets") );
		Mesh meshNew = source;
		string meshName = path.Split ( '/' )[ path.Split ( '/' ).Length - 1 ].Replace ( ".asset","");
		Debug.Log ( meshName );
		meshNew.name = meshName;
		Debug.Log ( path );
		AssetDatabase.CreateAsset ( meshNew, path  );
		AssetDatabase.SaveAssets();
	}

	public static void SaveMesh( SpriteRenderer spriteRenderer, string path )
	{
		if ( spriteRenderer == null || string.IsNullOrEmpty (path) || !path.Contains("Assets" ) ) {
			return;
		}
		SaveMesh ( GetMeshFormSprite( spriteRenderer.sprite ), path );
	}

	private static Mesh GetMeshFormSprite( Sprite sprite )
	{
		Mesh source = new Mesh ();
		source.name = sprite.name;
		
		Vector3[] vertices = new Vector3[ sprite.vertices.Length ];
		for (int i = 0; i < vertices.Length; i++) {
			vertices[ i ] = (Vector3)sprite.vertices[ i ];
		}
		source.vertices = vertices;
		
		source.uv = sprite.uv;
		
		int[] triangles = new int[ sprite.triangles.Length ];
		for (int i = 0; i < triangles.Length; i++) {
			triangles[ i ] = (int)sprite.triangles[ i ];
		}
		source.triangles = triangles;
		return source;
	}

	public static void SaveMeshWithScale( SpriteRenderer spriteRenderer, string path, Transform transform )
	{
		SaveMeshWithScale ( GetMeshFormSprite( spriteRenderer.sprite ), path, transform );
	}

	public static void SaveMeshWithScale( Mesh source, string path, Transform transform )
	{
		if ( source == null || string.IsNullOrEmpty (path) || !path.Contains("Assets" ) ) {
			return;
		}
		Debug.Log ( transform.localScale );
		Vector3 scale = transform.localScale;
		transform.localScale = Vector3.one;
		Mesh meshNew = source;
		Vector3[] vertices = meshNew.vertices;
		for( int j = 0; j < vertices.Length; j++ )
		{
			Debug.Log( vertices[ j ] +" before " );
			vertices[ j ].x *= scale.x;
			vertices[ j ].y *= scale.y;
			vertices[ j ].z *= scale.z;
			Debug.Log( vertices[ j ] );
		}
		meshNew.vertices = vertices;
		SaveMesh ( meshNew, path );
	}
	
}


