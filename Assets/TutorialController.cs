﻿using UnityEngine;
using System.Collections;

public class TutorialController : MonoBehaviour {

	public 	Animator	tutorialAnim;
	private Transform	tutorialTransf;
	public 	string		showState;

	private bool 		checkForDisable;
	private GameObject obj;

	void Awake()
	{
		obj = gameObject;
		tutorialTransf = tutorialAnim.transform;
	}

	public void ShowTutorial()
	{
		if( obj == null )
		{
			Awake();
		}
		tutorialTransf.localPosition = Vector3.zero;
		obj.SetActive( true );
		tutorialAnim.Play( showState ,0 ,0 );
	}

	public void HideTutorial()
	{
		obj.SetActive( false );
	}
}
