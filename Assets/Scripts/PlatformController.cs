﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlatformParams
{
	public 	Sprite	sprite;
	public	Vector2	size;
	public 	float 	chance;
}

public class PlatformController : PlatformBase {
	public SpikesController	spikes;
	public PlatformParams[]	plaftformParams;

	override public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		base.Spawn( upBorder, botBorder, minXPos, maxXPos );
		if( spikes != null )
		{
			spikes.Init( this );
		}
	}

	override public void PrepareForSpawn()
	{
		List<PlatformParams>	filtered = new List<PlatformParams>();
		float value = Random.Range( 0, 1f );
		for( int i = 0; i < plaftformParams.Length; i++ )
		{
			if( value <= plaftformParams[ i ].chance )
			{
				filtered.Add( plaftformParams[ i ] );
			}
		}
		PlatformParams selected = filtered[ Random.Range( 0, filtered.Count ) ];
		spriteRenderer.sprite = selected.sprite;
		boxCollider2D.size = selected.size;
		platformSize = selected.size / 2f;
		occupiedSpaceY = platformSize.y;	
	}
}
