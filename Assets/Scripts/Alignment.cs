﻿using UnityEngine;
using System.Collections;

public class Alignment : MonoBehaviour {

	private TextMesh 	textMesh;
	private Transform	transf;
	public 	float		addToWidth;
	public	float		letterWidth;

	void OnEnable()
	{
		if( textMesh == null )
		{
			textMesh = GetComponentInChildren<TextMesh>();
		}
		if( transf == null ){ transf = transform; }
		Vector3 tmp = transf.localPosition;
		tmp.x = -( addToWidth + ( textMesh.text.Length / 2f ) * letterWidth );
		transf.localPosition = tmp;
	}
}
