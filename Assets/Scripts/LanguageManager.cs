﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class TextObject
{
	#if UNITY_EDITOR
	[HideInInspector]
	public string name;
	#endif
	public 	TextMesh 			textMesh;
	public	LanguageText[]		languagesText;	

	public void SetLanguage( SystemLanguage language )
	{
		for( int i = 0; i < languagesText.Length; i++ )
		{
			if( languagesText[ i ].language == language )
			{
				languagesText[ i ].SetText( textMesh );
				break;
			}
		}
	}
}

[System.Serializable]
public class LanguageText
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public	SystemLanguage		language;
	public	string[]			lines;
	public	float				characterSize;

	public void SetText( TextMesh textObj )
	{
		if( characterSize != 0 )
		{
			textObj.characterSize = characterSize;
		}
		string s = "";
		for( int i = 0; i < lines.Length; i++ )
		{
			s += lines[ i ];
			if( i != lines.Length - 1 )
			{
				s += "\r\n";
			}
		}
		textObj.text = s;
	}
}


public class LanguageManager : MonoBehaviour {

	public TextObject[]		textObjects;
	public SystemLanguage	defaultLanguage;
#if UNITY_EDITOR
	public SystemLanguage	sysLanguage;
	public	static SystemLanguage	systemLanguage;
#endif
	// Use this for initialization
	void Awake () {
#if UNITY_EDITOR
		systemLanguage = sysLanguage;
		if( sysLanguage != defaultLanguage )
		{
			for( int i = 0; i < textObjects.Length; i++ )
			{
				textObjects[ i ].SetLanguage( sysLanguage );
			}
		}

#else
		if( Application.systemLanguage != defaultLanguage )
		{
			for( int i = 0; i < textObjects.Length; i++ )
			{
				textObjects[ i ].SetLanguage( Application.systemLanguage );
			}
		}
#endif
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		for( int i = 0; i < textObjects.Length; i++ )
		{
			//if( string.IsNullOrEmpty( textObjects[ i ].name ) )
			{
				textObjects[ i ].name = textObjects[ i ].textMesh.transform.parent.name;
			}
			for( int j = 0; j < textObjects[ i ].languagesText.Length; j++ )
			{
				textObjects[ i ].languagesText[ j ].name = textObjects[ i ].languagesText[ j ].language.ToString();
			}
		}
	}
#endif
}
