﻿using UnityEngine;
using System.Collections;

public class HeightText : MonoBehaviour {
	public string 		text;
	public Transform 	player;
	public float 		startHeight;

	private TextMesh   textMesh;
	private int 	   curHeight;
	private int 	   maxHeight;

	const string PREFS_KEY = "height";
	// Use this for initialization
	void Awake () {
		textMesh = GetComponent<TextMesh>();
		maxHeight = PlayerPrefs.GetInt( PREFS_KEY, 0 );
	}
	
	// Update is called once per frame
	void Update () {
		curHeight = Mathf.RoundToInt( player.position.y - startHeight );
		if( curHeight > maxHeight ){ maxHeight = curHeight; }
		textMesh.text = text + maxHeight;
	}
}
