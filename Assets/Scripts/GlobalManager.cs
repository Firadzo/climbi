﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds;
using GoogleMobileAds.Api;
#if UNITY_ANDROID
	using GooglePlayGames;
#endif

[System.Serializable]
public class ZObjectParams
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public ObjectType 	objType;
	public float 		zValue;
}

public enum ObjectType{ player, platform, enemy, warning, scoreEffect, bonus }
public class GlobalManager : MonoBehaviour {
	#region Variables
	public 	Transform[]					guiElements;
	public 	Camera[]					cameras;
	public 	GameObject[]				gameItems;
	public	GameObject[]				menuItems;
	public	SwitchButton				pauseBtn;
	public 	EnemySpawner				enemySpawner;
	public 	StartSpring					startSpring;
	public 	ZObjectParams[]				zObjParams;
	public 	Transform					playerTransf;
	public 	Transform					guiCameraTransf;
	private Camera						guiCamera;
	
	public 	GameObject 	leftBorder;
	private Transform 	leftBorderTransf;
	private Collider2D 	colliderLeft;
	private Rigidbody2D leftWallRb;

	public 	GameObject 	rightBorder;
	private Transform 	rightBorderTransf;
	private Collider2D 	colliderRight;
	private Rigidbody2D rightWallRb;

	private bool 		moveBack;

	const float MIN_DIST_TO_ACTIVATE_BORDER = 45f;
	const float BORDERS_RETURN_SPEED = 200f;
	
	private static Vector3			guiCameraPos;
	public 	static GlobalManager	instance;
	public 	static Transform 		mainCamTransf;
	public 	static int 				platforms_mask;

	//default values
	public const 	float	DEF_HALF_SCREEN_WIDTH 	= 384f;
	public const 	float 	DEF_HALF_SCREEN_HEIGHT 	= 512f;

	//current values
	private static 	float	cur_half_screen_height;
	private static 	float	cur_half_screen_width;
	private static 	float	cur_screen_height;
	private static	float	height_difference;	


	public GameObject				pauseMenu;
	public GameObject				gameOverMenu;

	private static	bool 	mouseClicked;
	private static	bool 	gamePaused;
	private static 	bool 	gameStarted;

	public GameObject		logInBtn;
	public GameObject		logOutBtn;
	private static 	bool	logIn;
	const string 	LOG_IN_KEY = "login";
	const string 	FIRST_START_KEY = "firstSTart";
	private bool	firstStart;
	public	bool		payd;
	#if UNITY_ANDROID
	const string 	LEADER_BOARD_ID = "CgkI_azO9vkREAIQAQ";
	const string	AD_BANNER_ID 	= "ca-app-pub-8915120471086193/1516570461";

	const string 	MAXIMUM_UPGRADE_ACHIEVEMENT = "CgkI_azO9vkREAIQAg";
	const string	FIRST_BUY_ACHIEVEMENT = "CgkI_azO9vkREAIQAw";
	const string	PERFECT_REACTION_ACHIEVEMENT = "CgkI_azO9vkREAIQEA";
	const string	CANT_TOUCH_ME_ACHIEVEMENT = "CgkI_azO9vkREAIQDw";
	const string	CRITTETS_ACHIEVEMENT = "CgkI_azO9vkREAIQEQ";

	const string 	NOT_BAD_ACHIEVEMENT = "CgkI_azO9vkREAIQBA";
	const float		NOT_BAD_HEIGHT = 10000f;
	const string 	VERY_GOOD_ACHIEVEMENT = "CgkI_azO9vkREAIQBQ";
	const float		VERY_GOOD_HEIGHT = 20000f;
	const string 	ALMOST_THERE_ACHIEVEMENT = "CgkI_azO9vkREAIQBg";
	const float		ALMOST_THERE_HEIGHT = 30000f;
	const string 	GREATE_ACHIEVEMENT = "CgkI_azO9vkREAIQDg";
	const float		GREATE_HEIGHT = 40000f;
	#endif
	private float	heightRecord;
	const 	string	HEIGHT_RECORD = "heightRecord";
	#endregion
	#region Properties
	public static float half_screen_height
	{
		get{ return cur_half_screen_height; }
	}

	public static float screen_height
	{
		get{ return cur_screen_height; }
	}

	public static float half_screen_width
	{
		get{ return cur_half_screen_width; }
	}
	
	public static float get_height_difference
	{
		get{ return height_difference; }
	}

	public static bool isGameActive
	{
		get { return gameStarted && !gamePaused; }
	}

	public static bool isGameStarted
	{
		get{ return gameStarted; }
	}

	public static bool isGamePaused
	{
		get { return gamePaused; }
	}

	public static bool isMouseClicked
	{
		get{ return mouseClicked; }
	}

	public static Vector3 getGuiCamPos
	{
		get { return guiCameraPos; }
	}

	public static float GetPlayerXOffsetFromCam
	{
		get { return GetPlayerPos.x - mainCamTransf.position.x; }
	}
	#endregion

	BannerView bannerView;

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
	public Vector2 fakeResolution = new Vector2( 768f, 1024f );
#endif

	// Use this for initialization
	void Awake () {
		instance = this;
		firstStart = PlayerPrefs.GetInt( FIRST_START_KEY, 1 ) == 1;
		if( firstStart )
		{
			PlayerPrefs.SetInt( FIRST_START_KEY, 0 );
		}
		heightRecord = PlayerPrefs.GetFloat( HEIGHT_RECORD, 0 );
#if UNITY_ANDROID// && !UNITY_EDITOR
		PlayGamesPlatform.Activate();
		logIn = PlayerPrefs.GetInt( LOG_IN_KEY, 1 ) == 1;
		if( logIn )
		{
			Social.localUser.Authenticate( 
			 ( bool success ) =>
			 {
				EnableLogin( !success );
			});
		}
		else
		{
			EnableLogin( true );
		}
#endif
//		float defaultRatio = 768f / 1024f;
		cur_half_screen_width = DEF_HALF_SCREEN_WIDTH;
		#if UNITY_EDITOR || UNITY_STANDALONE_WIN
//		Screen.SetResolution( (int)fakeResolution.x, (int)fakeResolution.y, true );
		float curRatio    = 	fakeResolution.x / fakeResolution.y;
#else
		float curRatio	= (float)Screen.width / Screen.height;
#endif
		//if( defaultRatio != curRatio )
		{
			cur_half_screen_height = DEF_HALF_SCREEN_WIDTH / curRatio; 

			int i;
			for( i = 0; i < cameras.Length; i++ )
			{
				cameras[ i ].orthographicSize = cur_half_screen_height;
			}
			Vector3 tempPos;
			float ratioX = cur_half_screen_width / DEF_HALF_SCREEN_WIDTH;
			float heightRatio = cur_half_screen_height / DEF_HALF_SCREEN_HEIGHT;
			for( i = 0; i < guiElements.Length; i++ )
			{
				tempPos = guiElements[ i ].localPosition;
				tempPos.x *= ratioX;
				tempPos.y *= heightRatio;
				guiElements[ i ].localPosition = tempPos;
			}
			ActiveBonusManager.ChangeIconHeight( heightRatio );
		}
		cur_screen_height = cur_half_screen_height * 2f;
		height_difference = cur_half_screen_height - DEF_HALF_SCREEN_HEIGHT;
		gameStarted  = false;
		EnableMenuItems( true );
		leftBorderTransf = leftBorder.transform;
		colliderLeft = leftBorder.GetComponent<Collider2D>();
		leftWallRb = leftBorder.GetComponent<Rigidbody2D>();
		rightBorderTransf = rightBorder.transform;
		colliderRight = rightBorder.GetComponent<Collider2D>();
		rightWallRb = rightBorder.GetComponent<Rigidbody2D>();
		platforms_mask = 1 << LayerMask.NameToLayer("platforms");
		guiCamera = guiCameraTransf.GetComponent<Camera>();
		guiCameraPos = guiCameraTransf.position;
		mainCamTransf = Camera.main.transform;
	}

	void Start()
	{
		EffectsManager.Init ();
		EnableGameItems( false );
		if (payd) {
			return;
		}
//#if UNITY_ANDROID
			bannerView = new BannerView(AD_BANNER_ID, AdSize.SmartBanner, AdPosition.Top);
			bannerView.AdLoaded += OnAdLoaded;
			bannerView.LoadAd( new AdRequest.Builder().Build() );
			StartCoroutine( RefreshAD() );
			adVisible = true;

//#endif
			AdBuddizBinding.SetAndroidPublisherKey("c214c651-ca23-4912-b199-67d5ad6aaa04");
			AdBuddizBinding.CacheAds();
	}

	private void OnAdLoaded( object sender, System.EventArgs args )
	{
		bannerView.Show();
	}

	private bool adVisible = false;
	private void ShowAd( bool visible )
	{
		if (payd) {
			return;
		}
		#if UNITY_ANDROID
		if( visible != adVisible )
		{
			if( visible )
			{
				bannerView.Show();
				bannerView.AdLoaded += OnAdLoaded;
			}
			else
			{
				bannerView.Hide();
				bannerView.AdLoaded -= OnAdLoaded;
			}
			adVisible = visible;
		}
#endif
	}

	private void ShowScreenAD(  )
	{
		if (payd) {
			return;
		}
		AdBuddizBinding.ShowAd ();
	}

#if UNITY_ANDROID
	/*
	void OpenSavedGame(string filename) {
		GooglePlayGames.BasicApi.SavedGame.ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
		savedGameClient.OpenWithAutomaticConflictResolution(filename, GooglePlayGames.BasicApi.SavedGame.DataSource.ReadCacheOrNetwork,
		                                                    GooglePlayGames.BasicApi.SavedGame.ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened );

	}
	
	public void OnSavedGameOpened( GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus status, GooglePlayGames.BasicApi.SavedGame.ISavedGameMetadata game) {
		if (status == GooglePlayGames.BasicApi.SavedGame.SavedGameRequestStatus.Success) {
			// handle reading or writing of saved game.
		} else {
			// handle error
		}
	}*/

	private IEnumerator RefreshAD()
	{
		yield return new WaitForSeconds( 45f );
		bannerView.LoadAd( new AdRequest.Builder().Build() );
		StartCoroutine( RefreshAD() );
		
	}	

	private void EnableLogin( bool enabled )
	{
		logInBtn.SetActive( enabled );
		logOutBtn.SetActive( !enabled );
	}

	private void _GoogleLogIn( System.Action onLogIn )
	{
		PlayerPrefs.SetInt( LOG_IN_KEY, 1 );
		Social.localUser.Authenticate( 
			( bool success ) =>
			{
				if( success )
				{
					EnableLogin( false );
					if( onLogIn != null ){ onLogIn(); }
				}
			});
	}

	private void GoogleLogIn()
	{
		_GoogleLogIn( null ); 
	}


	private void GoogleLogOut()
	{
		PlayerPrefs.SetInt( LOG_IN_KEY, 0 );
		((PlayGamesPlatform) Social.Active).SignOut();
		EnableLogin( true );
	}

	private void ShowLeaderBoard()
	{
		if( PlayGamesPlatform.Instance.IsAuthenticated() )
		{
			(( PlayGamesPlatform) Social.Active ).ShowLeaderboardUI( LEADER_BOARD_ID );
		}
		else
		{
			System.Action onLogIn = delegate() { (( PlayGamesPlatform) Social.Active ).ShowLeaderboardUI( LEADER_BOARD_ID ); };
			_GoogleLogIn( onLogIn );
			//if( GoogleLogIn() ){  (( PlayGamesPlatform) Social.Active ).ShowLeaderboardUI( LEADER_BOARD_ID ); }
		}
	}

	private void ShowAchievements()
	{
		if( PlayGamesPlatform.Instance.IsAuthenticated() )
		{
			Social.ShowAchievementsUI();
		}
		else
		{
			System.Action onLogIn = delegate() { Social.ShowAchievementsUI(); };
			_GoogleLogIn( onLogIn );
		}
	}


	public static void FirstBuyAchievement()
	{
		ReportAchievement ( FIRST_BUY_ACHIEVEMENT );
	}

	public static void PerfectReactionAchievement()
	{
		ReportAchievement ( PERFECT_REACTION_ACHIEVEMENT );
	}
	
	private static void ReportAchievement( string achievement_id )
	{
#if UNITY_EDITOR
		Debug.Log( achievement_id );
#else
		Social.ReportProgress( achievement_id, 100f, ( bool success ) => { } );
#endif
	}


	public static void CantTouchMeAchievement()
	{
		IncrementAchievement( CANT_TOUCH_ME_ACHIEVEMENT, 1 );
	}

	public static void	CrittersAchievement()
	{
		IncrementAchievement( CRITTETS_ACHIEVEMENT, 1 );
	}


	private static void IncrementAchievement( string achievement_id, int steps)
	{
		#if UNITY_EDITOR
		Debug.Log( achievement_id +"  "+steps );
		#else
		((PlayGamesPlatform) Social.Active).IncrementAchievement(
			achievement_id, 1, (bool success) => {
			// handle success or failure
		});
		#endif
	}
	#endif


	private void EnableMenuItems( bool enable )
	{
		for( int i = 0; i < menuItems.Length; i++ )
		{
			menuItems[ i ].SetActive( enable );
		}
	}

	private void EnableGameItems( bool enable )
	{
		for( int i = 0; i < gameItems.Length; i++ )
		{
			gameItems[ i ].SetActive( enable );
		}
	}
	

	public static void EnableBorders( bool enable )
	{
		if( enable )
		{
			Vector3 tmp;
			tmp = instance.leftBorderTransf.localPosition;
			tmp.x = -DEF_HALF_SCREEN_WIDTH;
			instance.leftBorderTransf.localPosition = tmp;

			tmp = instance.rightBorderTransf.localPosition;
			tmp.x = DEF_HALF_SCREEN_WIDTH;
			instance.rightBorderTransf.localPosition = tmp;

			if( instance.playerTransf.position.x <= -DEF_HALF_SCREEN_WIDTH )
			{
				tmp = instance.leftBorderTransf.localPosition;
				tmp.x = instance.playerTransf.position.x - MIN_DIST_TO_ACTIVATE_BORDER;
				instance.leftBorderTransf.localPosition = tmp;
				instance.colliderLeft.enabled = true;
				instance.moveBack = true;
//				Debug.LogError("");
			}
			else if( instance.playerTransf.position.x >= DEF_HALF_SCREEN_WIDTH )
			{
				tmp = instance.rightBorderTransf.localPosition;
				tmp.x = instance.playerTransf.position.x + MIN_DIST_TO_ACTIVATE_BORDER;
				instance.rightBorderTransf.localPosition = tmp;
				instance.colliderRight.enabled = true;
				instance.moveBack = true;
//				Debug.LogError("");
			}

		}
		instance.leftBorder.SetActive( enable );
		instance.rightBorder.SetActive( enable );
	}		

	public void Pause()
	{
		ShowAd( true );
		CoinsManager.SaveCoins();
		enemySpawner.PauseEnemies();
		EffectsManager.PauseEffects();
		BonusManager.PauseBonuses();
		WarningsManager.PauseWarnings();
		PlayerOnSpringJoint.instance.Pause();
		BackgroundScrollDownManager.Pause();
		if( gameStarted )
		{
			pauseMenu.SetActive( true );
		}
		else
		{
			ScoreManager.GameOver();
			gameOverMenu.SetActive( true );
			#if UNITY_ANDROID
			if( Social.localUser.authenticated )
			{
				Social.ReportScore( ScoreManager.getRecord, LEADER_BOARD_ID, 
				                   (bool succesfullyReported ) => {});
			}
			#endif
		}
		gamePaused = true;
	}

	public void Resume()
	{
		ShowAd( false );
		enemySpawner.ResumeEnemies();
		EffectsManager.ResumeEffects();
		BonusManager.ResumeBonuses();
		WarningsManager.ResumeWarnings();
		PlayerOnSpringJoint.instance.Resume();
		BackgroundScrollDownManager.Resume();
		pauseMenu.SetActive( false );
		gameOverMenu.SetActive( false );
		gamePaused = false;
	}

	public static void ActivateStartSpring()
	{
		instance.startSpring.ShowUp();
	}

	private void StartGame()
	{
		MyTapjoy.checkEarnings = false;
		EnableMenuItems( false );
		EnableGameItems( true );
		Restart();
		ShowAd( false );
	}

	private void OpenShope()
	{
		if( Random.Range( 0, 1f ) >= 0.5f )
		{
			ShowScreenAD();
		}
		if( gamePaused )
		{
			EnableGameItems( false );
		}
		else
		{
			EnableMenuItems( false );
		}
		ShopManager.OpenShop();
	}

	private void CloseShop()
	{
		if( gamePaused )
		{
			EnableGameItems( true );
		}
		else
		{
			EnableMenuItems( true );
		}
		ShopManager.CloseShop();

	}

	private void MainMenu()
	{
		if( Random.Range( 0, 1f ) >= 0.5f )
		{
			ShowScreenAD();
		}
		MyTapjoy.checkEarnings = true;
		SoundManager.PlayMenuMusic( true );
		pauseBtn.SetClicked( false );
		EnableMenuItems( true );
		EnableGameItems( false );
		ResetGame();
		ShowAd( true );
	}

	private void Restart()
	{
		if( Random.Range( 0, 1f ) >= 0.8f )
		{
			ShowScreenAD();
		}
		if( gamePaused )
		{
			pauseBtn.SetClicked( false );
			ResetGame();
		}
		SoundManager.PlayMenuMusic( false );
		ActiveBonusManager.SetBoostToPlayer();
		LevelGenerator.instance.Restart();
		gameStarted = true;
		if( firstStart )
		{
			firstStart = false;
			ShowTutorial();
		}
	}

	public TutorialController tutorialContr;
	private void ShowTutorial()
	{
		ShowAd( false );
		tutorialContr.ShowTutorial();
	}

	private void HideTutorial()
	{
		if( gamePaused )
		{
			ShowAd( true );
		}
		tutorialContr.HideTutorial();

	}

	private void ResetGame()
	{
		if( gamePaused )
		{
			instance.Resume();
		}
		SpawnManager.Reset();
		ActiveBonusManager.DeactivateAllBonuses();
		FollowPlayer.Reset();
		instance.startSpring.Reset();
		instance.enemySpawner.ResetEnemies();
		WarningsManager.ResetWarnings();
		LevelGenerator.instance.Reset();
		ScoreManager.ResetScore();
		BonusManager.ResetBonuses();
		BackgroundScrollDownManager.Reset();
		PlayerOnSpringJoint.instance.Reset();
		EffectsManager.ResetEffects();
		gameStarted = false;
	}


	public static float GetZParams( ObjectType objType )
	{
		for( int i = 0; i< instance.zObjParams.Length; i++ )
		{
			if( instance.zObjParams[ i ].objType == objType )
			{
				return instance.zObjParams[ i ].zValue;
			}
		}
		return 0;
	}


	public static Vector3 GetPlayerPos
	{
		get{ return GlobalManager.instance.playerTransf.position; }
	}

	public static float CameraHeight
	{
		get { return mainCamTransf.position.y; }
	}


	public static void GameOver()
	{
		gameStarted = false;
		instance.Pause();
	}

	RaycastHit2D hit;
	Vector3 t;
	Vector2 borderVel = new Vector2();
	void Update()
	{
		if( isGameActive )
		{
			if( moveBack )
			{
				if( rightBorderTransf.localPosition.x > DEF_HALF_SCREEN_WIDTH )
				{

					/*t.x -= BORDERS_RETURN_SPEED * Time.deltaTime;
					rightBorderTransf.localPosition = t;*/
					borderVel.x = -BORDERS_RETURN_SPEED;
					rightWallRb.velocity = borderVel;
					if( rightBorderTransf.localPosition.x <= DEF_HALF_SCREEN_WIDTH )
					{
						t = rightBorderTransf.localPosition;
						rightWallRb.velocity = Vector2.zero;
						t.x = DEF_HALF_SCREEN_WIDTH;
						rightBorderTransf.localPosition = t;
						moveBack = false;
					}

					if( rightBorderTransf.localPosition.x < playerTransf.position.x )
					{
						t.x = playerTransf.position.x + MIN_DIST_TO_ACTIVATE_BORDER;
						rightBorderTransf.localPosition = t;
						moveBack = true;
					}

				}
				else if( leftBorderTransf.localPosition.x < -DEF_HALF_SCREEN_WIDTH )
				{
				
					/*t.x += BORDERS_RETURN_SPEED * Time.deltaTime;
					leftBorderTransf.localPosition = t;*/
					//leftWallRb.velocity = new Vector2( BORDERS_RETURN_SPEED, 0 );
					borderVel.x = BORDERS_RETURN_SPEED;
					leftWallRb.velocity = borderVel;
					if( leftBorderTransf.localPosition.x >= -DEF_HALF_SCREEN_WIDTH )
					{
						leftWallRb.velocity = Vector2.zero;
						t.x = - DEF_HALF_SCREEN_WIDTH;
						leftBorderTransf.localPosition = t;
						moveBack = false;
					}

					if( leftBorderTransf.localPosition.x > playerTransf.position.x )
					{
						t.x = playerTransf.position.x - MIN_DIST_TO_ACTIVATE_BORDER;
						leftBorderTransf.localPosition = t;
						moveBack = true;
					}

				}
				else
				{
					t = rightBorderTransf.localPosition;
					rightWallRb.velocity = Vector2.zero;
					t.x = DEF_HALF_SCREEN_WIDTH;
					rightBorderTransf.localPosition = t;

					t = leftBorderTransf.localPosition;
					leftWallRb.velocity = Vector2.zero;
					t.x = - DEF_HALF_SCREEN_WIDTH;
					leftBorderTransf.localPosition = t;
		
					moveBack = false;
				}
			}
			else
			{
				colliderLeft.enabled = playerTransf.position.x - leftBorder.transform.position.x <= MIN_DIST_TO_ACTIVATE_BORDER;
				colliderRight.enabled =  rightBorder.transform.position.x - playerTransf.position.x <= MIN_DIST_TO_ACTIVATE_BORDER;
			}

#if UNITY_ANDROID
			if( heightRecord < NOT_BAD_HEIGHT )
			{
				if( playerTransf.position.y >= NOT_BAD_HEIGHT )
				{
					ReportAchievement( NOT_BAD_ACHIEVEMENT );
					heightRecord = playerTransf.position.y;
					PlayerPrefs.SetFloat( HEIGHT_RECORD, heightRecord );
				}
			}
			else if( heightRecord < VERY_GOOD_HEIGHT )
			{
				if( playerTransf.position.y >= VERY_GOOD_HEIGHT )
				{
					ReportAchievement( VERY_GOOD_ACHIEVEMENT );
					heightRecord = playerTransf.position.y;
					PlayerPrefs.SetFloat( HEIGHT_RECORD, heightRecord );
				}
			}
			else if( heightRecord < ALMOST_THERE_HEIGHT )
			{
				if( playerTransf.position.y >= ALMOST_THERE_HEIGHT )
				{
					ReportAchievement( ALMOST_THERE_ACHIEVEMENT );
					heightRecord = playerTransf.position.y;
					PlayerPrefs.SetFloat( HEIGHT_RECORD, heightRecord );
				}
			}
			else if( heightRecord < GREATE_HEIGHT )
			{
				if( playerTransf.position.y >= GREATE_HEIGHT )
				{
					ReportAchievement( GREATE_ACHIEVEMENT );
					heightRecord = playerTransf.position.y;
					PlayerPrefs.SetFloat( HEIGHT_RECORD, heightRecord );
				}
			}
#endif
		}

		if( Input.GetMouseButtonDown( 0 ) )
		{
			mouseClicked = true;
			hit = Physics2D.Raycast( guiCamera.ScreenToWorldPoint( Input.mousePosition ), Vector2.zero );
			if( hit.collider != null )
			{
				if( hit.collider.CompareTag( "button" ) )
				{
					hit.collider.SendMessage( "OnButtonDown", SendMessageOptions.DontRequireReceiver );
				}
			}
			else
			{
				if( !gamePaused  && gameStarted )
				{
					PlayerOnSpringJoint.instance.MouseDown();
				}
			}
		}
		if( Input.GetMouseButtonUp( 0 ) )
		{
			mouseClicked = false;
			hit = Physics2D.Raycast( guiCamera.ScreenToWorldPoint( Input.mousePosition ), Vector2.zero );
			if( hit.collider != null )
			{
				if( hit.collider.CompareTag( "button" ) )
				{
					hit.collider.SendMessage( "OnButtonUp", SendMessageOptions.DontRequireReceiver );
				}
			}
		}
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		for( int i = 0; i< zObjParams.Length; i++ )
		{
			zObjParams[ i ].name = zObjParams[ i ].objType.ToString()+" "+zObjParams[ i ].zValue;
		}
	}
#endif
}




