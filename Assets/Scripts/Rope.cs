﻿using UnityEngine;
using System.Collections;

public class Rope : MonoBehaviour {
	
	public float		scaleRatio; 
	public float 		maxRopeLength;
	public float 		minRopeLength = 1f;
	public float 		ropeSpeed;
	//private float		curRopeSpeed;
	private float 		moveTime;
	private CTimer		moveTimer;
	private Transform 	transf;
	private Vector3 	ropeEndPos;
	private bool 		moveToPoint;
	private Vector3 	tmpVec;
	private bool 		platformCatched;
	private bool 		moveBack;
	private bool 		activeState;
	private Vector3 	pivotPos;
	private Vector3		startPivotPos;
	private Vector3		startLocalScale;

	private BoxCollider2D	ropeCollider;
	private AudioSource		audioSource;

	public 	AudioClip 	throwRopeSound;
	public	AudioClip	ropeBackSound;

	public	AnimationCurve	ropeMoveCurve;

	void Awake () {
		transf = transform;
		moveTimer = new CTimer();
		startPivotPos = transf.localPosition;
		startLocalScale = transf.localScale;
		//curRopeSpeed = ropeSpeed / scaleRatio;
		ropeCollider = GetComponent<BoxCollider2D>();
		audioSource = GetComponent<AudioSource>();
		SoundManager.RegisterAudioSource( audioSource );
		ropeCollider.enabled = false;
	}

	public Vector3 getPosition
	{
		get { return transf.position; }
	}

	public Vector3 getRaycastPosition
	{
		get { return transf.position + ( ropeEndPos - transf.position ).normalized * ( ropeLength / 2f ); }
	}

	public float ropeLength
	{
		get { return  ( transf.localScale.y * scaleRatio ); }
	}

	public bool isMoving
	{
		get { return moveToPoint || moveBack; }
	}


	void OnTriggerEnter2D( Collider2D other )
	{
		if( moveToPoint )
		{
			if( PlayerOnSpringJoint.instance.ConnectToPlatform( other ) )
			{
				moveToPoint = false;
				platformCatched = true;
				activeState = false;
				ropeCollider.enabled = false;
			}
		}
	}

	void CheckCollisionWithPlatform()
	{
		Vector3 endPos = ropeEndPos - transf.position;
		endPos = transf.position + endPos.normalized * transf.localScale.y * scaleRatio;
		Collider2D platform = Physics2D.OverlapPoint (endPos, GlobalManager.platforms_mask);
		if( PlayerOnSpringJoint.instance.ConnectToPlatform( platform ) )
		{
			moveToPoint = false;
			platformCatched = true;
			activeState = false;
			ropeCollider.enabled = false;
		}
	}

	public void MoveBack()
	{
		audioSource.clip = ropeBackSound;
		audioSource.Play();
		moveToPoint = false;
		moveBack = true;
		activeState = true;
		platformCatched = false;
		ropeEndPos = PlayerOnSpringJoint.instance.ropeConnectionTR.position;
		moveTimer.Start( maxRopeLength / ropeSpeed );
		moveTimer.Update( moveTimer.time *  ( 1f - Mathf.InverseLerp( 0, maxRopeLength, ( transf.localScale.y * scaleRatio ) ) ) );
	}

	public void Reset()
	{
		audioSource.Stop();
		transf.localPosition = startPivotPos;
		transf.localScale 	=	startLocalScale;
		moveToPoint = false;
		activeState = false;
		moveBack = false;
		platformCatched = false;
		ropeCollider.enabled = false;
	}

	public void ThrowRope()
	{
		if( activeState ) { return; }
		moveTimer.Start( maxRopeLength / ropeSpeed );
		ropeCollider.enabled = true;
		transf.localPosition = startPivotPos;
		ropeEndPos  = Camera.main.ScreenToWorldPoint( Input.mousePosition );
		moveToPoint = true;
		activeState = true;
		audioSource.clip = throwRopeSound;
		audioSource.Play();
	}
	
	float angle;
	Vector3 dir;
	void Update () {
		if( GlobalManager.isGamePaused )
		{
			return;
		}
		if( activeState )
		{
			dir   = ropeEndPos - transf.position;
			dir.Normalize();
			angle = Mathf.Atan2( dir.y, dir.x ) * Mathf.Rad2Deg;
			angle -= 90f;
			transf.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
			if( moveToPoint )
			{
				if( moveTimer.active )
				{
					moveTimer.Update( Time.deltaTime );
					tmpVec = transf.localScale;
					tmpVec.y = ( maxRopeLength / scaleRatio ) * ropeMoveCurve.Evaluate( moveTimer.w );
					transf.localScale  = tmpVec;
					if( !moveTimer.active )
					{
						moveToPoint = false;
						moveBack = true;
						moveTimer.Start( ( transf.localScale.y * scaleRatio ) / ropeSpeed );
					}
				}
				CheckCollisionWithPlatform();
			}
			else if( moveBack )
			{
				if( moveTimer.active )
				{
					moveTimer.Update( Time.deltaTime );
					tmpVec = transf.localScale;
					tmpVec.y = ( maxRopeLength / scaleRatio ) * ( 1f - moveTimer.w );
					transf.localScale  = tmpVec;
					if( !moveTimer.active )
					{
						moveBack = false;
						activeState = false;
					}
				}
			}
		}
		else if( platformCatched )
		{
			dir   = PlayerOnSpringJoint.instance.ropeConnectionTR.position - transf.position;
			dir.Normalize();
			angle = Mathf.Atan2( dir.y, dir.x ) * Mathf.Rad2Deg;
			angle -= 90f;
			transf.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		}
	}

	private float curDist;
	void LateUpdate()
	{
		if( GlobalManager.isGamePaused )
		{
			return;
		}
		if( platformCatched )
		{
			curDist = Vector2.Distance( transf.position, PlayerOnSpringJoint.instance.ropeConnectionTR.position );
			transf.localScale = new Vector3( 2f, curDist / scaleRatio, 1f );
		}
	}

	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		if (Application.isPlaying ) {
			Gizmos.color = Color.green;
			Gizmos.DrawSphere (ropeEndPos, 10f);
			Gizmos.color = Color.red;
			Vector3 endPos = ropeEndPos - transf.position;
			endPos = transf.position + endPos.normalized * transf.localScale.y * scaleRatio;
			Gizmos.DrawSphere (endPos, 10f);
		}
	}
	#endif

}
