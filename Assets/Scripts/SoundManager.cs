﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoBehaviour {

	public AudioSource 	menuMusic;
	public AudioSource	gameMusic;
	public AudioSource	bonusPickSound;
	public AudioSource	clickSound;
	public AudioSource	sucessBuy;
	public AudioSource	failedBuy;

	public SwitchButton	soundButton;

	private static List<AudioSource>	gameSounds = new List<AudioSource>();
	private static bool			muted;
	public 	static SoundManager instance;

	const 	string		PREFS_KEY = "mute";

	public static bool isMuted
	{
		get{ return muted; }
	}

	void Awake()
	{
		muted = PlayerPrefs.GetInt( PREFS_KEY, 0 ) == 1;
		instance = this;
		RegisterAudioSource( menuMusic );
		RegisterAudioSource( gameMusic );
		RegisterAudioSource( bonusPickSound );
		RegisterAudioSource( clickSound );
		RegisterAudioSource( sucessBuy );
		RegisterAudioSource( failedBuy );
	}

	void Start()
	{
		soundButton.SetClicked( muted );
		PlayMenuMusic( true );
	}

	public static void RegisterAudioSource( AudioSource source )
	{
		gameSounds.Add( source );
		source.mute = muted;
	}

	public void MuteSound()
	{
		PlayerPrefs.SetInt( PREFS_KEY, 1 );
		muted = true;
		SetMute();
	}

	public void UnmuteSound()
	{
		PlayerPrefs.SetInt( PREFS_KEY, 0 );
		muted = false;
		SetMute();
	}

	private void SetMute()
	{
		for( int i = 0; i < gameSounds.Count; i++ )
		{
			gameSounds[ i ].mute = muted;
		}
	}

	public static void PlayBonusPickSound()
	{
		instance.bonusPickSound.Play();
	}

	public static void PlayClickSound()
	{
		instance.clickSound.Play();
	}

	public static void PlaySucessBuy()
	{
		instance.sucessBuy.Play();
	}

	public static void PlayFailedBuy()
	{
		instance.failedBuy.Play();
	}

	public static void PlayMenuMusic( bool play )
	{
		if( play )
		{
			instance.gameMusic.Stop();
			instance.menuMusic.Play();
		}
		else
		{
			instance.gameMusic.Play();
			instance.menuMusic.Stop();
		}
	}

}
