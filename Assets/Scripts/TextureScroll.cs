﻿using UnityEngine;
using System.Collections;

public class TextureScroll : MonoBehaviour {
	private Material mat;
	public 	float speed;
	// Use this for initialization
	void Awake () {
		mat = GetComponent<MeshRenderer>().material;
	}
	
	private Vector2 tmpVec2;
	void Update () {
		tmpVec2 = mat.mainTextureOffset;
		tmpVec2.x += speed * Time.deltaTime;
		mat.mainTextureOffset = tmpVec2;
	}
}
