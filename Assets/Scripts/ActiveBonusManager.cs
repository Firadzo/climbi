﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BonusIconInfo
{
	#if UNITY_EDITOR
	[HideInInspector]
	public string name;
	#endif
	public GameObject 				prefab;
	public ESpecialBonusType		bonusType;
}

public class BonusIconData
{
	public	ESpecialBonusType		bonusType;
	public 	GameObject				bonusIcon;
	public 	Transform				bonusIconTr;
	public	CircleProgressBar		progressBar;	
	private CTimer					bonusTimer;

	public  BonusIconData( ESpecialBonusType bonus_Type, GameObject obj, Transform parent )
	{
		bonusIcon = obj;
		bonusType = bonus_Type;
		bonusIconTr = bonusIcon.transform;
		bonusIconTr.parent = parent;
		progressBar = bonusIcon.GetComponentInChildren< CircleProgressBar >();
		bonusTimer = new CTimer();
		bonusIcon.SetActive( false );
	}

	public bool isActive
	{
		get { return bonusTimer.active; }
	}
	

	public void DeActivate()
	{
		if( bonusIcon.activeSelf )
		{
			if( bonusTimer.active )
			{
				bonusTimer.Stop();
			}
			bonusIcon.SetActive( false );
			PlayerOnSpringJoint.ActivateBonus( bonusType, false );
		}
	}

	public void Activate( float time )
	{
		if( !bonusIcon.activeSelf )
		{
			bonusIcon.SetActive( true );
			ActiveBonusManager.ChangeIconsNumber( 1 );
		}
		progressBar.Animate( time );
		bonusTimer.Start( time );
	}

	public void UpdateTime()
	{
		if( bonusTimer.active )
		{
			bonusTimer.Update( Time.deltaTime );
		//	textMesh.text = Mathf.RoundToInt( bonusTimer.getCurTime ).ToString();
			if( !bonusTimer.active )
			{
				DeActivate();
				ActiveBonusManager.ChangeIconsNumber( -1 );
			}
		}
	}

}

public enum EBoostType { shield, lowGravity, coinMagnet, startJump, timeAdd }
[ System.Serializable ]
public class BoostInfo
{
	public 	EBoostType 				boostType;
	public 	string 					prefsKey;
	public	ESpecialBonusType		replaceBonus;
	public	ESpecialBonusType		stuckWithBonus;
	private bool 					activated;

	public void GetState()
	{
		activated = 1 == PlayerPrefs.GetInt( prefsKey, 0 );
	}

	public bool isActivated
	{
		get {
			return activated;
		}
	}

	public void AvailableInShop()
	{
		if( activated )
		{
			activated = false;
			PlayerPrefs.SetInt( prefsKey, 0 );
		}
	}

	public void Bought()
	{
		activated = true;
		PlayerPrefs.SetInt( prefsKey, 1 );
	}
}


[System.Serializable]
public class UpgradeInShopData
{
	public 	ESpecialBonusType			bonusType;				
	public 	UpgradeInfo[] 				upgradeLevels;
	public	string						prefsKey;

	private	int							curUpgradeLevel = 0;

	public int getUpgradeLevel
	{
		get{ return curUpgradeLevel; }
	}

	public bool isFullyUpgraded
	{
		get { return curUpgradeLevel == upgradeLevels.Length - 1; }
	}

	public void ReadSavedData()
	{
		curUpgradeLevel = PlayerPrefs.GetInt( prefsKey, 0 );
		ActiveBonusManager.ReadUpgradeData( bonusType );
	}
	
	public void Upgrade()
	{
		curUpgradeLevel++;
		PlayerPrefs.SetInt( prefsKey, curUpgradeLevel );
		ActiveBonusManager.ReadUpgradeData( bonusType );
	}

	public float GetCurUpgradePower()
	{
		return upgradeLevels[ curUpgradeLevel ].power;
	}

	public float GetCurUpgradeDuration()
	{
		return upgradeLevels[ curUpgradeLevel ].duration;
	}

	public int GetUpgradeCost()
	{
		return upgradeLevels[ curUpgradeLevel ].nextLevelCost;
	}

	public float GetUpgradeProgress()
	{
		if( curUpgradeLevel == 0 ){ return 0; }
		return ( curUpgradeLevel + 1f )/upgradeLevels.Length;
	}

}

[System.Serializable]
public class UpgradeInfo
{
	public 	float 	duration;
	public	float	power;//это значение будет обрабатываться для различных бонусов по разному
	public	int		nextLevelCost;//цена для покупки следующего уровня, то есть для последнего элемента в массиве апгрейдов она равна нулю
}



public class ActiveBonusManager : MonoBehaviour {

	public	BonusIconInfo[]		bonusIconsInfo;
	public 	BoostInfo[]			boosts;
	public	UpgradeInShopData[]		upgrades;	

	private static List<BoostInfo>	boostInCurrentGame = new List<BoostInfo>();
	private static Dictionary< ESpecialBonusType, BonusIconData > bonusIcons = new Dictionary<ESpecialBonusType, BonusIconData>();
	//private static ESpecialBonusType	curBonusType = ESpecialBonusType.none;
//	private static CTimer 				bonusTimer;
//	private static BonusIconData		curBonusIcon;
	private static Transform			transf;
	private static ActiveBonusManager	instance;
	private static int 					iconsNumber;
	private	const float					DIST_BETWEEN_ICONS = 165f;	
	private static Vector3				icon_pos = new Vector3( 0, -400f, 9f );

	public	CoinMagnet					coinMagnet;
	private static float				lowGravityPower;
	private static float				bonusesTimeMultiplier = 1f;	

	public static float getLowGravityPower
	{
		get{ return lowGravityPower; }
	}

	void Awake()
	{
		instance = this;
	//	bonusTimer = new CTimer();
		transf = transform;
	}

	void Start()
	{
		int i;
		for( i = 0; i < bonusIconsInfo.Length; i++ )
		{
			bonusIcons.Add( bonusIconsInfo[ i ].bonusType, new BonusIconData( bonusIconsInfo[ i ].bonusType, Instantiate( bonusIconsInfo[ i ].prefab ) as GameObject, transf ) );
		}
		for( i = 0; i < boosts.Length; i++ )
		{
			boosts[ i ].GetState();
		}
		for( i = 0; i < upgrades.Length; i++ )
		{
			upgrades[ i ].ReadSavedData();
		}
	}

	public static void ChangeIconHeight( float ratio )
	{
		icon_pos.y *= ratio;
	}
	

	public static UpgradeInShopData GetUpgradeByType( ESpecialBonusType bonusType )
	{
		for( int i = 0; i < instance.upgrades.Length; i++ )
		{
			if( instance.upgrades[ i ].bonusType == bonusType )
			{
				return instance.upgrades[ i ];
			}
		}
		return null;
	}

	public static void ReadUpgradeData( ESpecialBonusType bonusType )
	{
		UpgradeInShopData upgrdDat = GetUpgradeByType( bonusType );
		switch( upgrdDat.bonusType )
		{
			case ESpecialBonusType.coinMagnet:
				instance.coinMagnet.SetRadius( upgrdDat.GetCurUpgradePower() );
				break;
			case ESpecialBonusType.lowGravity:
				lowGravityPower = upgrdDat.GetCurUpgradePower();
				break;
		}
	}

	/*private static void SetCurBonusIcon( ESpecialBonusType bonusType )
	{
		curBonusIcon = bonusIcons[ bonusType ];
	}

	private static bool IsBonusActive( ESpecialBonusType bonusType )
	{
		return bonusIcons[ bonusType ].bonusIcon.activeSelf;
	}*/

	
	public static void DeactivateAllBonuses()
	{
		iconsNumber = 0;
		foreach( BonusIconData bonusIcon in bonusIcons.Values )
		{
			bonusIcon.DeActivate();
		}
	}

	public static void ChangeIconsNumber( int number )
	{
		iconsNumber += number;
		Vector3 startPos = icon_pos;
		startPos.x -= ( ( iconsNumber - 1f )  * DIST_BETWEEN_ICONS )/2f;
		foreach( BonusIconData bonusIcon in bonusIcons.Values )
		{
			if( bonusIcon.bonusIcon.activeSelf )
			{
				bonusIcon.bonusIconTr.localPosition = startPos;
				startPos.x += DIST_BETWEEN_ICONS;
			}
		}
	}

	public static void ActivateBonus( ESpecialBonusType bonusType )
	{
		if( bonusType != ESpecialBonusType.none )
		{
			for( int i = 0; i < boostInCurrentGame.Count; i++ )
			{
				if( boostInCurrentGame[ i ].replaceBonus == bonusType )
				{
					return  ;
				}
			}
		}
		bonusIcons[ bonusType ].Activate( GetUpgradeByType( bonusType ).GetCurUpgradeDuration() * bonusesTimeMultiplier );
		//bonusIcons[ bonusType ].bonusIconTr.localPosition = ICON_POS;
		/*if( curBonusType != bonusType )
		{
			if( curBonusIcon != null )
			{
				curBonusIcon.DeActivate();
			}
			if( bonusType != ESpecialBonusType.none )
			{
				SetCurBonusIcon( bonusType );
				curBonusIcon.bonusIconTr.localPosition = ICON_POS;
				curBonusIcon.bonusIcon.SetActive( true );
				PlayerOnSpringJoint.ActivateBonus( ESpecialBonusType.none );
			}
		}
		if( bonusType != ESpecialBonusType.none )
		{
			bonusTimer.Start( time );
		}
		curBonusType = bonusType;*/
		PlayerOnSpringJoint.ActivateBonus( bonusType, true );
	}

	/// <summary>
	/// Проверка, блокирует ли буст  подбираемый бонус
	/// </summary>
	/// <param name="bonusType">Bonus type.</param>
	/*private static bool CanIActivateBonus( ESpecialBonusType bonusType )
	{
		for( int i = 0; i < boostInCurrentGame.Count; i++ )
		{
			if( boostInCurrentGame[ i ].replaceBonus == bonusType )
			{
				return false;
			}
			else if( boostInCurrentGame[ i ].stuckWithBonus == bonusType )
			{
				return true;
			}
		}
		return true;
	}*/

	private BoostInfo GetBoostInfo( EBoostType boostType )
	{
		for( int i = 0; i < boosts.Length; i++ )
		{
			if( boosts[ i ].boostType == boostType )
			{
				return boosts[ i ];
			}
		}
		return null;
	}

	public static bool GetBoostState( EBoostType boostType )
	{
		return instance.GetBoostInfo( boostType ).isActivated;
	}

	public static void ActivateBoost( EBoostType boostType )
	{
	 	instance.GetBoostInfo( boostType ).Bought();
	}

	public static void SetBoostToPlayer()
	{	
		int i;
		for( i = 0; i < boostInCurrentGame.Count; i++ )
		{
			PlayerOnSpringJoint.EnableBoost( boostInCurrentGame[ i ].boostType, false );//деактивация бустов предидущей игры
		}
		boostInCurrentGame.Clear();
		bonusesTimeMultiplier = 1f;
		for( i = 0; i < instance.boosts.Length; i++ )
		{
			if( instance.boosts[ i ].isActivated )
			{
				if( instance.boosts[ i ].boostType == EBoostType.timeAdd )
				{
					bonusesTimeMultiplier = 1.5f;
				}
				else
				{
					PlayerOnSpringJoint.EnableBoost( instance.boosts[ i ].boostType, true );
				}
				boostInCurrentGame.Add( instance.boosts[ i ] );
				instance.boosts[ i ].AvailableInShop();
			}
		}
	}
	
	void Update()
	{
		if( GlobalManager.isGameActive )
		{
			foreach( BonusIconData bonusIcon in bonusIcons.Values )
			{
				bonusIcon.UpdateTime();
			}
			/*if( bonusTimer.active )
			{
				if( bonusTimer.Update( Time.deltaTime ) )
				{
					ActivateBonus( ESpecialBonusType.none );
				}
				else
				{
					curBonusIcon.textMesh.text = Mathf.RoundToInt( bonusTimer.getCurTime ).ToString();
				}
			}*/
		}
	}


	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		if( bonusIconsInfo != null )
		{
			for( int i = 0; i < bonusIconsInfo.Length; i++ )
			{
				bonusIconsInfo[ i ].name = bonusIconsInfo[ i ].bonusType.ToString();
			}
		}
	}
	#endif
}
