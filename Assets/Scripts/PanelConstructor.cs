﻿using UnityEngine;
using System.Collections;

public class PanelConstructor : MonoBehaviour {

	public 	Sprite	panelBG;
	public	Sprite	panelCorner;
	public 	Sprite  panelSide;
	public 	string  panelName;
	public 	Vector2 size;
	public 	Color	color;
	private Vector2	halfSize;

	readonly Vector2	halfSideSize = new Vector2( 0.5f, 2f );
	readonly Vector2	halfCornerSize = new Vector2( 2.5f, 2.5f );

	public 	bool 	create;
	Transform		parent;

	SpriteRenderer spr;
	void SetRendererAndParent( Transform t, Sprite sprite, Transform parent )
	{
		spr = t.gameObject.AddComponent<SpriteRenderer>();
		spr.sprite = sprite;
		spr.color = color;
		t.parent = parent;
	}

	void OnDrawGizmosSelected()
	{
		if( create )
		{
			create = false;
			if( size != Vector2.zero )
			{
				halfSize = size / 2f;
				Vector3 tmp = Vector3.zero;
				Transform t;

				parent = new GameObject( panelName ).transform;

				t = new GameObject( "LUCorner" ).transform;
				SetRendererAndParent( t, panelCorner, parent );
				tmp.x = -halfSize.x + halfCornerSize.x;
				tmp.y = halfSize.y - halfCornerSize.y;
				t.localPosition = tmp;

				t = new GameObject( "RUCorner" ).transform;
				SetRendererAndParent( t, panelCorner, parent );
				tmp.x = halfSize.x - halfCornerSize.x;
				tmp.y = halfSize.y - halfCornerSize.y;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.x *= -1f;
				t.localScale = tmp;

				t = new GameObject( "LDCorner" ).transform;
				SetRendererAndParent( t, panelCorner, parent );
				tmp.x = -halfSize.x + halfCornerSize.x;
				tmp.y = -halfSize.y + halfCornerSize.y;
				tmp.z = 0;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.y *= -1f;
				t.localScale = tmp;

				t = new GameObject( "RDCorner" ).transform;
				SetRendererAndParent( t, panelCorner, parent );
				tmp.x = halfSize.x - halfCornerSize.x;
				tmp.y = -halfSize.y + halfCornerSize.y;
				tmp.z = 0;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.y *= -1f;
				tmp.x *= -1f;
				t.localScale = tmp;

				t = new GameObject( "Bg" ).transform;
				SetRendererAndParent( t, panelBG, parent );
				tmp = Vector3.zero;
				tmp.z = 0.1f;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.x = ( size.x - halfSideSize.y * 4f ) / 2f;
				tmp.y = ( size.y - halfSideSize.y * 4f) / 2f;
				t.localScale = tmp;


				t = new GameObject( "UPSide" ).transform;
				SetRendererAndParent( t, panelSide, parent );
				tmp = Vector3.zero;
				tmp.y = halfSize.y - halfSideSize.y;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.x = size.x - halfCornerSize.y * 4f;
				t.localScale = tmp;

				t = new GameObject( "BOTSide" ).transform;
				SetRendererAndParent( t, panelSide, parent );
				tmp = Vector3.zero;
				tmp.y = -halfSize.y + halfSideSize.y;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.x = size.x - halfCornerSize.y * 4f;
				tmp.y *= -1f;
				t.localScale = tmp;

				t = new GameObject( "LeftSide" ).transform;
				SetRendererAndParent( t, panelSide, parent );
				tmp = Vector3.zero;
				tmp.x = -halfSize.x + halfSideSize.y;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.x = size.y - halfCornerSize.y * 4f;
				t.localScale = tmp;
				tmp = t.localEulerAngles;
				tmp.z = 90f;
				t.localEulerAngles = tmp;

				t = new GameObject( "RightSide" ).transform;
				SetRendererAndParent( t, panelSide, parent );
				tmp = Vector3.zero;
				tmp.x = halfSize.x - halfSideSize.y;
				t.localPosition = tmp;
				tmp = t.localScale;
				tmp.x = size.y - halfCornerSize.y * 4f;
				tmp.y *= -1f;
				t.localScale = tmp;
				tmp = t.localEulerAngles;
				tmp.z = 90f;
				t.localEulerAngles = tmp;

			}
		}
	}
}
