﻿using UnityEngine;
using System.Collections;

public class UpgradeInShop : ItemInShopBase {

	public 	ESpecialBonusType	bonusType;
	public 	SpriteRenderer[]	progressBar;
	public 	Color				upgraded;
	public	Color				notUpgraded;

	private UpgradeInShopData	upgradeData;	

	override public void GetItemState()
	{
		if( upgradeData ==  null )
		{
			upgradeData = ActiveBonusManager.GetUpgradeByType( bonusType );
			bought = upgradeData.isFullyUpgraded;
			SetProgress();
		}
		boughtObj.SetActive( bought );
		priceObj.SetActive( !bought );
		if( !bought )
		{
			price = upgradeData.GetUpgradeCost();
			priceText.text = price.ToString();
		}
	}

	private void SetProgress()
	{
		for( int i = 0; i < upgradeData.getUpgradeLevel; i++ )
		{
			progressBar[ i ].color = upgraded;
		}
	}

	override protected void Buying()
	{
		CoinsManager.SpendCoins( price );
		upgradeData.Upgrade();
		SetProgress();
		if( upgradeData.isFullyUpgraded )
		{
			bought = true;
			boughtObj.SetActive( true );
			priceObj.SetActive( false );
		}
		else
		{
			price = upgradeData.GetUpgradeCost();
			priceText.text = price.ToString();
		}
	}
}
