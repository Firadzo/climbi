﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using Soomla.Store;

public class CoinsManager : MonoBehaviour {

	public TextMesh	coinsInShop;
	private static Text		coinsTextST;
	private static TextMesh	coinsInShopST;
	private static int 		coinsNumber;
	const string PREFS_KEY = "COINS";

	public static int getCoinsNumber
	{
		get { return coinsNumber; }
	}

	public static CoinsManager instance { get; private set; }

	void Awake()
	{
		instance = this;
		coinsInShopST = coinsInShop;
		coinsTextST = GetComponentInChildren<Text>();
	}
	// Use this for initialization
	public void  AStart () {

		coinsNumber = PlayerPrefs.GetInt( PREFS_KEY, 0 );
//		if (coinsNumber != 0) {
//			//StoreInventory.RefreshOnCurrencyBalanceChanged (MyIStore.COINS, 0, coinsNumber);
//			StoreInventory.GiveItem (MyIStore.COINS_ID, coinsNumber);
//			PlayerPrefs.SetInt (PREFS_KEY, 0);
//		} else {
//			coinsNumber = StoreInventory.GetItemBalance( MyIStore.COINS_ID );
//		}
		SetText();
	}

	private static void SetText()
	{
		coinsTextST.text = coinsNumber.ToString();
		coinsInShopST.text = coinsTextST.text;
	}

	public static void AddCoin()
	{
		coinsNumber++;
		SetText();
	}

	public static void SpendCoins(  int number )
	{
		coinsNumber -= number;
//		if (number > 0) {
//			StoreInventory.TakeItem ( MyIStore.COINS_ID, number );
//		} else {
//			StoreInventory.GiveItem ( MyIStore.COINS_ID, number );
//		}
		SaveCoins();
		SetText();
	}


	public static void SaveCoins()
	{
		//StoreInfo.Save ();
		PlayerPrefs.SetInt( PREFS_KEY, coinsNumber );
	}
}
