using UnityEngine;
using System.Collections;

[System.Serializable]
public class CollapsingPlatformParams
{
	public 	Vector2 		size;
	public 	InGameEffect	onDestroyEffect;
	public 	Sprite			normal;
	public 	Sprite			damaged;
	public 	Sprite			damaged2;

}

public class CollapsingPlatform : PlatformBase {	
	public 	float			lifeProcForDamaged;
	public 	float			lifeProcForDamaged2;
	public	AnimationCurve	lifeTimerCurve;
	private CTimer			lifeTimer;
	private bool 			playerConnected;

	public 	AudioClip[]		crackSounds;
	private AudioSource		audioSource;

	public 	CollapsingPlatformParams[] 	platformParams;
	private CollapsingPlatformParams	curParams;

	private bool damageFirstStep;
	private bool damageSecondStep;

	override protected void Awake () {
		lifeTimer = new CTimer( );
		audioSource = GetComponent<AudioSource>();
		SoundManager.RegisterAudioSource( audioSource );
		base.Awake();
	}

	override public bool IsDestroyed
	{
		get{ return !isActive; }
	}

	override public void PrepareForSpawn()
	{
		curParams = platformParams[ Random.Range( 0, platformParams.Length ) ];
		spriteRenderer.sprite = curParams.normal;
		boxCollider2D.size = curParams.size;
		platformSize = curParams.size / 2f;
		occupiedSpaceY = platformSize.y;	
	}


	override public void ReInit()
	{
		playerConnected = false;
		lifeTimer.Stop();
		base.ReInit();
	}

	override public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		base.Spawn( upBorder, botBorder, minXPos, maxXPos );
		lifeTimer.time = lifeTimerCurve.Evaluate( transf.position.y );
		damageFirstStep = true;
		damageSecondStep = true;
	}

	private void ShowDamaged( Sprite sprite )
	{
		spriteRenderer.sprite = sprite;
		PlayRandomCrackSound();
	}


	override public void PlayerConnected()
	{
		playerConnected = true;
		if( !lifeTimer.active )
		{
			lifeTimer.Start();
		}
	}

	private void PlayRandomCrackSound()
	{
		audioSource.clip = crackSounds[ Random.Range( 0, crackSounds.Length ) ];
		audioSource.Play();
	}

	override public void PlayerDisconnected()
	{
		playerConnected = false;
	}
	
	override protected void OnUpdate () {
		base.OnUpdate();
		if( lifeTimer.active && playerConnected )
		{
			if( lifeTimer.Update( Time.deltaTime ) )
			{
				EffectsManager.PlayEffectAtPos( curParams.onDestroyEffect.getEffectId, transf.position );
				LevelGenerator.RemovePlatformFromActived( this ); 
				ResetObject();
			}

			if( damageFirstStep )
			{
				if( lifeTimer.w >= lifeProcForDamaged )
				{
					ShowDamaged( curParams.damaged );
					damageFirstStep = false;
				}
			}

			if( damageSecondStep )
			{
				if( lifeTimer.w >= lifeProcForDamaged2 )
				{
					ShowDamaged( curParams.damaged2 );
					damageSecondStep = false;
				}
			}
		}
	}
}
