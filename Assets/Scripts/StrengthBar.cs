﻿using UnityEngine;
using System.Collections;

public class StrengthBar : MonoBehaviour {

	private Transform transf;

	void Awake()
	{
		transf = transform;
	}

	Vector3 tmp;
	// Update is called once per frame
	void LateUpdate () {
	if( GlobalManager.isGameActive )
		{
			tmp = transf.localScale;
			tmp.y = Mathf.Clamp( PlayerOnSpringJoint.instance.getRopeStrengh, 0, 1 );
			transf.localScale = tmp;
		}
	}
}
