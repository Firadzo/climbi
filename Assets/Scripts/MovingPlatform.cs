using UnityEngine;
using System.Collections;

[System.Serializable]
public class MovementPoints
{
	public Vector2 from;
	public Vector2 to;
#if UNITY_EDITOR
	public bool draw;
#endif
}

public class MovingPlatform : PlatformBase {

	public 	MovementPoints[] movementPoints;
	public 	float 			moveTime;

	private CTimer 			movementTimer;
	private Rigidbody2D		thisRB2D;
	private Vector2			velocityVector;

	// Use this for initialization
	override protected void Awake()
	{	thisRB2D = GetComponent<Rigidbody2D>();
		movementTimer = new CTimer( moveTime, false );
		base.Awake();
	}

	override public void ResetObject () {
		thisRB2D.velocity = Vector2.zero;
		movementTimer.Stop();
		base.ResetObject();
	}

	override public void Activate( Vector3 pos )
	{
		base.Activate( pos );
		MovementPoints curPoints = movementPoints[ Random.Range( 0, movementPoints.Length ) ];
		if( curPoints.to.y == 0 )//при учете что оффсета от точки старта вниз нет, то есть ( curPoints.from.y == 0 )
		{
			occupiedSpaceY = platformSize.y;
		}
		else
		{
			occupiedSpaceY = curPoints.to.y;
		}
		Vector2 		from;
		Vector2 		to;
		from = pos;
		if( curPoints.from.x != 0 )// что бы при движении не вылазит за границы экрана
		{
			from.x = curPoints.from.x;
			from.x -= platformSize.x * Mathf.Sign( from.x );
		}
	/*	if( curPoints.from.y != 0 )
		{
			from.y += curPoints.from.y;
			from.y -= platformSize.y * Mathf.Sign( curPoints.from.y );
		}*/
		to = pos;
		if( curPoints.to.x != 0 )
		{
			to.x = curPoints.to.x;
			to.x -= platformSize.x * Mathf.Sign( to.x );
		}
		to.y += curPoints.to.y;
		/*if( curPoints.to.y != 0 )
		{
			to.y += curPoints.to.y;
			to.y -= platformSize.y * Mathf.Sign( curPoints.to.y );
		}*/
		transf.position = from;
		velocityVector = ( to - from ).normalized;
		velocityVector *= Vector2.Distance( from, to ) / moveTime;
		thisRB2D.velocity = velocityVector;
		movementTimer.Start();
	}
	
	// Update is called once per frame
	override protected void OnUpdate () {
		base.OnUpdate();
		if( movementTimer.active )
		{
			if( movementTimer.Update( Time.deltaTime ) )
			{
				thisRB2D.velocity *= -1f;
				movementTimer.Start();
			}
		//	transf.position = Vector3.Lerp( from, to, movementTimer.w );
		}
	}
#if UNITY_EDITOR
	override protected void OnDrawGizmosSelected()
	{
		base.OnDrawGizmosSelected();
		if( movementPoints != null )
		{
			for( int i = 0; i < movementPoints.Length; i++ )
			{
				if( movementPoints[ i ].draw )
				{
					Gizmos.DrawLine( (Vector3)movementPoints[ i ].from, (Vector3)movementPoints[ i ].to );
				}
			}
		}
	}
#endif
}
