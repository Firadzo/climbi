﻿
using UnityEngine;
using System.Collections;

public class PlayerOnSpringJoint : MonoBehaviour {

	public 	Animator 		flyUpSuitA;
	public 	Rope			rope;
	public 	float 			restartHeight;
	private float 			curRestartHeight;
	public 	float			heightForForceAdd;
	private float			nextHeightForForceAdd;
	public 	Vector2			minMaxRotationForce;
	public 	float			forceAdd;
	private	float 			curForce		= 100f;
	public 	float 			startImpulse 	= 1000f;
	public 	Transform		ropeConnectionTR;
	public	CoinMagnet		coinMagnet;

	private DistanceJoint2D distanceJoint;
	private Rigidbody2D		lastPlatformRB;
	private Transform		transf; 
	private bool 			needDisconnect;
	private Transform		lastPlatfTransf;
	private bool 			firstJump;
	private Vector3 		startPos;
	private float			curScoreHeight;
	private float			startScoreHeight;
	private bool			canAddScore;
	private Vector3 		mousePos;
	private PlatformBase	curPlatformController;
	private GameObject 		ropeConnectionObj;

	private static float 				curStartImpulse;
	private static Rigidbody2D 			rigidBody_2D;
	private static	float 				defaultGravity;
//	private static ESpecialBonusType	activeBonus = ESpecialBonusType.none;
	public 	static PlayerOnSpringJoint 	instance;
	public	float	ropeStrength;
	private	float 	curTension;
	private bool	inFlyUpState;
	private bool	stunned;

	public	GameObject	stunnedEffect;
	public 	float 		stunnedTime;
	private CTimer		stunnedTimer;

	public float flyingRotSpeed;
	public Vector2 flyingRotationBorders;

	RaycastHit2D hit;
	Vector3 tmpVec3;

	public float getRopeStrengh
	{
		get {
			if( needDisconnect )
			{
				return curTension / ropeStrength;
			}
			else
			{
				return 0;
			}
		}
	}

	// Use this for initialization
	void Awake () {
		//flyUpSuitA.Play(
		stunnedTimer = new CTimer( stunnedTime, false );
		instance = this;
		firstJump = true;
		curStartImpulse = startImpulse;
		ropeConnectionObj = ropeConnectionTR.gameObject;
		rigidBody_2D = GetComponent< Rigidbody2D >();
		distanceJoint = GetComponent< DistanceJoint2D >();
		defaultGravity = rigidBody_2D.gravityScale;
		distanceJoint.enabled = false;
		transf = transform;
		startPos = transf.position;
		curRestartHeight = startPos.y - restartHeight;
		nextHeightForForceAdd = startPos.y + heightForForceAdd;
		curForce = minMaxRotationForce.x;
	}

	static bool checkReaction;//for achievement
	public static void HitedByFallinStone()
	{
		instance.LoseConnection();
		Stun();
		if( !instance.inFlyUpState )
		{
			checkReaction = true;
		}
	}

	public static void Stun()
	{
		if( instance.stunned ){ return; }
		instance.stunnedTimer.Start();
		instance.stunned = true;
		instance.stunnedEffect.SetActive( true );
	}

	private Vector2 savedVelocity;
	public void Pause()
	{
		savedVelocity = rigidBody_2D.velocity;
		rigidBody_2D.velocity = Vector2.zero;
		if( needDisconnect )
		{
			distanceJoint.enabled = false;
		}
		rigidBody_2D.isKinematic = true;
	}

	public static void EnableBoost( EBoostType boost, bool enable )
	{
		switch ( boost )
		{
		case EBoostType.lowGravity:
			defaultGravity += enable ? -ActiveBonusManager.getLowGravityPower : ActiveBonusManager.getLowGravityPower; 
			rigidBody_2D.gravityScale = defaultGravity;
			break;
		case EBoostType.coinMagnet:
			instance.coinMagnet.EnableCoinMagnet( enable );
			break;
		case EBoostType.shield:
			break;
		case EBoostType.startJump:
			curStartImpulse = enable ? instance.startImpulse * 2f : instance.startImpulse;
			break;
		}
	}

	public static void ActivateBonus( ESpecialBonusType bonusType, bool activate )
	{
		switch( bonusType )
		{
			case ESpecialBonusType.lowGravity:
				if( instance.inFlyUpState ){ return; }
				rigidBody_2D.gravityScale = activate ? defaultGravity - ActiveBonusManager.getLowGravityPower : defaultGravity;
				break;
			case ESpecialBonusType.coinMagnet:
				instance.coinMagnet.EnableCoinMagnet( activate );
				break;
			case ESpecialBonusType.shield:
				break;
			case ESpecialBonusType.flyUp:
				instance.inFlyUpState = activate;
				if( !activate )
				{
					instance.curRestartHeight = instance.transf.position.y - instance.restartHeight;
					rigidBody_2D.gravityScale = defaultGravity;		
					rigidBody_2D.drag = 0;
					instance.flyUpSuitA.Play("SuitOut");
				}
				else
				{
					checkReaction = false;
					instance.flyUpSuitA.Play("SuitUp");
					rigidBody_2D.gravityScale = 0;
					rigidBody_2D.drag = 4f;
					if( instance.needDisconnect )
					{
						instance.LoseConnection();
					}
					else if( instance.rope.isMoving )
					{
						instance.rope.Reset();
					}
				}
				break;
		/*	case ESpecialBonusType.none:
				if( activeBonus == ESpecialBonusType.lowGravity )
				{
					rigidBody_2D.gravityScale = defaultGravity;
				}
				else if( activeBonus == ESpecialBonusType.coinMagnet )
				{
					instance.coinMagnet.EnableCoinMagnet( false );
				}
				break;*/
		}
	//	activeBonus = bonusType;

	}

	public void Resume()
	{
		if( needDisconnect )
		{
			distanceJoint.enabled = true;
		}
		rigidBody_2D.isKinematic = false;
		rigidBody_2D.velocity = savedVelocity;
	}

	public bool connectedToPlatform
	{
		get{ return needDisconnect;}
	}

	public static Rigidbody2D GetRigidBody
	{
		get{ return rigidBody_2D;}
	}

	public static Vector2 GetVelocity
	{
		get { return rigidBody_2D.velocity; }
	}

	public void LoseConnection( PlatformBase checkPlatform )
	{
		if( curPlatformController == checkPlatform )
		{
			LoseConnection();
		}
	}

	public void LoseConnection()
	{
	//	startTouching = false;
		curTension = 0;
		distanceJoint.enabled = false;
		rope.MoveBack();
		//distanceJoint.enabled = false;
		needDisconnect = false;
		if( curPlatformController != null )
		{
			curPlatformController.PlayerDisconnected();
		}
		curPlatformController = null;
		ropeConnectionObj.SetActive( false );
		ropeConnectionTR.transform.parent = transf;
		canAddScore = true;
		startScoreHeight = transf.position.y;
		curScoreHeight = startScoreHeight;
		GlobalManager.EnableBorders( true );
	}
	

	private void SetupRopeConnection()
	{
		ropeConnectionTR.transform.parent = lastPlatfTransf;
		ropeConnectionTR.localPosition = new Vector3( distanceJoint.connectedAnchor.x, distanceJoint.connectedAnchor.y, 1f );
		ropeConnectionTR.localEulerAngles = new Vector3( 0, 0, curPlatformController.GetRopeConnectionAngle( ropeConnectionTR.position ) );
		ropeConnectionObj.SetActive( true );
	}

	RaycastHit2D[] hits;
	public bool ConnectToPlatform( Collider2D platfCollider )
	{
		if (platfCollider == null) {
			return false;
		}
		if( !needDisconnect )
		{
			//mousePos.x += Mathf.Sign( platfCollider.transform.position.x - mousePos.x ) * 0.15f; 
			hits = Physics2D.RaycastAll( ( Vector2 )rope.getPosition, ( Vector2 )( ( mousePos - rope.getPosition ) ).normalized, Mathf.Infinity, GlobalManager.platforms_mask ); 
			//hit = Physics2D.Raycast( ( Vector2 )platfPos, ( Vector2 )( ( platfPos - transf.position ) ).normalized, rope.maxRopeLength, GlobalManager.platforms_mask ); 
			for( int i = 0; i < hits.Length; i++ )
			{
				if( hits[ i ].collider != null )
				{
					if(  hits[ i ].collider == platfCollider )
					{
						if( canAddScore )
						{
							ScoreManager.AddScore( curScoreHeight - startScoreHeight );
							canAddScore = false;
						}
						lastPlatformRB = hits[ i ].collider.GetComponent< Rigidbody2D >();
						curPlatformController = lastPlatformRB.GetComponent< PlatformBase >();
						curPlatformController.PlayerConnected();
						lastPlatfTransf = hits[ i ].collider.transform;
						distanceJoint.connectedBody = lastPlatformRB;
						distanceJoint.connectedAnchor = (Vector2)hits[ i ].point - ( Vector2 )lastPlatfTransf.position ;
						SetupRopeConnection();
						distanceJoint.distance 	  = Vector2.Distance( ( (Vector2)transf.position + distanceJoint.anchor ), hits[ i ].point );
						distanceJoint.enabled 	  = true;
						needDisconnect = true;
						GlobalManager.EnableBorders( false );
						if( checkReaction )
						{
#if UNITY_ANDROID
							GlobalManager.PerfectReactionAchievement();
#endif
							checkReaction = false;
						}
						return true;
					}
				}
			}
			//Debug.DrawLine( rope.getPosition, mousePos, Color.blue, 100f );
			Debug.DrawLine( rope.getPosition, mousePos );
			//Debug.LogError("");
		/*	curTension = 0;
			distanceJoint.enabled = false;
			rope.MoveBack();
			needDisconnect = false;
			if( curPlatformController != null )
			{
				curPlatformController.PlayerDisconnected();
			}
			curPlatformController = null;
			ropeConnectionObj.SetActive( false );
			ropeConnectionTR.transform.parent = transf;*/
		}
		return false;
	}

	public void MouseDown()
	{
		if( rope.isMoving || inFlyUpState || stunned )
		{
			return;
		}
		if( needDisconnect )
		{
			LoseConnection();
			return;
		}

		if( firstJump )
		{
			GlobalManager.ActivateStartSpring();
			rigidBody_2D.AddForce( Vector2.up * curStartImpulse, ForceMode2D.Impulse );
			firstJump = false;
			return;
		}
		mousePos = Camera.main.ScreenToWorldPoint( Input.mousePosition );
		rope.ThrowRope();
		//Debug.LogError("");
		//Debug.DrawLine( transf.position, ( Camera.main.ScreenToWorldPoint( Input.mousePosition ) - transf.position ).normalized * distance, Color.red );
	}

	public void Reset()
	{
		checkReaction = false;
		curRestartHeight = startPos.y - restartHeight;
		LoseConnection();
		rope.Reset();
		rigidBody_2D.Sleep();
		transf.position = startPos;
		transf.localEulerAngles = Vector3.zero;
		canAddScore = false;
		nextHeightForForceAdd = startPos.y + heightForForceAdd;
		curForce = minMaxRotationForce.x;
		firstJump = true;
		if( stunned )
		{
			stunned  = false;
			stunnedEffect.SetActive( false );
			stunnedTimer.Stop();
		}
	}

	private void GameOver()
	{
		//Reset();
		GlobalManager.GameOver();
	}


//	private bool startTouching;
//	private Vector2 statTouchPos;
//	private float minTouchLength = 50f;
	// Update is called once per frame
	void Update () {

#if UNITY_EDITOR
		if( Input.GetKeyDown( KeyCode.G ) )
		{
			//rigidBody_2D.gravityScale *= -1f;
			ActivateBonus( ESpecialBonusType.flyUp, !inFlyUpState );
		}
#endif

		if( !GlobalManager.isGameActive )
		{
			return;
		}
		if( stunned )
		{
			if( stunnedTimer.Update( Time.deltaTime ) )
			{
				stunned = false;
				stunnedEffect.SetActive( false );
			}
		}

		if( transf.position.y < curRestartHeight && GlobalManager.isGameStarted )
		{
			GameOver();
			//GlobalManager.ResetGame();
		}

		if( inFlyUpState )
		{
			tmpVec3 = transf.localEulerAngles;
			if( transf.localEulerAngles.z > flyingRotationBorders.x && transf.localEulerAngles.z < 180f )
			{
				tmpVec3.z += flyingRotSpeed * Time.deltaTime;
			}
			else if( transf.localEulerAngles.z < flyingRotationBorders.y && transf.localEulerAngles.z > 180f )
			{
				tmpVec3.z += -flyingRotSpeed * Time.deltaTime;
			}
			else
			{
#if UNITY_EDITOR || UNITY_STANDALONE_WIN
				if( Camera.main.ScreenToWorldPoint( Input.mousePosition ).x < 0 )
				{
					tmpVec3.z = Mathf.Lerp( 0, flyingRotationBorders.x,  Mathf.Abs( Camera.main.ScreenToWorldPoint( Input.mousePosition ).x ) / GlobalManager.DEF_HALF_SCREEN_WIDTH );
				}
				else
				{
					tmpVec3.z = Mathf.Lerp( 360f, flyingRotationBorders.y, Mathf.Abs( Camera.main.ScreenToWorldPoint( Input.mousePosition ).x ) / GlobalManager.DEF_HALF_SCREEN_WIDTH );
				}
			//tmpVec3.z += Mathf.InverseLerp( 0, GlobalManager.DEF_HALF_SCREEN_WIDTH, Mathf.Abs( Camera.main.ScreenToWorldPoint( Input.mousePosition ).x ) ) 
			//	* flyingRotSpeed *Time.deltaTime * ( Mathf.Sign(Camera.main.ScreenToWorldPoint( Input.mousePosition ).x ) );
#else
				if( Input.acceleration.x < 0 )
				{
					tmpVec3.z = Mathf.Lerp( 0, flyingRotationBorders.x, Mathf.Abs( Input.acceleration.x ) * 3f );
				}
				else
				{
					tmpVec3.z = Mathf.Lerp( 360f, flyingRotationBorders.y, Input.acceleration.x * 3f );
				}
			//tmpVec3.z += Input.acceleration.x * flyintRotSpeed * Time.deltaTime;
#endif
			}
			transf.localEulerAngles = tmpVec3;
			return;
		}

		if( curForce < minMaxRotationForce.y )//добавление силы вращение
		{
			if( transf.position.y >= nextHeightForForceAdd )
			{
				nextHeightForForceAdd += heightForForceAdd;
				curForce += forceAdd;
			}
		}

		if( needDisconnect )
		{

			curRestartHeight = transf.position.y - restartHeight;
			if( curPlatformController.IsDestroyed )
			{
				LoseConnection();
			}
			curTension = Mathf.Max( Mathf.Abs( rigidBody_2D.velocity.x ), Mathf.Abs( rigidBody_2D.velocity.y ) );
		}
		else
		{

			if( canAddScore )
			{
				if( curScoreHeight <= transf.position.y )
				{
					curScoreHeight = transf.position.y;
				}
				else
				{
					ScoreManager.AddScore( curScoreHeight - startScoreHeight );
					canAddScore = false;
				}
			}
		}
	}

	void OnCollisionEnter2D( Collision2D other )
	{
		if( GlobalManager.isGameStarted )
		{
			if( other.collider.CompareTag ( "ground" ) )
			{
				GameOver();
			}
		}
	}

	Vector2 forceVector = new Vector2();

	public Vector2 maxVelocityWhileFlying;
	public float flyingForce;
	const float ROPE_OFFSET_FROM_PLAYER = 40f;
	void FixedUpdate()
	{
		if( GlobalManager.isGameActive )
		{
//			if( Input.GetKeyDown( KeyCode.B ) )
//			{
//				Debug.LogError("");
//				HitedByFallinStone();
//			}

			if( inFlyUpState )
			{
				if( stunned )
				{
					Vector2 stunnedVelocityLimits = maxVelocityWhileFlying /2f;
					if( rigidBody_2D.velocity.y < stunnedVelocityLimits.y && Mathf.Abs( rigidBody_2D.velocity.x ) < stunnedVelocityLimits.x )
					{		
						rigidBody_2D.AddRelativeForce( Vector2.up * flyingForce/2f, ForceMode2D.Force );
					}
				}
				else
				{
					if( rigidBody_2D.velocity.y < maxVelocityWhileFlying.y && Mathf.Abs( rigidBody_2D.velocity.x ) < maxVelocityWhileFlying.x )
					{		
						rigidBody_2D.AddRelativeForce( Vector2.up * flyingForce, ForceMode2D.Force );
					}
				}
			}
			if( needDisconnect )
			{
				if( stunned ){ return; }
				float dist = Vector2.Distance( transf.position, ropeConnectionTR.position ) - ROPE_OFFSET_FROM_PLAYER;
				if( dist - distanceJoint.distance  > 0 )//блокировка растяжение веревки при вращении
				{
					//Debug.Log( dist );
					Vector3 dir = ( ropeConnectionTR.position - transf.position ).normalized;
					dir *= dist - distanceJoint.distance;
					Vector3 t = transf.position;
					t += dir;
					t.z = transf.position.z;
					transf.position = t;
				}
				//ограничение силы вращения
				curTension = Mathf.Max( Mathf.Abs( rigidBody_2D.velocity.x ), Mathf.Abs( rigidBody_2D.velocity.y ) );
				if( curTension >= ropeStrength )
				{
					return;
				}
				forceVector = Vector2.zero;
				forceVector.x = ( (rigidBody_2D.velocity.normalized).x );
				forceVector *= curForce;
				rigidBody_2D.AddForce( forceVector, ForceMode2D.Force );
				//Debug.Log( rigidBody_2D.velocity );
			}
		}
	}


	#if UNITY_EDITOR
	Vector3 mainCamPos;
	void OnDrawGizmosSelected()
	{
		mainCamPos = Camera.main.transform.position;
		Gizmos.color = Color.green;
		Gizmos.DrawLine( new Vector3( mainCamPos.x - 512f, transform.position.y - restartHeight, mainCamPos.z + 10f ),
		                new Vector3( mainCamPos.x + 512f, transform.position.y - restartHeight, mainCamPos.z + 10f ) );
		
	}
	#endif
}
