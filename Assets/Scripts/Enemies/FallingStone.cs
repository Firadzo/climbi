﻿using UnityEngine;
using System.Collections;

public class FallingStone : Enemy {

	public	AnimationCurve	warningTimeCurve;		
	public 	InGameEffect	onCollisionEffect;
	public 	float 			minHeightForDisable;
	public 	float 			spawnYOffset;
	
	private Rigidbody2D	thisRB;
	private Vector3		tempPos;
	private bool 		isActived;
	private AudioSource	audioSource;
	// Use this for initialization
	override protected void Awake () {
		base.Awake();
		audioSource = GetComponent<AudioSource>();
		SoundManager.RegisterAudioSource( audioSource );
		thisRB = GetComponent<Rigidbody2D>();
		isActived = false;
		obj.SetActive( false );
	}


	void OnCollisionEnter2D( Collision2D collision )
	{
		if( isActived )
		{
			if( collision.gameObject.CompareTag("player") )
			{
				PlayerOnSpringJoint.HitedByFallinStone();
				Vector3 t = transf.position;
				t.z = GlobalManager.GetPlayerPos.z - 0.5f;
				EffectsManager.PlayEffectAtPos( onCollisionEffect.getEffectId, t );
				DisableEnemy();
			}
			else if( collision.gameObject.CompareTag( "ground") )
			{
				EffectsManager.PlayEffectAtPos( onCollisionEffect.getEffectId, transf.position );
				AddScore();
				DisableEnemy();
			}
		}
	}

	private Vector2	savedVelocity;
	public override void OnPause ()
	{
		savedVelocity = thisRB.velocity;
		thisRB.velocity = Vector2.zero;
		thisRB.Sleep();
	}

	public override void OnResume ()
	{
		thisRB.velocity = savedVelocity;
	}

	override protected void AddScore()
	{

#if UNITY_ANDROID
		GlobalManager.CantTouchMeAchievement();
#endif
		ScoreManager.AddScore( scoreForEnemy );
		EffectsManager.PlayScoreEffect( GlobalManager.GetPlayerPos, scoreForEnemy );
	}

	override public void DisableEnemy()
	{
		thisRB.velocity = Vector2.zero;
		isActived = false;
		obj.SetActive( false );
	}

	override public void Spawn()
	{
		WarningsManager.ShowWarningOfType( WarningsType.fallingStone, warningTimeCurve.Evaluate( GlobalManager.CameraHeight ), ThrowStone );
	}

	private void ThrowStone()
	{
		isActived = true;
		tempPos = GlobalManager.mainCamTransf.position;
		tempPos.x = GlobalManager.GetPlayerPos.x;
		tempPos.y += GlobalManager.half_screen_height + spawnYOffset;
		tempPos.z = zPos;
		thisRB.gravityScale = difficultyCurve.Evaluate( GlobalManager.CameraHeight );
		transf.position = tempPos;
		obj.SetActive( true );
		audioSource.Play();
	}

	override protected void OnUpdate()
	{
		if( isActived )
		{	
			base.OnUpdate();
			if( transf.position.y + minHeightForDisable <= GlobalManager.CameraHeight)
			{
				AddScore();
				DisableEnemy();
			}
		}
	}
}
