﻿using UnityEngine;
using System.Collections;

public enum EnemyStates{ active, notActive, killed }
public class FlyingCritter : Enemy {

	public	InGameEffect hitedEffect;	
	public 	float 		speed;
	public 	float 		onKillImpulse;
	public 	float 		minHeightForDisable;
	public 	float		objHeight;

	public 	RuntimeAnimatorController[]	crittersAnimators;
	static 	int							curIndex = 0;

	private Animator	animator;

	private EnemyStates enemyState;
	private Vector3 	tempPos;
	private float 		direction;
	private Rigidbody2D	thisRb;	

	public	AudioClip[]	deathClips;
	private AudioSource	audioSource;
	

	// Use this for initialization
	override protected void Awake () {
		base.Awake();
		animator = GetComponent<Animator>();
		audioSource = GetComponent<AudioSource>();
		SoundManager.RegisterAudioSource( audioSource );

		thisRb = GetComponent< Rigidbody2D >();
		thisRb.isKinematic = true;
		enemyState = EnemyStates.notActive;
		obj.SetActive( false );
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		if( enemyState == EnemyStates.active ) 
		{
			if( other.gameObject.CompareTag("player") )
			{
				EffectsManager.PlayEffectAtPos( hitedEffect.getEffectId, transf.position );
				Killed();
			}
			else if( other.gameObject.CompareTag("ground") )
			{
				DisableEnemy();
			}
		}
	}

	private void Flip()
	{
		direction *= -1f;
		tempPos = transf.localScale;
		tempPos.x = direction;
		transf.localScale = tempPos;
	}

	private void Killed()
	{
		audioSource.clip = deathClips[ Random.Range( 0, deathClips.Length ) ];
		audioSource.Play();
		thisRb.isKinematic = false; 
		thisRb.AddForce( new Vector2( Random.Range( -0.5f,0.5f ), 1f ) * onKillImpulse, ForceMode2D.Impulse ); 
		tempPos = transf.localEulerAngles;
		tempPos.z = 180;
		transf.localEulerAngles = tempPos;
		AddScore();
		enemyState = EnemyStates.killed;
		animator.SetBool( "killed", true );
		#if UNITY_ANDROID
		GlobalManager.CrittersAchievement();
		#endif
	}

	private Vector2	savedVelocity;
	public override void OnPause ()
	{
		animator.enabled = false;
		if( enemyState == EnemyStates.killed )
			{
				savedVelocity = thisRb.velocity;
				thisRb.velocity = Vector2.zero;
				thisRb.isKinematic = true;
			}
	}
	
	public override void OnResume ()
	{
		animator.enabled = true;
		if( enemyState == EnemyStates.killed )
		{
			thisRb.velocity = savedVelocity;
			thisRb.isKinematic = false;
		}
	}


	/*override public void Spawn()
	{
		thisRb.isKinematic = true;
		transf.localEulerAngles = Vector3.zero;
		tempPos = GlobalManager.mainCamTransf.position;
		tempPos.y = LevelGenerator.instance.GetYPosForEnemy( tempPos.y + spawnYOffset );
		direction = 1f;
		if( Random.value > 0.5f )
		{
			direction = -1f;
		}
		tempPos.x += GlobalManager.DEF_HALF_SCREEN_WIDTH * direction;
		tempPos.z = zPos;
		transf.position = tempPos;
		Flip();
		enemyState = EnemyStates.active;
		obj.SetActive( true );
		animator.SetBool( "killed", false );
	}*/

	override public void Spawn( float upBorder, float botBorder )
	{
		animator.runtimeAnimatorController = crittersAnimators[ curIndex ];
		curIndex ++;
		if( curIndex == crittersAnimators.Length ){ curIndex = 0; }

		thisRb.isKinematic = true;
		transf.localEulerAngles = Vector3.zero;
		tempPos = Vector3.zero;
		tempPos.y = Random.Range( upBorder - objHeight, botBorder + objHeight );// .instance.GetYPosForEnemy( tempPos.y + spawnYOffset );
		direction = 1f;
		if( Random.value > 0.5f )
		{
			direction = -1f;
		}
		tempPos.x += GlobalManager.DEF_HALF_SCREEN_WIDTH * direction;
		tempPos.z = zPos;
		transf.position = tempPos;
		Flip();
		enemyState = EnemyStates.active;
		speed = difficultyCurve.Evaluate( transf.position.y );
		obj.SetActive( true );
		animator.SetBool( "killed", false );
	}

	override public void DisableEnemy()
	{
		thisRb.velocity = Vector2.zero;
		enemyState = EnemyStates.notActive;
		obj.SetActive( false );
	}

	override protected void OnUpdate()
	{
		if( enemyState != EnemyStates.notActive )
		{
			/*tempPos = transf.position;
			tempPos.y -= fallingSpeed * Time.deltaTime;
			transf.position = tempPos;*/
			base.OnUpdate();
			if( transf.position.y + minHeightForDisable <= GlobalManager.CameraHeight )
			{
				DisableEnemy();
			}
		}
		if( enemyState == EnemyStates.active ) 
		{
			tempPos = transf.position;
			tempPos.x += speed * direction * Time.deltaTime;
			transf.position = tempPos;
			if( direction > 0 )
			{
				if( transf.position.x >= GlobalManager.mainCamTransf.position.x + GlobalManager.DEF_HALF_SCREEN_WIDTH )
				{
					Flip();
				}
			}
			else
			{
				if( transf.position.x <= GlobalManager.mainCamTransf.position.x - GlobalManager.DEF_HALF_SCREEN_WIDTH )
				{
					Flip();
				}
			}
		}
	}
}
