﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SpawnInfo
{
	# if UNITY_EDITOR
	[HideInInspector]
	public string 		name;
	public bool			showHeight;
	#endif
	public GameObject[]	enemyPrefab;
	public AnimationCurve  spawnDelayCurve;
	public bool			useSpawner;
	public bool 		useDelayBeforeSpawn;

	public float 		heightBeetwenSpawn;
	public bool 		useHeightBeetwenSpawn;
	private float		lastSpawnHeight;

	public float		activationHeight;
	public int 			precreatedCount;
	//private Enemy[]		enemies;
	private List<Enemy>[]	enemies;
	private CTimer			spawnTimer;


	public void Init()
	{
		if( useDelayBeforeSpawn )
		{
			spawnTimer = new CTimer();
		}
		lastSpawnHeight = activationHeight;
		enemies = new List<Enemy>[ enemyPrefab.Length ];
		//enemies = new Enemy[ enemyPrefab.Length ];

		for( int i = 0; i < enemyPrefab.Length; i++ )
		{
			enemies[ i ] = new List<Enemy>();
			for( int j = 0; j < precreatedCount; j++ )
			{
				InstantiateEnemy( i );
			}
		}

	}

	public void Reset()
	{
		if( useDelayBeforeSpawn )
		{
			spawnTimer.Stop();
		}
		lastSpawnHeight = activationHeight;
		for( int i = 0; i < enemies.Length; i++ )
		{
			for( int j = 0; j < enemies[ i ].Count; j++ )
			{
				if( enemies[ i ][ j ].isActive )
				{
					enemies[ i ][ j ].DisableEnemy();
				}
			}
		}
	}

	private Transform 		tr;
	private void InstantiateEnemy( int index )
	{
		tr = ( GameObject.Instantiate( enemyPrefab[ index ] ) as GameObject ).transform;
		enemies[ index ].Add( tr.GetComponent< Enemy >() );
		tr.parent = EnemySpawner.getEnemiesParent;
	}
	
	private Enemy GetInactiveEnemy( int index )
	{
		for( int i = 0; i < enemies[ index ].Count; i++ )
		{
			if( !enemies [ index ][ i ].isActive )
			{
				return enemies [ index ][ i ];
			}
		}
		InstantiateEnemy( index );
		return enemies [ index ][ enemies[ index ].Count - 1 ];

	}


	public void SpawnEnemy()
	{
		if( useSpawner )
		{
			SpawnManager.RegisterForSpawn( GetInactiveEnemy( Random.Range( 0, enemies.Length ) ), GlobalManager.CameraHeight + GlobalManager.screen_height );
		}
		else
		{
			GetInactiveEnemy( Random.Range( 0, enemies.Length ) ).Spawn();
		}
	}

	public void Pause()
	{
		for( int i = 0; i < enemies.Length; i++ )
		{
			for( int j = 0; j < enemies[ i ].Count; j++ )
			{
				if( enemies[ i ][ j ].isActive )
				{
					enemies[ i ][ j ].OnPause();
				}
			}
		}
	}
	
	public void Resume()
	{
		for( int i = 0; i < enemies.Length; i++ )
		{
			for( int j = 0; j < enemies[ i ].Count; j++ )
			{
				if( enemies[ i ][ j ].isActive )
				{
					enemies[ i ][ j ].OnResume();
				}
			}
		}
	}

	public void Update( )
	{
		if( useDelayBeforeSpawn )
		{
			if( spawnTimer.active )
			{
				if( spawnTimer.Update( Time.deltaTime ) )
				{
					SpawnEnemy();
					spawnTimer.Start( spawnDelayCurve.Evaluate( GlobalManager.CameraHeight ) );
				}
			}
			else
			{
				if( GlobalManager.CameraHeight >= activationHeight )
				{
					spawnTimer.Start( spawnDelayCurve.Evaluate( GlobalManager.CameraHeight ) );
				}
			}
		}
		else if( useHeightBeetwenSpawn )
		{
			if( GlobalManager.CameraHeight >= lastSpawnHeight )
			{
				lastSpawnHeight += heightBeetwenSpawn;
				SpawnEnemy();
			}
		}
	}
}

public class EnemySpawner : MonoBehaviour {
	public SpawnInfo[]		spawnInfo;
	private static Transform transf;
	// Use this for initialization
	void Awake () {
		transf = transform;
	}

	void Start()
	{
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].Init();
		}
	}

	public void PauseEnemies()
	{
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].Pause();
		}
	}

	public void ResumeEnemies()
	{
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].Resume();
		}
	}

	public void ResetEnemies()
	{
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].Reset();
		}
	}

	public static Transform getEnemiesParent
	{
		get { return transf; }
	}

	// Update is called once per frame
	void Update () {
		if( GlobalManager.isGameActive )
		{
			if( Input.GetKeyDown( KeyCode.A ) )
			{
				//Debug.Log(  test.Evaluate( testVal ) );
				spawnInfo[ 1 ].SpawnEnemy();
			}
			for( int i = 0; i < spawnInfo.Length; i++ )
			{
				spawnInfo[ i ].Update( );
			}
		}

	}

	# if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{

		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].name = "";
			if( spawnInfo[ i ].enemyPrefab != null )
			{
				for( int j = 0; j < spawnInfo[ i ].enemyPrefab.Length; j++ )
				{
					spawnInfo[ i ].name += spawnInfo[ i ].enemyPrefab[ j ].name + " ";
				}
			}
			if( spawnInfo[ i ].showHeight )
			{
				Gizmos.color = Color.red;
				Gizmos.DrawLine( new Vector3( -1000f, spawnInfo[ i ].activationHeight, 0 ), new Vector3( 1000f, spawnInfo[ i ].activationHeight, 0 ) );
			}
		}
	}
	#endif

}
