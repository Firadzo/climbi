﻿using UnityEngine;
using System.Collections;

public class MetallBlock : Enemy {

	public float objWidth;
	public float disableOffset;
	//public float forcePower;

	private float 		speed;
	private bool		moveRight;
	static 	Vector2 	movingBorders;
	private Vector2		velocity;
	private Rigidbody2D rb;
	private AudioSource	audioSource;
	public 	AudioClip[]	hitSounds;

	// Use this for initialization
	override  protected void Awake () {
		base.Awake();
		audioSource = GetComponent<AudioSource>();
		SoundManager.RegisterAudioSource( audioSource );
		if( movingBorders == Vector2.zero )
		{
			movingBorders = new Vector2( -GlobalManager.DEF_HALF_SCREEN_WIDTH + objWidth, GlobalManager.DEF_HALF_SCREEN_WIDTH - objWidth );
		}
		rb = GetComponent<Rigidbody2D>();
		obj.SetActive( false );

	}

	private void OnCollisionEnter2D( Collision2D other )
	{
		if( other.gameObject.CompareTag( "player" ) )
		{
			PlayerOnSpringJoint.Stun();
			if( !audioSource.isPlaying )
			{
				audioSource.clip = hitSounds[ Random.Range( 0, hitSounds.Length ) ];
				audioSource.Play();
			}
			//Debug.Log("c");
		//	PlayerOnSpringJoint.GetRigidBody.velocity = Vector2.zero;
		//	PlayerOnSpringJoint.GetRigidBody.AddForce( ( (Vector2)(GlobalManager.GetPlayerPos - transf.position ) ).normalized * forcePower, ForceMode2D.Impulse );
		}
	}

	private void ChangeDirection()
	{
		moveRight = !moveRight;
		velocity = new Vector2( ( moveRight ? speed: -speed ), 0 );
	}

	override public void Spawn( float upBorder, float botBorder )
	{
		Vector3 startPos = Vector3.zero;
		startPos.y = Random.Range( botBorder + objWidth, upBorder - objWidth );
		startPos.z = zPos;
		moveRight = Random.value > 0.5f;
		speed = difficultyCurve.Evaluate( startPos.y );
		velocity = new Vector2( ( moveRight ? speed: -speed ), 0 );
		startPos.x = moveRight ? movingBorders.x : movingBorders.y;
		transf.position = startPos;
		obj.SetActive( true );
		
	}

	override public void OnPause()
	{
		rb.velocity = Vector2.zero;
	}
	
	override public void OnResume()
	{
		
	}

	override public void DisableEnemy()
	{
		obj.SetActive( false );
	}

	private void FixedUpdate()
	{
		if( GlobalManager.isGameActive )
		{
			rb.velocity = velocity;
		}
	}

	override protected void OnUpdate()
	{
		base.OnUpdate();
		if( transf.position.y + disableOffset <= GlobalManager.CameraHeight )
		{
			DisableEnemy();
		}
		if( moveRight )
		{
			if( transf.position.x >= movingBorders.y )
			{
				ChangeDirection();
			}
		}
		else
		{
			if( transf.position.x <= movingBorders.x )
			{
				ChangeDirection();
			}
		}
	}

}
