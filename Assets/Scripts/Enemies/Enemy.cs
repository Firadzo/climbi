﻿using UnityEngine;
using System.Collections;

public class Enemy : ObjBase, ISpawnable {
	//public		HeightParams[]	heightParams;
	public 		int				scoreForEnemy;
	public		Vector2			distToActivateCollider;
	private 	Collider2D		collider_2D;
	public		AnimationCurve	difficultyCurve;

	protected static float zPos;

	virtual public float getObjWidth
	{
		get{ return 0; }
	}

	virtual public bool occupyAllLine
	{
		get { return true; }
	}

	virtual public Vector2 getXOccupied
	{
		get { return Vector2.zero; }
	}

	override protected void Awake()
	{
		base.Awake();
		collider_2D 			= GetComponent<Collider2D>();
		collider_2D.enabled 	= false;

		if( zPos == 0 )
		{
			zPos = GlobalManager.GetZParams( ObjectType.enemy );
		}
	}

	/*virtual protected float GetCurHeightParams()
	{
		for( int i = heightParams.Length - 1; i >= 0; i-- )
		{
			if( transf.position.y >= heightParams[ i ].height )
			{
				return heightParams[ i ].value;
			}
		}
		return 0;
	}*/

	virtual public void Spawn()
	{}

	virtual public void Spawn( float upBorder, float botBorder )
	{

	}

	virtual public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		
	}

	virtual protected void OnUpdate()
	{
		collider_2D.enabled = Mathf.Abs( transf.position.x - GlobalManager.GetPlayerPos.x ) <= distToActivateCollider.x || Mathf.Abs( transf.position.y - GlobalManager.GetPlayerPos.y ) <= distToActivateCollider.y;
	}

	virtual protected void Update()
	{
		if( GlobalManager.isGameActive )
		{
			OnUpdate();
		}
	}

	virtual public void DisableEnemy()
	{}

	virtual protected void AddScore()
	{
		ScoreManager.AddScore( scoreForEnemy );
		EffectsManager.PlayScoreEffect( transf.position, scoreForEnemy );
	}
}
