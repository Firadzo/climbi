using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BonusSpawnInfo
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public	AnimationCurve		spawnCurve;
	public 	GameObject 			bonusPrefab;
	public 	int 				precreatedCount;
	public 	int 				spawnPriority;
	
	private float 				lastSpawnHeight;
	private List<BonusBase> 	bonuses;

	public void Init()
	{
		lastSpawnHeight = spawnCurve.keys[ 0 ].time;
		bonuses = new List<BonusBase>();
		for( int i = 0; i < precreatedCount; i++ )
		{
			InstantiateBonus();
		}
	}

	private void InstantiateBonus()
	{
		bonuses.Add( ( GameObject.Instantiate( bonusPrefab ) as GameObject ).GetComponent<BonusBase>() );
		bonuses[ bonuses.Count - 1 ].transform.parent = BonusManager.transf;
	}
			
	public void Reset()
	{
		lastSpawnHeight = spawnCurve.keys[ 0 ].time;
		for( int i = 0; i < bonuses.Count; i++ )
		{
			bonuses[ i ].ResetObject();
		}
	}

	public void Pause()
	{
		for( int i = 0; i < bonuses.Count; i++ )
		{
			if( bonuses[ i ].isActive )
			{
				bonuses[ i ].OnPause();
			}
		}
	}

	public void Resume()
	{
		for( int i = 0; i < bonuses.Count; i++ )
		{
			if( bonuses[ i ].isActive )
			{
				bonuses[ i ].OnResume();
			}
		}
	}

	private BonusBase GetInactiveBonus()
	{
		for( int i = 0; i < bonuses.Count; i++ )
		{
			if( !bonuses[ i ].isActive )
			{
				bonuses[ i ].PrepareForSpawn();
				return bonuses[ i ];
			}
		}
		InstantiateBonus();
		bonuses[ bonuses.Count - 1 ].PrepareForSpawn();
		return bonuses[ bonuses.Count - 1 ];

	}

	public void SpawnBonus()
	{
		if( GlobalManager.CameraHeight >= lastSpawnHeight )
		{
			lastSpawnHeight += spawnCurve.Evaluate( GlobalManager.CameraHeight );
			SpawnManager.RegisterForSpawn( GetInactiveBonus(), GlobalManager.CameraHeight + GlobalManager.screen_height );//.Spawn();
		}
	}

}


public class BonusManager : MonoBehaviour {

	public BonusSpawnInfo[]			bonusesSpawnInfo;
	public GameObject				coinPrefab;
	public int						precreatedCoinsCount;

	public 	static Transform		transf;
	private static List<Coin>		coins;
	private static	BonusManager	instance;

	// Use this for initialization
	void Start () {
		instance = this;
		transf = transform;
		coins = new List<Coin>();
		int i;
		for( i = 0; i < precreatedCoinsCount; i++ )
		{
			InstantiateCoin();
		}
		for( i = 0; i < bonusesSpawnInfo.Length; i++ )
		{
			bonusesSpawnInfo[ i ].Init();
		}
	}

	public static void ResetBonuses()
	{
		for( int i = 0; i < instance.bonusesSpawnInfo.Length; i++ )
		{
			instance.bonusesSpawnInfo[ i ].Reset();
		}
	}

	public static void PauseBonuses()
	{
		for( int i = 0; i < instance.bonusesSpawnInfo.Length; i++ )
		{
			instance.bonusesSpawnInfo[ i ].Pause();
		}
	}
	
	public  static void ResumeBonuses()
	{
		for( int i = 0; i < instance.bonusesSpawnInfo.Length; i++ )
		{
			instance.bonusesSpawnInfo[ i ].Resume();
		}
	}


	private static List< Coin > freeCoins = new List<Coin>();
	public static List< Coin > GetCoins( int number )
	{
		freeCoins.Clear();
		int i;
		for( i = 0; i < coins.Count; i++ )
		{
			if( !coins[ i ].isActive )
			{
				freeCoins.Add( coins[ i ] );
				if( freeCoins.Count == number )
				{
					return freeCoins;
				}
			}
		}
		number -= freeCoins.Count;
		for( i = 0; i < number; i++ )
		{
			instance.InstantiateCoin();
			freeCoins.Add( coins[ coins.Count - 1 ] );
		}
		return freeCoins;
	}

	private void InstantiateCoin()
	{
		coins.Add( ( Instantiate( coinPrefab ) as GameObject ).GetComponent<Coin>() );
	}
	int j;
	void Update()
	{
		if( GlobalManager.isGameActive )
		{
			for( j = 0; j < bonusesSpawnInfo.Length; j++ )
			{
				bonusesSpawnInfo[ j ].SpawnBonus();
			}
		}
	}

	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		for( int i = 0; i < bonusesSpawnInfo.Length; i++ )
		{
			if( bonusesSpawnInfo[ i ].bonusPrefab != null )
			{
				bonusesSpawnInfo[ i ].name = bonusesSpawnInfo[ i ].bonusPrefab.name;
			}
		}
		
	}
	#endif
	
}
