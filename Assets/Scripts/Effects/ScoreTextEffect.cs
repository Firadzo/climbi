﻿using UnityEngine;
using System.Collections;

public class ScoreTextEffect : InGameEffect {

	public 	AnimationCurve scaleCurve;
	public 	float		moveTime;
	public 	float 		yOffset;
	private TextMesh 	textMesh;
	private Color		tempColor;
	private Vector3		startPos;
	private Vector3		endPos;
	private CTimer		moveTimer;
	const 	string 		ADD_TO_TEXT = "+ ";
	static 	float 		zPos;


	override protected void Awake () 
	{
		base.Awake();
		textMesh = GetComponent<TextMesh>();
		moveTimer = new CTimer( moveTime, false );
		if( zPos == 0 )
		{
			zPos = GlobalManager.GetZParams( ObjectType.scoreEffect );
		}
		obj.SetActive( false );
	}

	public override void Play (Vector3 atPos, int param)
	{
		//Debug.LogError("");
		textMesh.text = ADD_TO_TEXT + param;
		tempColor = textMesh.color;
		tempColor.a = 1f;
		textMesh.color = tempColor;
		atPos.z = zPos;
		startPos = atPos;
		endPos = atPos;
		endPos.y += yOffset;
		transf.position = atPos;
		moveTimer.Start();
		obj.SetActive( true );

	}

	public override void ResetObject()
	{
		moveTimer.Stop();
		base.ResetObject();
	}

	Vector3 scale = new Vector3();
	// Update is called once per frame
	void Update () {
		if( GlobalManager.isGameActive )
		{
			if( moveTimer.active )
			{
				if( moveTimer.Update( Time.deltaTime ) )
				{
					obj.SetActive( false );
				}
				scale.x = scaleCurve.Evaluate( moveTimer.w );
				scale.y = scale.x;
				transf.localScale = scale;
				tempColor = textMesh.color;
				tempColor.a = Mathf.Lerp( 1f, 0, moveTimer.w );
				textMesh.color = tempColor;
				transf.position = Vector3.Lerp( startPos, endPos, moveTimer.w );
			}
		}
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawLine( new Vector3( transform.position.x - 100f, transform.position.y + yOffset, transform.position.z ), new Vector3( transform.position.x + 100f, transform.position.y + yOffset, transform.position.z ) );
	}
#endif
}
