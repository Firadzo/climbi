﻿using UnityEngine;
using System.Collections;

public class ParticleController : InGameEffect {

	private ParticleSystem 	particles;
	private bool 			checkForDisable;
	private	AudioSource		audio_Source;
	public	AudioClip[]		sounds;

	public bool	autoRegister;
	public bool autoDisable;
	// Use this for initialization
	override protected void Awake () {
		base.Awake();
		audio_Source = GetComponent<AudioSource>();
		particles = GetComponent< ParticleSystem >();
		if( autoRegister )
		{
			EffectsManager.RegisterEffect( this );
		}
		else
		{
			obj.SetActive( false );
		}
		if( audio_Source != null )
		{
			SoundManager.RegisterAudioSource( audio_Source );
		}
	}

	override public void Play( Vector3 atPos )
	{
//		Debug.Log( obj );
		if( !obj.activeSelf )
		{
			obj.SetActive( true );
		}
		//if( !SoundManager.isMuted )
		{
			if( audio_Source != null )
			{
				if( sounds != null && sounds.Length > 0 )
				{
					audio_Source.clip = sounds[ Random.Range( 0, sounds.Length ) ];
				}
				audio_Source.Play();
			}
		}
		transf.position = atPos;
		particles.Play();
		StartCoroutine( ActivateDisableFlag() );
	}

	public void SetStartSize( float startSize )
	{
		particles.startSize = startSize;
	}

	public void Play( int particlesNumber )
	{
		particles.Emit( particlesNumber );
	}

	public void EnableEmission( bool enable )
	{
		particles.enableEmission = enable;
	}

	IEnumerator ActivateDisableFlag()//если вызывать в том же кадре, может сразу же отключить эффект
	{
		yield return new WaitForEndOfFrame();
		if( autoDisable )
		{
			checkForDisable = true;
		}
	}

	public override void OnPause ()
	{
		particles.Pause();
	}
	
	public override void OnResume ()
	{
		particles.Play();
	}

	override public void ResetObject () {
		if (!autoRegister) {
			obj.SetActive (false);
		}
	}

	// Update is called once per frame
	void Update () {
	if( checkForDisable )
		{
			if( particles.particleCount == 0 )
			{
				particles.Clear();
				obj.SetActive( false );
				checkForDisable = false;
			}
		}
	}
}
