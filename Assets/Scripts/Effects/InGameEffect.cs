﻿using UnityEngine;

using System.Collections;

public class InGameEffect : ObjBase {
	[SerializeField][HideInInspector]
	private int effectID;

	public int getEffectId
	{
		get { return effectID; }
	}

	virtual public bool IsReadyToUse()
	{
		if( obj == null )
		{
			obj = gameObject;
		}
		return !obj.activeSelf;
	}

	virtual public void Play( Vector3 atPos )
	{

	}

	virtual public void Play( Vector3 atPos, int param )
	{
		
	}
#if UNITY_EDITOR
	[SerializeField][HideInInspector]
	private string idName;//для проверки, если создали новый эффект из префаба старого, что бы был сгенерирован новый ID
	virtual protected void OnDrawGizmos()
	{
		if( !Application.isPlaying )
		{
			if( effectID == 0 || idName != gameObject.name )
			{
				effectID = GetInstanceID();
				idName = gameObject.name;
				Debug.Log( effectID );
			}
		}
	}
#endif

}
