﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class EffectInfo
{
#if UNITY_EDITOR
	[HideInInspector]
	public string	  name;
#endif
	public GameObject effectPrefab;
	public int 		  precreatedCount = 5;

	private int effectId;

	public int getEffectID
	{
		get { return effectId; }
	}

	public void Init()
	{
		effectId = effectPrefab.GetComponent<InGameEffect>().getEffectId;
	}
}

public class EffectsManager : MonoBehaviour {

	public EffectInfo[] effectsInfo;
	public InGameEffect	scoreEffect;
	static Dictionary< int, List<InGameEffect> > effects = new Dictionary< int, List<InGameEffect>>();
	static EffectsManager instance;
	private Transform transf;
	// Use this for initialization
	void Awake () {
		instance = this;
		transf = transform;

	}

	public static void Init()
	{
		for( int i = 0; i < instance.effectsInfo.Length; i++ )
		{
			instance.effectsInfo[ i ].Init();
			//			Debug.Log( effectsInfo[ i ].getEffectID +"   "+ effectsInfo[ i ].effectPrefab.name );
			for( int j = 0; j < instance.effectsInfo[ i ].precreatedCount; j++ )
			{
				instance.InstantiateEffect( instance.effectsInfo[ i ] );
			}
		}
	}

	private void InstantiateEffect( EffectInfo effectInfo )
	{
		InGameEffect effect;
		effect = ( Instantiate( effectInfo.effectPrefab ) as GameObject ).GetComponent< InGameEffect >(); 
		effect.transform.parent = transf;
		RegisterEffect( effect );
	}

	public static void RegisterEffect( InGameEffect effect )
	{
		if( !effects.ContainsKey( effect.getEffectId ) )
		{
			effects.Add( effect.getEffectId, new List<InGameEffect>() );
		}
		effects[ effect.getEffectId ].Add( effect );
	}

	private void InstantiateEffect( int effectID )
	{
		for( int i = 0; i < effectsInfo.Length; i++ )
		{
			if( effectsInfo[ i ].getEffectID == effectID )
			{
				InstantiateEffect( effectsInfo[ i ] );
				return;
			}
		}
	}

	public static void PlayEffectAtPos( int effectID, Vector3 atPos )
	{
		InGameEffect effect = GetFreeEffectOfType( effectID );
		if( effect != null )
		{
			effect.Play( atPos );
		}
		else
		{
			Debug.LogWarning( "There is no effects of ID "+effectID );
		}
	}

	public static void PlayEffectAtPos( int effectID, Vector3 atPos, int param )
	{
		InGameEffect effect = GetFreeEffectOfType( effectID );
		if( effect != null )
		{
			effect.Play( atPos, param );
		}
		else
		{
			Debug.LogWarning( "There is no effects of ID "+effectID );
		}
	}



	public static void PlayScoreEffect( Vector3 atPos, int score )
	{
		InGameEffect effect = GetFreeEffectOfType( instance.scoreEffect.getEffectId );
		if( effect != null )
		{
			effect.Play( atPos, score );
		}
	}

	private static InGameEffect GetFreeEffectOfType( int effectID )
	{
		foreach( InGameEffect effect in effects[ effectID ] )
		{
			if( effect.IsReadyToUse() )
			{
				return effect;
			}
		}
		instance.InstantiateEffect( effectID );
		return effects[ effectID ][ effects[ effectID ].Count - 1 ];
	}

	public static void PauseEffects()
	{
		foreach( int effId in effects.Keys )
		{
			for( int i = 0; i < effects[ effId ].Count; i++ )
			{
				if( effects[ effId ][ i ].isActive  )
				{
					effects[ effId ][ i ].OnPause();
				}
			}
		}
	}
	
	public  static void ResumeEffects()
	{
		foreach( int effId in effects.Keys )
		{
			for( int i = 0; i < effects[ effId ].Count; i++ )
			{
				if( effects[ effId ][ i ].isActive  )
				{
					effects[ effId ][ i ].OnResume();
				}
			}
		}
	}

	public  static void ResetEffects()
	{
		foreach( int effId in effects.Keys )
		{
			for( int i = 0; i < effects[ effId ].Count; i++ )
			{
				if( effects[ effId ][ i ].isActive )
				{
					effects[ effId ][ i ].ResetObject();
				}
			}
		}
	}

#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		if( effectsInfo != null )
		{
			for( int i = 0; i < effectsInfo.Length; i++ )
			{
				effectsInfo[ i ].name = effectsInfo[ i ].effectPrefab.name;
			}
		}
	}
#endif

}
