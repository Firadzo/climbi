﻿using UnityEngine;
using System.Collections;

public class FallingStoneWarning : WarningBase {
		
	public 	float		smoothTime;
	private float		velocity;
	public 	float 		yOffset;
	private CTimer		warningTimer;
	private Vector3 	tempPos;
	private bool		actived;
	private static float 	zPos;
	private static float	yPos;

	override protected void Awake()
	{
		base.Awake();
		actived = false;
		warningTimer = new CTimer();
		obj.SetActive( false );
	}

	void Start()
	{
		if( zPos == 0 )
		{
			zPos = GlobalManager.GetZParams( ObjectType.warning );
		}
		if( yPos == 0 )
		{
			yPos = ( GlobalManager.getGuiCamPos.y + GlobalManager.half_screen_height ) + yOffset;
		}
	}

	private void SetToPos()
	{
		tempPos = transf.position;
		tempPos.x = Mathf.SmoothDamp( tempPos.x,  GlobalManager.getGuiCamPos.x + GlobalManager.GetPlayerXOffsetFromCam, ref velocity, smoothTime );
		tempPos.y = yPos;
		tempPos.z = zPos;
		transf.position = tempPos;
	}

	override public void Show( float warnTime, AfteWarningCB callBack )
	{
		base.Show( warnTime, callBack );
		warningTimer.Start( warnTime );
		actived = true;
		tempPos = transf.position;
		tempPos.x = GlobalManager.getGuiCamPos.x + GlobalManager.GetPlayerXOffsetFromCam;
		tempPos.y = yPos;
		tempPos.z = zPos;
		transf.position = tempPos;
		obj.SetActive( true );
	}

	override public void DisableWarning()
	{
		warningTimer.Stop();
		afterWarningCB = null;
		actived = false;
		obj.SetActive( false );
	}
	
	void Update () {
		if( GlobalManager.isGamePaused )
		{
			return;
		}
		if( actived )
		{
			if( warningTimer.Update( Time.deltaTime ) )
			{
				if( afterWarningCB != null )
				{
					afterWarningCB();
				}
				DisableWarning();
			}
			else
			{
				SetToPos();
			}
		}
	}
	
}
