﻿using UnityEngine;
using System.Collections;

public class Blinker : MonoBehaviour {
	public 	Color			blinkFrom;
	public 	Color			blinkTo;
	public 	SpriteRenderer 	spriteRender;
	public	float			blinkTime;
	private CTimer			blinkTimer;
	private int				curBlinkNumber;
	private int				blinkNumber;
	private bool			toColor;
	public 	bool			lerp;
	public 	bool			loop;

	void Awake () {
		blinkTimer = new CTimer( blinkTime, false );
	}

	public bool isBlinking
	{
		get{ return blinkTimer.active; }
	}

	public void Blink( int number )
	{
		curBlinkNumber = 0;
		blinkNumber = number;
		blinkTimer.Start();
		toColor = true;
	}

	private void OnEnable()
	{
		if( loop )
		{
			blinkTimer.Start();
		}
	}

	private void OnDisable()
	{
		blinkTimer.Stop();
		spriteRender.color = blinkFrom;
	}

	// Update is called once per frame
	void Update () {
		if( GlobalManager.isGameActive )
		{
			if( blinkTimer.active )
			{
				blinkTimer.Update( Time.deltaTime );
				if( lerp )
				{
					if( toColor )
					{
						spriteRender.color = Color.Lerp( blinkFrom, blinkTo, blinkTimer.w );
					}
					else
					{
						spriteRender.color = Color.Lerp( blinkTo, blinkFrom, blinkTimer.w );
					}
				}
				if( !blinkTimer.active )
				{
					if( toColor )
					{
						spriteRender.color = blinkTo;
					}
					else
					{
						spriteRender.color = blinkFrom;
						curBlinkNumber++;
					}
					if( curBlinkNumber < blinkNumber || loop )
					{
						blinkTimer.Start();
						toColor = !toColor;
					}
				}
			}
		}
	}
}
