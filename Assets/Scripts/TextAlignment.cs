﻿using UnityEngine;
using System.Collections;

public class TextAlignment : MonoBehaviour {

	public	GameObject	disabledObject;
	public 	Transform[]	objects;
	public 	float		step;
	public 	float		startYOnDisabled;

	void OnEnable()
	{
		Vector3 tmp;
		if( disabledObject.activeSelf )
		{
			for( int i = 0; i < objects.Length; i++ )
			{
				tmp = objects[ i ].localPosition;
				tmp.y = step * i;
				objects[ i ].localPosition = tmp;
			}
		}
		else
		{
			for( int i = 0; i < objects.Length; i++ )
			{
				tmp = objects[ i ].localPosition;
				tmp.y = startYOnDisabled + step * i;
				objects[ i ].localPosition = tmp;
			}
		}
	}
}
