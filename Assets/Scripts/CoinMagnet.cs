﻿using UnityEngine;
using System.Collections;

public class CoinMagnet : MonoBehaviour {

	public 	float				emitTime;
	private CTimer				emitTimer;
	private ParticleController	particles;
	private CircleCollider2D	_collider;
	private Transform			transf;
	const 	string				COIN_TAG = "Coin";
	const 	string				MESSAGE_ON_MAGNET = "InMagneticField";
	const	float				DEF_START_SIZE = 350f;
	const 	float				DEF_RADIUS = 150f;

	// Use this for initialization
	void Awake () {
		transf = transform;
		emitTimer = new CTimer( emitTime, false );
		particles = GetComponent<ParticleController>();
		particles.EnableEmission( false ); 
		_collider = GetComponent<CircleCollider2D>();
		_collider.enabled  = false;
	}

	public void SetRadius( float radius )
	{
		_collider.radius = radius;
		particles.SetStartSize( DEF_START_SIZE * ( radius / DEF_RADIUS ) );
	}

	public void EnableCoinMagnet( bool enable )
	{
		_collider.enabled = enable;
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		if( other.CompareTag( COIN_TAG ) )
		{
			//particles.Play( 1 );
			particles.EnableEmission( true );
			other.SendMessage( MESSAGE_ON_MAGNET );
			emitTimer.Start();
		}
	}

	void LateUpdate()
	{

		if( GlobalManager.isGameActive )
		{
			if( _collider.enabled )
			{
				transf.position = GlobalManager.GetPlayerPos;
			}
			if( emitTimer.active )
			{
				if( emitTimer.Update( Time.deltaTime ) )
				{
					particles.EnableEmission( false );
				}
			}	
		}
	}
}
