﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class BezierCurveQube
{
	public Vector3 point1;
	public Vector3 point2;
	public Vector3 point3;
	public Vector3 point4;
	
	private float curveLength;
	private float t1;
	private Vector3[] points;
	private int nextPointIndex;
	private Vector3 lastPoint;
	private bool pathComplete;
	private float distMoved;
	
	const int pointsCount = 50;
	
	public float getCurveLength
	{
		get { return curveLength; }
	}
	
	public float getDistanceRatio
	{
		get { return distMoved / curveLength;}
	}
	
	public bool isPathComplete
	{
		get{ return pathComplete; }
	}
	
	public BezierCurveQube( )
	{
	}
	
	public BezierCurveQube( Vector3 point_1, Vector3 point_2, Vector3 point_3, Vector3 point_4 )
	{
		Init( point_1, point_2, point_3, point_4 );
	}
	
	public void Init( Vector3 point_1, Vector3 point_2, Vector3 point_3, Vector3 point_4 )
	{
		point1 = point_1;
		point2 = point_2;
		point3 = point_3;
		point4 = point_4;
		CalculateCurveLength();
	}
	
	public void Reset()
	{
		pathComplete = false;
		distMoved = 0;
		lastPoint = points[ 0 ];
		nextPointIndex = 1;
	}
	
	public void CalculateCurveLength()
	{
		Vector3 curPoint = Vector3.zero;
		Vector3 nextPoint;
		curveLength = 0;
		points = new Vector3[ pointsCount ];
		for( int i = 0; i < pointsCount; i++ )
		{
			nextPoint = Evaluate( i / ( pointsCount - 1f ) );
			points[ i ] = nextPoint;
			if( i > 0 )
			{
				curveLength += Vector3.Distance( curPoint, nextPoint );
			}
			curPoint = nextPoint;
		}
		Reset();
		//Debug.Log( curveLength );
	}
	
	float distBetweenPoints;
	float distRatioBetweenPoints;
	public Vector3 MoveByDistance( float speed )
	{
		distMoved += speed;
		return CalculateNextPoint( speed );
	}
	
	private Vector3 CalculateNextPoint( float speed )
	{
		distBetweenPoints = Vector3.Distance( lastPoint, points[ nextPointIndex ] );
		if( speed <= distBetweenPoints )
		{
			distRatioBetweenPoints = speed / distBetweenPoints;
			lastPoint = Vector3.Lerp( lastPoint, points[ nextPointIndex ], distRatioBetweenPoints );
			return lastPoint;
		}
		else
		{
			if( nextPointIndex == pointsCount - 1 )
			{
				pathComplete = true;
				return points[ nextPointIndex ];
			}
			else
			{
				nextPointIndex ++;
				return CalculateNextPoint( speed );
			}
		}
	}
	
	public Vector3 Evaluate( float t )
	{
		t1 = 1f - t;
		return t1 * t1 * t1 * point1 + 3 * t * t1 * t1 * point2
			+ 3 * t * t * t1 * point3 + t * t * t * point4;
	}
}
