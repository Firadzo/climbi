﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	public 	float		heightOffSet;
	public 	float		smoothTime;
	private Vector3 	tmpVec3;
	private float		curYVel;
	public 	Transform	followedTransf;
	public 	Transform	backgroundScrollDown;

	private static	float			startFollowHeight;
	private static 	Transform 		transf;
	private static	Vector3			startPos;
		

	void Start()
	{
		transf = transform;
		startPos = transf.position;
		startPos.y += GlobalManager.get_height_difference;
		startFollowHeight = startPos.y - heightOffSet;
		transf.position = startPos;
	}

	public static void Reset()
	{
		transf.position = startPos;
	}

	// Update is called once per frame
	void LateUpdate () {
		if( GlobalManager.isGameStarted )
		{
			tmpVec3 = transf.position;
			if( followedTransf.position.y >= startFollowHeight ) //&& !offsetChangeTimer.active && curOffset != heightOffSet )
			{
				/*	offsetChangeTimer.Start();
			}
			if( offsetChangeTimer.active )
			{
				offsetChangeTimer.Update( Time.deltaTime );
				curOffset = Mathf.Lerp( startOffset, heightOffSet, offsetChangeTimer.w );
			}
			tmpVec3.y = Mathf.SmoothDamp( tmpVec3.y, followedTransf.position.y + curOffset, ref curYVel, smoothTime );*/
			tmpVec3.y = Mathf.SmoothDamp( tmpVec3.y, followedTransf.position.y + heightOffSet, ref curYVel, smoothTime );
			transf.position = tmpVec3;
			}
		}
		backgroundScrollDown.position = transf.position;
	}
	
}
