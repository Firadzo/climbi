﻿using UnityEngine;
using System.Collections;

public class ShopPageButton : ButtonBase {

	public 	EShopPage 	pageType;
	public	Sprite	 	up;
	public 	Sprite	 	down;
		
	private SpriteRenderer 	sprite_Renderer;

	private SpriteRenderer getSpriteRenderer
	{
		get {
			if( sprite_Renderer == null )
			{
				sprite_Renderer = GetComponent<SpriteRenderer>();
			}
			return sprite_Renderer;
		}
	}

	protected override void OnButtonDown ()
	{
		base.OnButtonDown();
		ShopManager.ChangePage( pageType );
	}

	public void PageSelected( bool selected )
	{
		getSpriteRenderer.sprite = selected ? down : up ;
	}

}
