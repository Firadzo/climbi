﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpikeMoveData
{
	public	GameObject	spike;
	public 	Transform 	spikeTransf;
	public 	Vector3		from;
	public 	Vector3		to;

	public SpikeMoveData( GameObject spikeObj, Transform parent )
	{
		spike = spikeObj;
		spikeTransf = spike.transform;
		spikeTransf.parent = parent;
		spike.SetActive( false );
	}

	public void MoveSpike( float ratio, bool isEnd )
	{
		if( !spike.activeSelf )
		{
			spike.SetActive( true );
		}
		spikeTransf.localPosition = Vector3.Lerp( from, to, ratio );
		if( isEnd )
		{
			spike.SetActive( false );
		}
	}

}

public class SpikesController : MonoBehaviour {

	public	float			maxVolumeDist;
	public	float			minVolumeDist;
	public	AnimationCurve	delayCurve;
	public 	AnimationCurve	moveCurve;	
	public 	GameObject		spikePrefab;
	public	Vector2			spikeSize;
	public 	float			moveTime;
	private CTimer			moveTimer;
	private float		 	delayTime;
	private CTimer			delayTimer;
	public 	int				blinksNumber;
	private float			timeToStartBlink;
	private bool			needBlink;
	private AudioSource		audioSource;

	private Blinker		blinker;
	private Vector2		platformSize;
	private Transform 	transf;

	private PlatformBase			platformBase;
	private List<SpikeMoveData>		spikesInUse = new List<SpikeMoveData>();
	private List<SpikeMoveData> 	allSpikes = new List<SpikeMoveData>();

	void Awake()
	{
		transf = transform;
		moveTimer = new CTimer( moveTime, false );
		delayTimer = new CTimer();
		blinker = GetComponent<Blinker>();
		audioSource = GetComponent<AudioSource>();
		SoundManager.RegisterAudioSource( audioSource );
	}

	public void GetFreeSpike()
	{
		if( allSpikes.Count == 0 )
		{
			allSpikes.Add( new SpikeMoveData( Instantiate( spikePrefab ) as GameObject, transf ) );
		}
		spikesInUse.Add( allSpikes[ 0 ] );
		allSpikes.RemoveAt( 0 );
	}

	public void Init( PlatformBase platform )
	{
		delayTime = delayCurve.Evaluate( transf.position.y );
		timeToStartBlink = 1f - ( ( blinksNumber * 2f * blinker.blinkTime ) / delayTime );
		platformBase = platform;
		delayTimer.Start( delayTime );
		needBlink = true;
		if( platformBase.platformSize * 2f == platformSize ){ return; }
		allSpikes.AddRange( spikesInUse );
		spikesInUse.Clear();
		platformSize = platformBase.platformSize * 2f;
		int spikesNumberX = Mathf.RoundToInt( platformSize.x / spikeSize.x );
		int spikesNumberY = Mathf.RoundToInt( platformSize.y / spikeSize.x );
		int spikesNumber = ( spikesNumberX + spikesNumberY ) * 2;
		int i = 0;
		for( i = 0; i < spikesNumber; i++ )
		{
			GetFreeSpike();
		}
		float startX = platformSize.x / -2f;//левый верхний угол платформы
		float startY = platformSize.y / 2f;
		float halfSpikeWidth = spikeSize.x / 2f;
		int curSpikeIndex = 0;
		Vector3 temp;
		for( i = 0 ; i < spikesNumberX; i++ )//верхние шипы
		{
			temp = spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles;
			temp.z = 0f;
			spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles = temp;
			//temp = spikesMoveData[ curSpikeIndex ].spikeTransf.localEulerAngles;
			temp = new Vector3( startX + halfSpikeWidth + spikeSize.x * i, startY, 0 );
			spikesInUse[ curSpikeIndex ].to = temp;
			temp.y -= spikeSize.y;
			spikesInUse[ curSpikeIndex ].from = temp;
			curSpikeIndex++;
		}
		for( i = 0 ; i < spikesNumberX; i++ )//нижние шипы
		{
			temp = spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles;
			temp.z = 180f;
			spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles = temp;
			temp = new Vector3( startX + halfSpikeWidth + spikeSize.x * i, -startY, 0 );
			spikesInUse[ curSpikeIndex ].to = temp;
			temp.y += spikeSize.y;
			spikesInUse[ curSpikeIndex ].from = temp;
			curSpikeIndex++;
		}
		for( i = 0 ; i < spikesNumberY; i++ )//левые шипы
		{
			temp = spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles;
			temp.z = 90f;
			spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles = temp;
			temp = new Vector3( startX, startY - halfSpikeWidth - spikeSize.x * i, 0 );
			spikesInUse[ curSpikeIndex ].to = temp;
			temp.x += spikeSize.y;
			spikesInUse[ curSpikeIndex ].from = temp;
			curSpikeIndex++;
		}
		for( i = 0 ; i < spikesNumberY; i++ )//правые шипы
		{
			temp = spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles;
			temp.z = -90f;
			spikesInUse[ curSpikeIndex ].spikeTransf.localEulerAngles = temp;
			temp = new Vector3( -startX, startY - halfSpikeWidth - spikeSize.x * i, 0 );
			spikesInUse[ curSpikeIndex ].to = temp;
			temp.x -= spikeSize.y;
			spikesInUse[ curSpikeIndex ].from = temp;
			curSpikeIndex++;
		}
	}

	public void ResetSpikes()
	{
		moveTimer.Stop();
		for( int i = 0; i < spikesInUse.Count; i++ )
		{
			spikesInUse[ i ].MoveSpike( 0, !moveTimer.active );
		}
	}

	// Update is called once per frame
	void Update () {

	if( GlobalManager.isGameActive )
		{
			if( audioSource.isPlaying )
			{
				audioSource.volume = Mathf.InverseLerp(  minVolumeDist, maxVolumeDist , Mathf.Abs(GlobalManager.GetPlayerPos.y - transf.position.y) );
			}

			if( moveTimer.active )
			{
				if( moveTimer.Update( Time.deltaTime ) )
				{
					delayTimer.Start();
					needBlink = true;
				}
				for( int i = 0; i < spikesInUse.Count; i++ )
				{
					spikesInUse[ i ].MoveSpike( moveCurve.Evaluate( moveTimer.w ), !moveTimer.active );
				}
			}
			else if( delayTimer.active )
			{
				if( needBlink )
				{
					if( delayTimer.w >= timeToStartBlink )
					{
						blinker.Blink( blinksNumber );
						needBlink = false;
					}
				}

				if( delayTimer.Update( Time.deltaTime ) )
				{
					moveTimer.Start();
					PlayerOnSpringJoint.instance.LoseConnection( platformBase );
					audioSource.Play();
				}
			}
		}
	}	
}
