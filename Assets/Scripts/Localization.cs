﻿using UnityEngine;
using System.Collections;

public class Localization : MonoBehaviour {
	public TextObject		textObj;
	

	void Start()
	{
		if( textObj == null ){ textObj.textMesh = GetComponent<TextMesh>(); }
#if UNITY_EDITOR
		textObj.SetLanguage( LanguageManager.systemLanguage );
#else
		textObj.SetLanguage( Application.systemLanguage );
#endif

	}
}
