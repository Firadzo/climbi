﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class HeightParams
{
	public 	float	height;
	public	float	value;//различное значение и использование для различных обьектов
}

public class ObjBase : MonoBehaviour {

	protected GameObject	obj;
	protected Transform 	transf;
	
	virtual public bool isActive
	{
		get { 
			if( obj == null )
			{
				obj = gameObject;
			}
			return obj.activeSelf; 
		}
	}

	virtual protected void Awake()
	{
		obj 	= gameObject;
		transf 	= transform;
	}

	
	virtual public void ResetObject () {
		obj.SetActive( false );
	}

	virtual public void OnPause()
	{

	}

	virtual public void OnResume()
	{

	}
}
