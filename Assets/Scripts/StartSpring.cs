﻿using UnityEngine;
using System.Collections;

public class StartSpring : MonoBehaviour {
	public  Sprite	springUp;
	public  Sprite	springDown;
	private GameObject obj;
	private SpriteRenderer spriteRenderer;

	void Awake()
	{
		obj = gameObject;
		obj.tag = "Untagged";
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void ShowUp()
	{
		obj.tag = "ground";
		spriteRenderer.sprite = springUp;
	}

	public void Reset()
	{
		obj.tag = "Untagged";
		spriteRenderer.sprite = springDown;
	}

}
