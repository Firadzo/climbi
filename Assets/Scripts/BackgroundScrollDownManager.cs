﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BackgroundMovementData
{
	public 	float 	height;
	public	float	speed;
}

[System.Serializable]
public class BackgroundImageData
{
	public 	Sprite 	sprite;
	public	Color[]	colors;
	public 	Color	baseColor;
}

public class BackgroundScrollDownManager : MonoBehaviour {

	public	List <BackgroundScrollDown>		backgrounds;
	public	BackgroundImageData[]			backgroundSprites;
	private	Transform						transf;
	public	GameObject						prefab;
	public 	BackgroundScrollDown			defLastBackground;

	static	readonly 	Vector3				SPAWN_START_POS = new Vector3(0, -75f, 140f );
	const	float							START_SPEED = 0.01f;
	const	float							SPAWN_NEXT_HEIGHT = -122f;

	private	static 	float				curSpeed;
	private	static	Vector3				spawnNextPos;
	private static	int					curSpriteIndex = 0;
	private static BackgroundScrollDown	_lastBackground;
	public static int					currentSortOrder = -1;
	//private List<BackgroundScrollDown>	backgroundsPull = new List<BackgroundScrollDown>();

	static BackgroundScrollDownManager 	instance;
	// Use this for initialization
	void Awake () {
		transf = transform;
		instance = this;
		curSpeed = START_SPEED;
		spawnNextPos = SPAWN_START_POS;
		_lastBackground = defLastBackground;
	}

	public static void Reset()
	{
		curSpeed = START_SPEED;
		spawnNextPos = SPAWN_START_POS;
		curSpriteIndex = 0;
		_lastBackground = instance.defLastBackground;
		currentSortOrder = -1;
		for( int i = 0; i < instance.backgrounds.Count; i++ )
		{
			instance.backgrounds[ i ].Reset();
		}
	}

	public static void Pause()
	{
		for( int i = 0; i < instance.backgrounds.Count; i++ )
		{
			instance.backgrounds[ i ].Pause();
		}
	}

	public static void Resume()
	{
		for( int i = 0; i < instance.backgrounds.Count; i++ )
		{
			instance.backgrounds[ i ].Resume();
		}
	}

	private void GetFreeBackground()
	{
		for( int i = 0; i < backgrounds.Count; i++ )
		{
			if( backgrounds[ i ].notActive )
			{
				_lastBackground = backgrounds[ i ];
				return;
			}
		}
		_lastBackground = ( Instantiate( prefab ) as GameObject ).GetComponent<BackgroundScrollDown>();
		_lastBackground.getTransform.SetParent( transf );
		backgrounds.Add( _lastBackground );
	}


	void Update()
	{
		if( GlobalManager.isGameActive )
		{
			if( _lastBackground.getLocPositionY <= SPAWN_NEXT_HEIGHT )
			{
				GetFreeBackground();
				spawnNextPos.z += 1f;
				curSpeed /= 2f;
				_lastBackground.Activate( curSpeed, backgroundSprites[ curSpriteIndex ], spawnNextPos );
				//_lastBackground.GetComponent<Canvas> ().overrideSorting = true;
				//_lastBackground.GetComponent<Canvas> ().sortingOrder = BackgroundScrollDownManager.currentSortOrder;
				BackgroundScrollDownManager.currentSortOrder -= 1;
				curSpriteIndex++;
				if( curSpriteIndex == backgroundSprites.Length )
				{
					curSpriteIndex = 0;
				}
			}
		}
	}
}
