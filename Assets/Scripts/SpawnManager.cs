﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public interface ISpawnable
{
	//int spawnPriority{ get; }
	float	getObjWidth{ get; }
	bool	occupyAllLine{ get; }
	Vector2	getXOccupied{ get; }

	void Spawn( float upBorder, float botBorder );
	void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos );
}

public class SegmentData
{
	public 	bool				occupied;
	public	bool				fullyOccupied;
//	public	int 				spawnPriority;
//	public 	List<ISpawnable> 	objects;
	public 	bool[]				horizontalSegments;				
	private List<Vector2>		randomHorizontalPos = new List<Vector2>();

	/*public void AddObject( ISpawnable obj )
	{
		objects.Add( obj );
		spawnPriority = obj.spawnPriority;
	}
	
	public void AddObject( SegmentData segmentData )
	{
		objects.AddRange( segmentData.objects );
		spawnPriority = segmentData.spawnPriority;
	}*/
	
	public SegmentData()
	{
		occupied = false;
		fullyOccupied = false;
		horizontalSegments = new bool[ 10 ];
		//sobjects = new List<ISpawnable>();
	}

	public bool CheckHorizontal( float objWidth )
	{
		int freeSegmentsNumber = 0;
		for( int i = 0; i < horizontalSegments.Length; i++ )
		{
			if( !horizontalSegments[ i ] )//позиция свободна
			{
				freeSegmentsNumber++;
			}
			else
			{
				if( freeSegmentsNumber * SpawnManager.horizontalSegmentSize >= objWidth )
				{
					return true;
				}
				freeSegmentsNumber = 0;
			}
		}

		if( freeSegmentsNumber * SpawnManager.horizontalSegmentSize >= objWidth )//проверка последнего элемента
		{
			return true;
		}
		return false;
	}


	public void Spawn( ISpawnable obj, float topPos, float botPos )
	{
		int freeSegmentsNumber = 0;
		int startIndex = 0;
		float startPos;
		fullyOccupied = obj.occupyAllLine;
		occupied = true;
		randomHorizontalPos.Clear();
//		Debug.Log( fullyOccupied );
		if( !fullyOccupied )
		{
			for( int i = 0; i < horizontalSegments.Length; i++ )
			{
				if( !horizontalSegments[ i ] )//позиция свободна
				{
					freeSegmentsNumber++;
				}
				else
				{
					if( freeSegmentsNumber * SpawnManager.horizontalSegmentSize >= obj.getObjWidth )
					{
						startPos = -GlobalManager.DEF_HALF_SCREEN_WIDTH + startIndex * SpawnManager.horizontalSegmentSize;
						randomHorizontalPos.Add( new Vector2( startPos, startPos + freeSegmentsNumber * SpawnManager.horizontalSegmentSize ) );
					}
					freeSegmentsNumber = 0;
					startIndex = i + 1;
				}
			}

			if( freeSegmentsNumber > 0 )//если по окончанию цикла не было найдено занятых  сегментов, то проводится проверка после выхода из цикла
			{
				if( freeSegmentsNumber * SpawnManager.horizontalSegmentSize >= obj.getObjWidth )
				{
					startPos = -GlobalManager.DEF_HALF_SCREEN_WIDTH + startIndex * SpawnManager.horizontalSegmentSize;
					randomHorizontalPos.Add( new Vector2( startPos, startPos + freeSegmentsNumber * SpawnManager.horizontalSegmentSize ) );
				}
			}

			Vector2 xPosRange = randomHorizontalPos[ Random.Range( 0, randomHorizontalPos.Count) ];
			obj.Spawn( topPos, botPos, xPosRange.x, xPosRange.y );
			xPosRange = obj.getXOccupied;
			for( int i = 0; i < horizontalSegments.Length; i++ )
			{
				startPos = -GlobalManager.DEF_HALF_SCREEN_WIDTH + ( i + 1f ) * SpawnManager.horizontalSegmentSize;

				if( startPos >= xPosRange.x  )
				{
					horizontalSegments[ i ] = true;
				}

				if( startPos >= xPosRange.y )
				{
					break;
				}
			}
			return;
		}
		obj.Spawn( topPos, botPos );
	}

}

public class SpawnManager : MonoBehaviour {

	const	float 			segmentHeight = 150f;
	const	float			startHeight = 250f;
	const 	float			horizontalSegmentsNumber = 10;				

	public 	static	float	horizontalSegmentSize;
	private  static	float 	halfSegmentHeight;
	private	 static float	lastHeight;
	private  static Dictionary< float, SegmentData > segments = new Dictionary< float, SegmentData >();
	

	void Awake()
	{
		horizontalSegmentSize = ( GlobalManager.DEF_HALF_SCREEN_WIDTH * 2f ) / horizontalSegmentsNumber;
		halfSegmentHeight = segmentHeight / 2f;
	}

	public static void Reset()
	{
		segments.Clear();
		lastHeight = startHeight;
		segments.Add( lastHeight, new SegmentData() );
	}
	
	private static float GetNearestHeightForSpawn( float height )
	{
		foreach( float heightKey in segments.Keys )
		{
			if( heightKey == height )
			{
				return heightKey;
			}
			else if( heightKey > height )
			{
				//находим ближайший по высоте сегмент, сравнивая дистанцию  с следующим и предидушим
				if( heightKey - height  <= halfSegmentHeight )
				{
					 return heightKey;
				}
				else//( height - ( heightKey - segmentHeight ) < dif )
				{
					return heightKey - segmentHeight;
				}
			}
		}
		Debug.LogError( height + "  "+lastHeight);
		return height;
	}
	
/*	private void MoveSegmentsUpward( SegmentData segmentData, float height )
	{
		RegisterForSpawn( segmentData, height );
		segmentData.objects.Clear();//проверить корректно ли очищается список
	}
	
	public static void RegisterForSpawn( SegmentData segmentData, float height )
	{
		if( lastHeight < height )
		{
			lastHeight += segmentHeight;
			segments.Add( lastHeight, new SegmentData() );
			if( lastHeight >= height )
			{
				segments[ lastHeight ].AddObject( segmentData );
				return;
			}
		}
		height = GetNearestHeightForSpawn( height );
		if( segments[ height ].spawnPriority == segmentData.spawnPriority )
		{
			segments[ height ].AddObject( segmentData );
			return;
		}
		else if( segments[ height ].spawnPriority < segmentData.spawnPriority )
		{
			MoveSegmentsUpward( segmentData, height + segmentHeight );
			segments[ height ].AddObject( segmentData );
			return;
		}
		else if( segments[ height ].spawnPriority > segmentData.spawnPriority )
		{
			height += segmentHeight;
			RegisterForSpawn( segmentData, height );
		}
	}*/
	
	// Регистрируем элемент для спавна 
	public static void RegisterForSpawn( ISpawnable obj, float height )
	{
		//если высота последнего спавна  ниже заданной, то добавляем в словарь элементы, пока не достигнем нужной высоты, куда и региструем обьект для спавна
		while( lastHeight < height )
		{
			lastHeight += segmentHeight;
			segments.Add( lastHeight, new SegmentData() );
			if( lastHeight >= height )
			{
				//segments[ lastHeight ].AddObject( obj );
				segments[ lastHeight ].Spawn( obj, lastHeight, lastHeight - segmentHeight );
				//obj.Spawn( lastHeight, lastHeight - segmentHeight );// lastHeight + halfSegmentHeight, lastHeight - halfSegmentHeight );
				return;
			}
		}

		height = GetNearestHeightForSpawn( height );//если высота не выше, находим высоту ближайщего сегмента для спавна

		if( segments[ height ].fullyOccupied || ( segments[ height ].occupied && obj.occupyAllLine ) )
		{
			height += segmentHeight;
			RegisterForSpawn( obj, height );
		}
		else if( !segments[ height ].occupied )
		{
			segments[ height ].Spawn( obj, height, height - segmentHeight );
		}
		else if( !obj.occupyAllLine && segments[ height ].CheckHorizontal( obj.getObjWidth ) )
		{
			segments[ height ].Spawn( obj, height, height - segmentHeight );
		}
		else
		{
			height += segmentHeight;
			RegisterForSpawn( obj, height );

		}
		/*if( segments[ height ].spawnPriority == obj.spawnPriority )//если приорит спавна заданного сегмента и переданного элемента совпадают, то добавляем элемент сюда
		{
			segments[ height ].AddObject( obj );
			return;
		}
		else if( segments[ height ].spawnPriority < obj.spawnPriority )// если приоритет спавна элементов в списке сегмента ниже текущего, то сдвигаем элементы дальше по тому же принципу, 
			//а текущий регистрируем в данном сегменте
		{
			MoveSegmentsUpward( segments[ height ], height + segmentHeight );
			segments[ height ].AddObject( obj );
			return;
		}
		else if( segments[ height ].spawnPriority > obj.spawnPriority )//если приоритет спавна элементов в сегменте выше приоритета текущего элемента, то сдвигаем элемент выше
		{
			height += segmentHeight;
			RegisterForSpawn( obj, height );
		}*/
	}

	Dictionary <float, SegmentData > temp = new Dictionary<float, SegmentData>();
	void Update()
	{
		if( GlobalManager.isGameActive )
		{
			temp.Clear();
			foreach( float key in segments.Keys )
			{
				if( key > GlobalManager.GetPlayerPos.y )
				{
					temp.Add(  key, segments[ key ] );
				}
			}
			segments.Clear();
			foreach( float key in temp.Keys )
			{
				segments.Add( key, temp[ key ] );
			}
		}
	}

# if UNITY_EDITOR
	public bool drawHorizontalSegments;
	void OnDrawGizmosSelected()
	{

		if( segments != null )
		{
			foreach( float key in segments.Keys )
			{
				Gizmos.color = Color.blue;
				Gizmos.DrawLine( new Vector3( -512f, key, 10f ),new Vector3( 512f, key, 10f ) );
				if( drawHorizontalSegments )
				{
					for( int i = 0; i < segments[ key ].horizontalSegments.Length; i++ )
					{
						if( segments[ key ].horizontalSegments[ i ] )
						{
							Gizmos.color = Color.red;
						}
						else
						{
							Gizmos.color = Color.green;
						}
						Gizmos.DrawLine( new Vector3( -GlobalManager.DEF_HALF_SCREEN_WIDTH + i * horizontalSegmentSize, key, 10f ), 
						                new Vector3( -GlobalManager.DEF_HALF_SCREEN_WIDTH + ( i + 1 ) * horizontalSegmentSize, key - segmentHeight, 10f ) );
					}
				}
			}
		}
	}

# endif
}
