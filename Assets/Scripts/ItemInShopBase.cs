﻿using UnityEngine;
using System.Collections;

public class ItemInShopBase : MonoBehaviour {
	public 		int 						price;
	public 		GameObject					boughtObj;
	public		GameObject					priceObj;
	protected 	TextMesh					priceText;

	protected	Animator				_animator;
	protected 	bool					bought;
	const 		string					ANIM_NAME = "scaleAnim";

	/*protected TextMesh getPriceText
	{
		get{
			if( priceText == null )
			{
				priceText = GetComponentInChildren<TextMesh>();
			}
			return priceText;

		}
	}*/


	virtual protected void Awake()
	{
		_animator = GetComponentInChildren<Animator>();
		priceText = priceObj.GetComponent<TextMesh>();
		priceText.text = price.ToString();
	}

	virtual public void GetItemState()
	{

	}

	virtual protected void OnButtonDown()
	{
		if( bought )
		{
			SoundManager.PlayFailedBuy();
			return;
		}

		if( CoinsManager.getCoinsNumber >= price )
		{
			Buying();
			_animator.Play( ANIM_NAME );
			SoundManager.PlaySucessBuy();
		}
		else
		{
			ShopManager.PlayNotEnoughCoinsEffect();
			SoundManager.PlayFailedBuy();
		}
	}

	virtual protected void Buying()
	{
		bought = true;
		priceObj.SetActive( false );
		boughtObj.SetActive( true );
		CoinsManager.SpendCoins( price );
#if UNITY_ANDROID
		GlobalManager.FirstBuyAchievement();
#endif
	}

	virtual protected void Buy()
	{
		if( CoinsManager.getCoinsNumber >= price && !bought )
		{
			Buying();
		}
	}
}
