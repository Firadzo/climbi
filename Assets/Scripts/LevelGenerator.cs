using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PlatformSpawnInfo
{
#if UNITY_EDITOR
	[HideInInspector]
	public string				name;
#endif
	public 	GameObject 			platformPrefab;
	public 	int 				precreatedCount;
	public 	int 				spawnPriority;
//	public 	float				startSpawnHeight;
	//public 	float				heightBeetweenSpawn;
	public	AnimationCurve		spawnHeightCurve;//key[0].time == высота старта спавна, значение кривой по высоте это время между спавном
	public 	bool				defaultPlatf;

	private float 				lastSpawnHeight;
	private List<PlatformBase> 	platforms;
	private PlatformBase		nextForSpawn;

	public PlatformBase getNextForSpawn
	{
		get {
			//if( nextForSpawn == null )
			{
				SetPlatformForSpawn();
			}
			return nextForSpawn;
		}
	}

	public bool canSpawn
	{
		get { 
				if( defaultPlatf )
				{
					return true;
				}
				else
				{
					return LevelGenerator.getSpawnHeight >= lastSpawnHeight;
				} 
		}
	}

	public void Init()
	{
		platforms = new List<PlatformBase>();
		lastSpawnHeight = spawnHeightCurve.keys[ 0 ].time;
		for( int i = 0; i < precreatedCount; i++ )
		{
			InstantiatePlatform();
		}
	}

	public void Reset()
	{
		lastSpawnHeight = spawnHeightCurve.keys[ 0 ].time;
		for( int i = 0; i < platforms.Count; i++ )
		{
			if( platforms[ i ].isActive )
			{
				platforms[ i ].ResetObject();
			}
		}
	}

	private void InstantiatePlatform()
	{
		platforms.Add( ( GameObject.Instantiate( platformPrefab ) as GameObject ).GetComponent<PlatformBase>() );
		platforms[ platforms.Count - 1 ].getTransform.parent = LevelGenerator.transf;
	}

	public void SetNewSpawnHeight()
	{
		lastSpawnHeight = nextForSpawn.getTransform.position.y + spawnHeightCurve.Evaluate( GlobalManager.CameraHeight );
	}

	/*public void SpawnPlatform( Vector3 pos )
	{
		lastSpawnHeight += pos.y + heightBeetweenSpawn;
		nextForSpawn.Activate( pos );
		nextForSpawn = null;
	}*/

	public void SetPlatformForSpawn()
	{
		for( int i = 0; i < platforms.Count; i++ )
		{
			if( !platforms[ i ].isActive )
			{
				nextForSpawn = platforms[ i ];
				return;
			}
		}
		InstantiatePlatform();
		nextForSpawn = platforms[ platforms.Count - 1 ];
	}

}

public class LevelGenerator : MonoBehaviour {
	public 	PlatformSpawnInfo[] 	spawnInfo;
	public 	float 					distanceBetweenElements;
	public	float					firstElementHeight;
	public 	float					heightForGeneration = 1500f;

	private static float 			spawnHeight;
	private float 					curElementWidth;
	private float					curHeightForGeneration;


	public Transform				playerTransform;
//	private float 					platformsZ;
	private List<PlatformSpawnInfo>	platformsForSpawn;

	private	static List<PlatformBase>	activedPlatforms;
	public 	static LevelGenerator 		instance;
	public	static Transform			transf;

	public static float getSpawnHeight
	{
		get{  return spawnHeight; }
	}


	void Awake()
	{
		instance = this;
		activedPlatforms = new List<PlatformBase>();
		platformsForSpawn = new List<PlatformSpawnInfo>();
		spawnHeight = firstElementHeight;
		transf = transform;

	}

	public static void RemovePlatformFromActived( PlatformBase platform )
	{
		activedPlatforms.Remove( platform );
	}

	void Start()
	{
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].Init();
		}
	}

	public void Reset()
	{
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			spawnInfo[ i ].Reset();
		}
		spawnHeight = firstElementHeight;
	}

	public void Restart()
	{
		GenerateLevel();
	}

	public float GetYPosForEnemy( float yPos )
	{
		for( int i = 0; i < activedPlatforms.Count; i++ )
		{
			if( activedPlatforms[ i ].IsYOccupied( yPos ) )
			{
				return activedPlatforms[ i ].GetMaxYBorder + 100f;
			}
		}
		return yPos;
	}

	private float curMaxSpawnPriority;
	private PlatformSpawnInfo GetNextPlatform() 
	{
		platformsForSpawn.Clear();
		curMaxSpawnPriority = 0;
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			if( spawnInfo[ i ].canSpawn )
			{
				if( curMaxSpawnPriority == spawnInfo[ i ].spawnPriority )
				{
					platformsForSpawn.Add( spawnInfo[ i ] );
				}
				else if( curMaxSpawnPriority < spawnInfo[ i ].spawnPriority )
				{
					curMaxSpawnPriority = spawnInfo[ i ].spawnPriority;
					platformsForSpawn.Clear();
					platformsForSpawn.Add( spawnInfo[ i ] );
				}
			}
		}
		return platformsForSpawn[ Random.Range( 0, platformsForSpawn.Count ) ];
	}

	PlatformSpawnInfo curSpawnInfo;
	PlatformBase nextElement;
	private void GenerateLevel()
	{
		curHeightForGeneration = playerTransform.position.y + heightForGeneration;
		while ( curHeightForGeneration > spawnHeight )
		{
			curSpawnInfo = GetNextPlatform();
			nextElement = curSpawnInfo.getNextForSpawn;
			//curElementWidth = nextElement.platformSize.x;
			//curSpawnInfo.SpawnPlatform( new Vector3( Random.Range( -GlobalManager.HALF_SCREEN_WIDTH + curElementWidth, GlobalManager.HALF_SCREEN_WIDTH - curElementWidth  ), spawnHeight, platformsZ ) );
			nextElement.PrepareForSpawn();
			SpawnManager.RegisterForSpawn( nextElement, spawnHeight );
			spawnHeight = distanceBetweenElements + nextElement.GetYOccupiedSpace + nextElement.getTransform.position.y;
			//Debug.Log( spawnHeight );
			curSpawnInfo.SetNewSpawnHeight(); 
			activedPlatforms.Add( nextElement );
		}
	}
	
	// Update is called once per frame
	void Update () {
		if( GlobalManager.isGameActive )
		{
			GenerateLevel();
		}
	}

#if UNITY_EDITOR
	Vector3 mainCamPos;
	void OnDrawGizmosSelected()
	{
		mainCamPos = Camera.main.transform.position;
		Gizmos.color = Color.red;
		Gizmos.DrawLine( new Vector3( -500f, firstElementHeight, mainCamPos.z + 10f ),
		                new Vector3( 500f, firstElementHeight, mainCamPos.z + 10f ) );
		Gizmos.color = Color.green;
		Gizmos.DrawLine( new Vector3( -500f, firstElementHeight + distanceBetweenElements, mainCamPos.z + 10f ),
		                new Vector3( 500f, firstElementHeight + distanceBetweenElements, mainCamPos.z + 10f ) );
		for( int i = 0; i < spawnInfo.Length; i++ )
		{
			if( spawnInfo[ i ].platformPrefab != null )
			{
				spawnInfo[ i ].name = spawnInfo[ i ].platformPrefab.name;
			}
		}

	}
#endif
}
