using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*[System.Serializable]
public class CoinsShapeInfo
{
#if UNITY_EDITOR
	[HideInInspector]
	public string	 name;
	public bool		drawGizmos;
#endif
	public int coinsHorizontal;
	public int coinsVertical;
	public int coinsNeeded;
}*/

public class CoinsBonus : BonusBase {
	public HeightParams[]		coinsHeightParams;
	const int MAX_COINS_X = 14;
	const int MAX_COINS_Y = 3;
	//public float 				spawnYOffset;
	//public CoinsShapeInfo[]		coinsShapeInfo;
	private List<Coin>			coinsInUse = new List<Coin>();
//	private CoinsShapeInfo		curShape;
	private float				curObjWidth;


	const float SPAWN_OFFSET = 45f;

	override public float getObjWidth
	{
		get { return curObjWidth; }
	}

	override public Vector2 getXOccupied
	{
		get { return new Vector2( transf.position.x, transf.position.x + curObjWidth ); }
	}

	override public void ResetObject()
	{
		for( int i = 0; i < coinsInUse.Count; i++ )
		{
			coinsInUse[ i ].ResetObject();
		}
		base.ResetObject();
	}

	override public void OnPause()
	{
		for (int i = 0; i < coinsInUse.Count; i++) 
		{
			coinsInUse[ i ].OnPause();
		}
	}
	
	override public void OnResume()
	{
		for (int i = 0; i < coinsInUse.Count; i++) 
		{
			coinsInUse[ i ].OnResume();
		}
	}

	public void RemoveCoin( Coin coin )
	{
		coinsInUse.Remove( coin );
		if( coinsInUse.Count == 0 )
		{
			ResetObject();
		}
	}

	public void GetCurMaxCoins()
	{
		float height = GlobalManager.CameraHeight + GlobalManager.screen_height;
		for( int i = coinsHeightParams.Length - 1; i >=0 ; i-- )
		{
			if( height >= coinsHeightParams[ i ].height )
			{
				curMaxCoins = (int)coinsHeightParams[ i ].value;
				return; 
			}

		}
	}

	int curMaxCoins;
	int coinsHor;
	int coinsVert;
	override public void PrepareForSpawn()
	{
		GetCurMaxCoins();
		int xMin = curMaxCoins / MAX_COINS_Y;
		if( xMin < MAX_COINS_X )
		{
			int xMax = curMaxCoins;
			if( curMaxCoins > MAX_COINS_X )
			{
				xMax = MAX_COINS_X;
			}
			coinsHor = Random.Range( xMin, xMax + 1);
		}
		else
		{
			coinsHor = MAX_COINS_X;
		}
		coinsVert = Mathf.Clamp( Mathf.RoundToInt( curMaxCoins / (float)coinsHor ), 1, 3 );
		//curShape = coinsShapeInfo[ Random.Range( 0, coinsShapeInfo.Length ) ];
		curObjWidth = coinsHor * SPAWN_OFFSET;
	}

	/*override public void Spawn()
	{
		float spawnHeight = LevelGenerator.instance.GetYPosForEnemy( GlobalManager.CameraHeight + spawnYOffset );
		Spawn( spawnHeight, spawnHeight );
	}*/

	override public void Spawn( float upBorder, float botBorder )
	{
		Spawn( upBorder, botBorder, -GlobalManager.DEF_HALF_SCREEN_WIDTH, GlobalManager.DEF_HALF_SCREEN_WIDTH );
	}

	override public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		coinsInUse.Clear();
		coinsInUse.AddRange( BonusManager.GetCoins( coinsHor * coinsVert ) );
		
		int k = 0;
		Vector3 spawnPos = new Vector3();
		spawnPos.x = Random.Range( minXPos + SPAWN_OFFSET,  maxXPos - SPAWN_OFFSET - curObjWidth );
		spawnPos.y = Random.Range( botBorder, upBorder - coinsVert * SPAWN_OFFSET );
		spawnPos.z = zPos;
		transf.position = spawnPos;
		Vector3 spawnOffet = new Vector3( SPAWN_OFFSET/2f, SPAWN_OFFSET/2f, 0 );
		for( int i = 0; i < coinsHor; i++ )
		{
			for( int j = 0; j < coinsVert; j++ )
			{
				spawnPos = transf.position + spawnOffet;
				spawnPos.x += SPAWN_OFFSET * i;
				spawnPos.y += SPAWN_OFFSET * j;
				spawnPos.z = zPos;
				coinsInUse[ k ].SpawnAtPos( spawnPos, this );
				k++;
			}
		}
		obj.SetActive( true );
	}


/*
#if UNITY_EDITOR
	private Vector3 tmp;
	void OnDrawGizmosSelected()
	{
		if( coinsShapeInfo != null )
		{
			Gizmos.color = Color.yellow;
			for( int i = 0; i < coinsShapeInfo.Length; i++ )
			{
				coinsShapeInfo[ i ].name = coinsShapeInfo[ i ].coinsHorizontal+" x "+coinsShapeInfo[ i ].coinsVertical;
				coinsShapeInfo[ i ].coinsNeeded =  coinsShapeInfo[ i ].coinsHorizontal * coinsShapeInfo[ i ].coinsVertical;
				if( coinsShapeInfo[ i ].drawGizmos )
				{
					for( int j = 0; j < coinsShapeInfo[ i ].coinsVertical; j++ )
					{
						for( int k = 0; k < coinsShapeInfo[ i ].coinsHorizontal; k++ )
						{
							tmp = transform.position;
							tmp.x += SPAWN_OFFSET * k;
							tmp.y += SPAWN_OFFSET * j;
							Gizmos.DrawWireSphere( tmp, 14f );
						}
					}
				}
			}
		}
	}
#endif*/
}
