﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ShopPageController
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public	EShopPage		pageType;
	public 	ShopPageButton	pageButton;
	public	GameObject 		pageParent;
	public 	GameObject[]	elements;
	public 	Vector3 		startSpawnPos;
	public	float			xOffset;
	public	float			yOffset;
	public	int 			maxNumberInLine;
	
	private ItemInShopBase[]	itemsInShop;

	public void Init()
	{
		Transform 	parent = pageParent.transform;
		Transform 	tempTransf;
		Vector3 	tempV3;
		float 		xIndex = 0;
		float 		yIndex = 0;
		int 		curNumberInLine = 0;
		itemsInShop = new ItemInShopBase[ elements.Length ];
		for( int i = 0; i < elements.Length; i++ )
		{
			tempTransf = ( GameObject.Instantiate( elements[ i ] ) as GameObject ).transform;
			itemsInShop[ i ] = tempTransf.GetComponent< ItemInShopBase >(); 
			tempTransf.parent = parent;
			tempV3 = startSpawnPos;
			tempV3.x += xOffset * xIndex;
			tempV3.y += yOffset * yIndex;
			tempTransf.localPosition = tempV3;
			xIndex ++;
			curNumberInLine++;
			if( curNumberInLine == maxNumberInLine )
			{
				curNumberInLine = 0;
				yIndex++;
				xIndex = 0;
			}
		}
		pageParent.SetActive( false );
	}

	public void GetItemsState()
	{
		for( int i = 0; i < itemsInShop.Length; i++ )
		{
			itemsInShop[ i ].GetItemState();
		}
	}

	public void SelectPage( bool select )
	{
		pageParent.SetActive( select );
		pageButton.PageSelected( select );
	}

}

public enum EShopPage { boosts, special, upgrades }
public class ShopManager : MonoBehaviour {

	public	ShopPageController[]	shopPages;
	public 	GameObject				shopMenu;
	public	Animator				notEnoughCoins;
	const 	string					STATE_NAME = "NotEnoughCoins";

	private const 	EShopPage		DEFAULT_PAGE = EShopPage.boosts;
	private static 	EShopPage		curShopPage;
	private static 	ShopManager		instance;
	
	public static EShopPage getCurPageType
	{
		get { return curShopPage; }
	}

	void Awake () {
		//PlayerPrefs.DeleteAll();
		instance = this;
		curShopPage = DEFAULT_PAGE;
		for( int i = 0; i < shopPages.Length; i++  )	
		{
			shopPages[ i ].Init();
		}
		GetShopPageOfType( curShopPage ).SelectPage( true );
	}


	public static void PlayNotEnoughCoinsEffect()
	{
		 instance.notEnoughCoins.Play( STATE_NAME, -1, 0 );
	}

	private ShopPageController GetShopPageOfType( EShopPage pageType )
	{
		for( int i = 0; i < shopPages.Length; i++  )	
		{
			if( shopPages[ i ].pageType == pageType )
			{
				return shopPages[ i ];
			}
		}
		Debug.LogWarning(" There is no shop page of type "+ pageType );
		return null;
	}

	public static void ChangePage( EShopPage pageType )
	{
		if( curShopPage != pageType )
		{
			instance.GetShopPageOfType( curShopPage ).SelectPage( false );
			curShopPage = pageType;
			instance.GetShopPageOfType( curShopPage ).SelectPage( true );
		}
	}

	public static void OpenShop()
	{
		instance.shopMenu.SetActive( true );
		for( int i = 0; i < instance.shopPages.Length; i++ )
		{
			instance.shopPages[ i ].GetItemsState();
		}
		ChangePage( EShopPage.boosts );
	}

	public static void CloseShop()
	{
		instance.shopMenu.SetActive( false );
	}


	#if UNITY_EDITOR
	public bool sortBoosts;
	void OnDrawGizmosSelected()
	{
		if( shopPages != null )
		{
			if( sortBoosts )
			{
				sortBoosts = false;
				for( int i = 0; i < shopPages.Length; i++ )
				{
					if( shopPages[ i ].pageType == EShopPage.boosts )
					{
						QuickSortPrices( shopPages[ i ].elements, 0, shopPages[ i ].elements.Length - 1 );
						break;
					}
				}
			}

			for( int i = 0; i < shopPages.Length; i++  )	
			{
				shopPages[ i ].name = shopPages[ i ].pageType.ToString();
			}
		}
	}


	int Partition ( GameObject[] m, int a, int b)// where T : System.IComparable<T>
	{
		int i = a;
		for (int j = a; j <= b; j++)         // просматриваем с a по b
		{
			if ( m[j].GetComponent<BoostInShop>().price <=  m[b].GetComponent<BoostInShop>().price )  // если элемент m[j] не превосходит m[b],
			{
				GameObject t = m[i];                  // меняем местами m[j] и m[a], m[a+1], m[a+2] и так далее...
				m[i] = m[j];                 // то есть переносим элементы меньшие m[b] в начало,
				m[j] = t;                    // а затем и сам m[b] «сверху»
				i++;                         // таким образом последний обмен: m[b] и m[i], после чего i++
			}
		}
		return i - 1;                        // в индексе i хранится <новая позиция элемента m[b]> + 1
	}
	
	void QuickSortPrices ( GameObject[] m, int a, int b) //where T : System.IComparable<T>// a - начало подмножества, b - конец
	{                                        // для первого вызова: a = 0, b = <элементов в массиве> - 1
		if (a >= b) return;
		int c = Partition( m, a, b);
		QuickSortPrices( m, a, c - 1);
		QuickSortPrices( m, c + 1, b);
	}

	#endif

}
