using UnityEngine;
using System.Collections;

public class PlatformBase : ObjBase, ISpawnable {

	public 		float			disableHeight;
	public 		Vector2			platformSize;
	protected 	float 			minYBorder;
	protected 	float 			maxYBorder;
	protected 	float 			occupiedSpaceY;
	const		float 			ADD_TO_BORDER = 25f;
	static 		float 			zPos;
	protected 	SpriteRenderer	spriteRenderer;
	protected 	BoxCollider2D	boxCollider2D;
	
	virtual public float getObjWidth
	{
		get { return platformSize.x * 2f; }
	}

	virtual public Vector2 getXOccupied
	{
		get {
				return new Vector2( transf.position.x - platformSize.x, transf.position.x + platformSize.x ); 
			}
	}

	virtual public bool occupyAllLine
	{
		get { return false; }
	}

	virtual public bool IsDestroyed
	{
		get { return false; }
	}

	public float GetMaxYBorder
	{
		get { return maxYBorder; }
	}

	public Transform getTransform
	{
		get {
			if( transf == null )
			{
				transf = transform;
			}
			return transf;
		}
	}

	virtual public void PlayerConnected()
	{
	}

	virtual public void PlayerDisconnected()
	{
	}

	virtual public float GetYOccupiedSpace
	{
		get { return occupiedSpaceY; }
	}

	override protected void Awake () {
		base.Awake();
		spriteRenderer = GetComponent<SpriteRenderer>();
		boxCollider2D = GetComponent<BoxCollider2D>();
		occupiedSpaceY = platformSize.y;
		if( zPos == 0 )
		{
			zPos = GlobalManager.GetZParams( ObjectType.platform );
		}
		obj.SetActive( false );
	}
	

	virtual public void Activate( Vector3 pos )
	{
		transf.position = pos;
		ReInit();
		obj.SetActive( true );
	}

	virtual public void PrepareForSpawn()
	{

	}

	virtual public void Spawn( float upBorder, float botBorder )
	{
		Spawn( upBorder, botBorder, -GlobalManager.DEF_HALF_SCREEN_WIDTH, GlobalManager.DEF_HALF_SCREEN_WIDTH );
	}

	virtual public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		ReInit();
		Vector3 pos = new Vector3();
		pos.x = Random.Range( minXPos + platformSize.x, maxXPos - platformSize.x ) ;
		pos.y = Random.Range( botBorder + platformSize.y, upBorder - platformSize.y );
		pos.z = zPos;
		transf.position = pos; 
		maxYBorder = transf.position.y + occupiedSpaceY + ADD_TO_BORDER;
		minYBorder = transf.position.y - occupiedSpaceY - ADD_TO_BORDER;
		obj.SetActive( true );
	}

	virtual public void ReInit()
	{
	}

	virtual public bool IsYOccupied( float y )
	{
		return ( y <= maxYBorder && y >= minYBorder );
	}

	virtual protected void OnUpdate()
	{
		if( transf.position.y + disableHeight <= GlobalManager.CameraHeight )
		{
			LevelGenerator.RemovePlatformFromActived( this ); 
			ResetObject();
		}
	}

	virtual protected void Update()
	{
		if( !GlobalManager.isGameActive )
		{  
			return;
		}
		OnUpdate();
	}

	virtual public float GetRopeConnectionAngle( Vector3 connectionPos )
	{
		if( connectionPos.x >= transf.position.x + platformSize.x )
		{
			if( connectionPos.y > transf.position.y - platformSize.y &&  connectionPos.y < transf.position.y + platformSize.y )
			{
				return 90f;
			}
		}
		if( connectionPos.x <= transf.position.x - platformSize.x )
		{
			if( connectionPos.y > transf.position.y - platformSize.y &&  connectionPos.y < transf.position.y + platformSize.y )
			{
				return -90f;
			}
		}
		
		if( connectionPos.x < transf.position.x + platformSize.x && connectionPos.x > transf.position.x - platformSize.x )
		{
			if( connectionPos.y > transf.position.y )
			{
				return 180f;
			}
		}
		return 0;
	}

	#if UNITY_EDITOR
	
	virtual protected void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.green;
		Gizmos.DrawLine( new Vector3( transform.position.x - platformSize.x, transform.position.y + platformSize.y, transform.position.z - 1f ), 
		                new Vector3( transform.position.x + platformSize.x, transform.position.y + platformSize.y, transform.position.z - 1f ) );
		Gizmos.DrawLine( new Vector3( transform.position.x - platformSize.x, transform.position.y - platformSize.y, transform.position.z - 1f ), 
		                new Vector3( transform.position.x + platformSize.x, transform.position.y - platformSize.y, transform.position.z - 1f ) );
		Gizmos.DrawLine( new Vector3( transform.position.x + platformSize.x, transform.position.y + platformSize.y, transform.position.z - 1f ), 
		                new Vector3( transform.position.x + platformSize.x, transform.position.y - platformSize.y, transform.position.z - 1f ) );
		Gizmos.DrawLine( new Vector3( transform.position.x - platformSize.x, transform.position.y + platformSize.y, transform.position.z - 1f ), 
		                new Vector3( transform.position.x - platformSize.x, transform.position.y - platformSize.y, transform.position.z - 1f ) );
	}
	#endif
}
