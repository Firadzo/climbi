﻿using UnityEngine;
using System.Collections;

public class WarningBase : ObjBase {
	
	protected AfteWarningCB	afterWarningCB;

	public delegate void AfteWarningCB(); 

	virtual public void Show()
	{

	}

	virtual public void Show( float warnTime )
	{
		
	}

	virtual public void Show( float warnTime, AfteWarningCB callBack )
	{
		afterWarningCB = callBack;
	}

	virtual public void DisableWarning()
	{

	}
}
