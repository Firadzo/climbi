﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BackgroundScrollDown : MonoBehaviour {

	public 	float 			speedScale;
	public 	float 			activationHeight;
	private float			opacityHeight;
	public 	bool 			useAlphaFade;
	public 	ParticleSystem	clouds;
	private Material		cloudsMat;
	public	SpriteRenderer	solidColor;
	public	bool			oneOfMain;
	
	private SpriteRenderer	spriteRend;
	private Image			image;
	private Color			tmpCol;
	private bool			isActive;
	private Transform 		transf;
	private	GameObject		obj;
	private Vector3			tempVec3;
	private Vector3			startPos;

	const float				OPACITY_OFFSET = 5000f;
	
	public Transform getTransform
	{
		get {
			if( transf == null )
			{
				transf = transform;
			}
			return transf;
		}
	}

	public float getLocPositionY
	{
		get {
			return getTransform.localPosition.y;
		}
	}

	public bool notActive
	{
		get { return !obj.activeSelf; }
	}


	void Start()
	{
		cloudsMat = clouds.GetComponent<Renderer>().material;
		image = GetComponent < Image> ();
		transf 	= transform;
		obj		= gameObject;
		spriteRend = GetComponent<SpriteRenderer>();
		if( oneOfMain )
		{
			startPos = transf.localPosition;
			startPos.y -= GlobalManager.get_height_difference;
			transf.localPosition = startPos;
			activationHeight += GlobalManager.get_height_difference;
		}
	}

	public void Reset()
	{
		if( oneOfMain )
		{
			isActive = false;
			transf.localPosition = startPos;
			if( useAlphaFade )
			{
				tmpCol = spriteRend.color;
				tmpCol.a = 0;
				spriteRend.color = tmpCol;
			}
		}
		else
		{
			obj.SetActive( false );
		}
	}

	public void Activate( float speed, BackgroundImageData imageData, Vector3 startPos )
	{
		Start();
		transf.localPosition = startPos;
		spriteRend.sprite = imageData.sprite;
		spriteRend.color = imageData.colors[ Random.Range( 0, imageData.colors.Length ) ];
		tmpCol = cloudsMat.GetColor("_TintColor");
		tmpCol.a = 0;
		cloudsMat.SetColor( "_TintColor", tmpCol );
		activationHeight = GlobalManager.CameraHeight;
		speedScale = speed;
		opacityHeight = activationHeight + OPACITY_OFFSET;
		solidColor.color = spriteRend.color * imageData.baseColor;
		obj.SetActive( true );
	}


	public void Pause()
	{
		if( clouds != null )
		{
			clouds.Pause();
		}
	}

	public void Resume()
	{
		if( clouds != null )
		{
			clouds.Play();
		}
	}

	// Update is called once per frame
	void LateUpdate () {
		if( GlobalManager.isGameActive  )
		{
			if( isActive )
			{
				if( useAlphaFade )
				{
					float alpha = Mathf.InverseLerp( activationHeight, opacityHeight, GlobalManager.CameraHeight );
					tmpCol = spriteRend.color;
					tmpCol.a = alpha;
					spriteRend.color = tmpCol;

					tmpCol = solidColor.color;
					tmpCol.a = spriteRend.color.a;
					solidColor.color = tmpCol;

					tmpCol = cloudsMat.GetColor("_TintColor");
					tmpCol.a = alpha * 0.5f;
					cloudsMat.SetColor( "_TintColor", tmpCol );
				}
				if( PlayerOnSpringJoint.GetVelocity.y != 0 )
				{
					tempVec3 = transf.localPosition;
					tempVec3.y += PlayerOnSpringJoint.GetVelocity.y * -1f * speedScale * Time.deltaTime;
					transf.localPosition = tempVec3;
				}
			}
			else
			{
				isActive = GlobalManager.CameraHeight >= activationHeight;
			}
		}

	}
}
