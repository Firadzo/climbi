﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpecialBonusParams
{
	public	GameObject			obj;
	public 	ESpecialBonusType	bonusType;
}

public enum ESpecialBonusType { none, lowGravity, coinMagnet, shield, flyUp }
public class SpecialBonus : BonusBase {

	public	SpecialBonusParams[]	bonuses;
	//public 	ESpecialBonusType		bonusType;
	public 	int 					scoreForBonus;
	const	float 					SPAWN_OFFSET = 45f;
	private int 					curBonusIndex = 0;


	void OnTriggerEnter2D( Collider2D other )
	{
		if( other.CompareTag("player") )
		{
			ScoreManager.AddScore( scoreForBonus );
			EffectsManager.PlayScoreEffect( transf.position, scoreForBonus );
			ActiveBonusManager.ActivateBonus( bonuses[ curBonusIndex ].bonusType );
			SoundManager.PlayBonusPickSound();
			obj.SetActive( false );
		}
	}

	override public float getObjWidth
	{
		get { return SPAWN_OFFSET * 2f; }
	}

	override public Vector2 getXOccupied
	{
		get { return new Vector2( transf.position.x - SPAWN_OFFSET, transf.position.x + SPAWN_OFFSET ); }
	}

	override public void Spawn( float upBorder, float botBorder )
	{
		Spawn( upBorder, botBorder, -GlobalManager.DEF_HALF_SCREEN_WIDTH, GlobalManager.DEF_HALF_SCREEN_WIDTH );
	}

	override public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		bonuses[ curBonusIndex ].obj.SetActive( false );
		curBonusIndex = Random.Range( 0, bonuses.Length );
		bonuses[ curBonusIndex ].obj.SetActive( true );
		Vector3 spawnPos = Vector3.zero;
		spawnPos.x = Random.Range( minXPos + SPAWN_OFFSET,  maxXPos - SPAWN_OFFSET );
		spawnPos.y = Random.Range( upBorder - SPAWN_OFFSET, botBorder + SPAWN_OFFSET );
		spawnPos.z = zPos;
		transf.position = spawnPos;
		obj.SetActive( true );
	}

}
