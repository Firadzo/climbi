﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[System.Serializable]
public class LanguageParams
{
	public SystemLanguage	language;
	public string			scoreTxt;
	public string			youScoredTxt;
	public string			recordText;
}

public class ScoreManager : MonoBehaviour {

	public	Text		scoreText;
	public	float		scoreForHeightRatio; 	
	public 	string 		recordKey;
	public	GameObject	newRecordObj;
	public	TextMesh	scoreForGameText;
	public	TextMesh	recordText;
	public	LanguageParams[]	languageParams;
	public	SystemLanguage		defaultLanguage;

	private  static LanguageParams		selectedLanguage;


	private static ScoreManager instance;
	private static int 			curScore;
	private static int			tmpInt;
	private static int			curRecord;

	const string COLON 		= ": ";

	public static int getRecord
	{
		get{ return curRecord; }
	}

	void Awake () {
		instance = this;
		SetTextVariables();
		SetScoreText();
		curRecord = PlayerPrefs.GetInt( recordKey, 0 );
	}

	private void SetTextVariables()
	{
#if UNITY_EDITOR

		SystemLanguage sysLang = GetComponent<LanguageManager>().sysLanguage;
		if( sysLang != defaultLanguage )
		{
			for( int i = 0; i < languageParams.Length; i++ )
			{
				if( languageParams[ i ].language == sysLang )
				{
					selectedLanguage = languageParams[ i ];
					return;
				}
			}
		}

#else
		if( Application.systemLanguage != defaultLanguage )
		{
			for( int i = 0; i < languageParams.Length; i++ )
			{
				if( languageParams[ i ].language == Application.systemLanguage )
				{
					selectedLanguage = languageParams[ i ];
					return;
				}
			}
		}
#endif

		for( int i = 0; i < languageParams.Length; i++ )
		{
			if( languageParams[ i ].language == defaultLanguage )
			{
				selectedLanguage = languageParams[ i ];
				return;
			}
		}
	}

	public static void AddScore( int score )
	{
		curScore += score;
		instance.SetScoreText();
	}

	public static void AddScore( float height )
	{
		tmpInt = Mathf.RoundToInt( height * instance.scoreForHeightRatio );
		if( tmpInt > 0 )
		{
			EffectsManager.PlayScoreEffect( GlobalManager.GetPlayerPos, tmpInt );
			AddScore( tmpInt );
		}
	}

	public static void GameOver()
	{
		if( curScore > curRecord )
		{
			curRecord = curScore;
			PlayerPrefs.SetInt( instance.recordKey, curRecord );
			instance.newRecordObj.SetActive( true );
		}
		else
		{
			instance.newRecordObj.SetActive( false );
		}
		instance.scoreForGameText.text = selectedLanguage.youScoredTxt + COLON + curScore;
		instance.recordText.text	=	selectedLanguage.recordText + COLON + curRecord;

	}

	public static void ResetScore()
	{
		curScore = 0;
		instance.SetScoreText();
	}

	private void SetScoreText()
	{
		scoreText.text = selectedLanguage.scoreTxt + COLON + curScore.ToString();
	}

}
