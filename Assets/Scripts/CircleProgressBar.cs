﻿using UnityEngine;
using System.Collections;

public class ColorAnimDat
{
	public float valueFrom;
	public float valueTo;
	public int[] pointsIndexes;

	public ColorAnimDat( float _valueFrom, float _valueTo, int[] _pointsIndexes )
	{
		valueFrom = _valueFrom;
		valueTo = _valueTo;
		pointsIndexes = _pointsIndexes;
	}
}

public class CircleProgressBar : MonoBehaviour {

	public 	Color		circleColor;
	private MeshFilter	meshFilt;
	private CTimer		timer;
	private Color[]		meshColors;
	private Color		tempColor;

	private static ColorAnimDat[]	colorAnimDat;	
	const int POINTS_COUNT = 35;

	void Awake () {
		meshFilt = GetComponent<MeshFilter>();
		timer = new CTimer();
		if( colorAnimDat == null || colorAnimDat.Length == 0 )
		{
			colorAnimDat = new ColorAnimDat[ POINTS_COUNT ];
			int verNumber = 3;
			for( int i = 0; i < POINTS_COUNT; i++ )
			{
				if( i == 0 )
				{
					colorAnimDat[0] = new ColorAnimDat( 0, (float)i/POINTS_COUNT, new int[4]{ 0,1,2,3 }  );
				}
				else
				{
					verNumber += 2;
					colorAnimDat[ i ] = new ColorAnimDat( ( i - 1f )/ POINTS_COUNT, (float)i/POINTS_COUNT, new int[2]{ verNumber, verNumber - 1 }  );
				}
			}
		}
	}

	public void Animate( float time )
	{
		timer.Start( time );
	}

	public bool animate;
	// Update is called once per frame
	void Update () {
		if( animate )
		{
			animate = false;
			Animate( 10f );
		}
		if( GlobalManager.isGameActive )
		{
			if( timer.active )
			{
				meshColors = meshFilt.mesh.colors;
				timer.Update( Time.deltaTime );
				for( int i = 0; i < colorAnimDat.Length; i++ )
				{
					tempColor = circleColor;
					tempColor.a = Mathf.InverseLerp( colorAnimDat[ i ].valueTo, colorAnimDat[ i ].valueFrom, timer.w ) * circleColor.a;
					for( int j = 0; j < colorAnimDat[ i ].pointsIndexes.Length; j++ )
					{
						meshColors[ colorAnimDat[ i ].pointsIndexes[ j ] ] = tempColor;
					}
				}
				meshFilt.mesh.colors = meshColors;
			}
		}
	}
}
