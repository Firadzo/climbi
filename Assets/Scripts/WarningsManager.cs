﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class WarningInfo
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public GameObject 		prefab;
	public int 				precreatedCount;
	public WarningsType		warningType;
}
public enum WarningsType{ fallingStone }
public class WarningsManager : MonoBehaviour {

	public WarningInfo[] warningsInfo;

	private static Dictionary< WarningsType , List<WarningBase> > 	warnings;
	
	// Use this for initialization
	void Awake () {
		warnings = new Dictionary< WarningsType, List<WarningBase> >();
		Transform transf;
		Transform thisTransf = transform;
		for( int i = 0; i < warningsInfo.Length; i++ )
		{
			if( !warnings.ContainsKey( warningsInfo[ i ].warningType ) )
			{
				warnings.Add( warningsInfo[ i ].warningType, new List<WarningBase>() );
			}
			for( int j = 0; j < warningsInfo[ i ].precreatedCount; j++ )
			{
				transf = ( Instantiate( warningsInfo[ i ].prefab ) as GameObject ).transform;
				transf.parent = thisTransf;
				warnings[ warningsInfo[ i ].warningType ].Add( transf.GetComponent<WarningBase>() );
			}
		}
	}

	public static void ResetWarnings()
	{
		foreach( WarningsType warnT in warnings.Keys )
		{
			for( int i = 0; i < warnings[ warnT ].Count; i++ )
			{
				if( warnings[ warnT ][ i ].isActive )
				{
					warnings[ warnT ][ i ].DisableWarning();
				}
			}
		}
	}

	public static void PauseWarnings()
	{
		foreach( WarningsType warnT in warnings.Keys )
		{
			for( int i = 0; i < warnings[ warnT ].Count; i++ )
			{
				if( warnings[ warnT ][ i ].isActive )
				{
					warnings[ warnT ][ i ].OnPause();
				}
			}
		}
	}

	public static void ResumeWarnings()
	{
		foreach( WarningsType warnT in warnings.Keys )
		{
			for( int i = 0; i < warnings[ warnT ].Count; i++ )
			{
				if( warnings[ warnT ][ i ].isActive )
				{
					warnings[ warnT ][ i ].OnResume();
				}
			}
		}
	}

	public static void ShowWarningOfType( WarningsType warnType )
	{
		WarningBase warning = GetWarningOfType( warnType );
		warning.Show();
	}

	public static void ShowWarningOfType( WarningsType warnType, float warnTime )
	{
		WarningBase warning = GetWarningOfType( warnType );
		warning.Show( warnTime );
	}

	public static void ShowWarningOfType( WarningsType warnType, float warnTime, WarningBase.AfteWarningCB callBack )
	{
		WarningBase warning = GetWarningOfType( warnType );
		warning.Show( warnTime, callBack );
	}

	private static WarningBase GetWarningOfType( WarningsType warnType )
	{
		if( warnings.ContainsKey( warnType ) )
		{
			for( int i = 0; i < warnings[ warnType ].Count; i++ )
			{
				if( !warnings[ warnType ][ i ].gameObject.activeSelf )
				{
					return warnings[ warnType ][ i ];
				}
			}
		}
		return null;
	}

	#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		for( int i = 0; i< warningsInfo.Length; i++ )
		{
			warningsInfo[ i ].name = warningsInfo[ i ].warningType.ToString();
		}
	}
	#endif
}
