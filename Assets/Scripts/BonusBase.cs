using UnityEngine;
using System.Collections;

public class BonusBase : ObjBase, ISpawnable {

	public 		float		disableHeight;
	protected 	static float zPos;

	virtual public float getObjWidth
	{
		get { return 0; }
	}

	virtual public Vector2 getXOccupied
	{
		get { return Vector2.zero; }
	}

	virtual public bool occupyAllLine
	{
		get { return false; }
	}

	override protected void Awake()
	{
		base.Awake();
		zPos = GlobalManager.GetZParams( ObjectType.bonus );
		obj.SetActive( false );
	}

	virtual public void PrepareForSpawn()
	{

	}

	virtual public void Spawn()
	{

	}

	virtual public void Spawn( float upBorder, float botBorder )
	{
		
	}

	virtual public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos  )
	{
		
	}


	virtual protected void Update()
	{
		if( transf.position.y + disableHeight <= GlobalManager.CameraHeight )
		{
			ResetObject();
		}
	}
}
