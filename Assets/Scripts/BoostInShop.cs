﻿using UnityEngine;
using System.Collections;

public class BoostInShop : ItemInShopBase {
	public 	EBoostType					boostType;

	override public void GetItemState()
	{
		bought = ActiveBonusManager.GetBoostState( boostType );
		//priceText.text = bought ? "Bought " : price.ToString();
		priceObj.SetActive( !bought );
		boughtObj.SetActive( bought );
	}

	override protected void Buying()
	{
		base.Buying();
		ActiveBonusManager.ActivateBoost( boostType );
	}

}
