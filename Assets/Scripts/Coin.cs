﻿using UnityEngine;
using System.Collections;

public class Coin : ObjBase {
	
	public 	InGameEffect	starts;
	public 	int 			scoreForCoin;
	private bool 			magneted = false;
	private CoinsBonus		coinsHolder;
	private	Animator 		_animator;

	public const float 	HALF_COIN_SIZE = 35f;
	const 		 float	MAGNETED_SPEED = 1500f;

	

	override protected void Awake()
	{
		base.Awake ();
		transf.parent = BonusManager.transf;
		_animator = GetComponentInChildren<Animator> ();
		obj.SetActive( false );
	}

	public void SpawnAtPos( Vector3 pos, CoinsBonus coinsBonus )
	{
		_animator.enabled = true;
		magneted = false;
		coinsHolder = coinsBonus;
		transf.position = pos;
		obj.SetActive( true );
	}

	void OnTriggerEnter2D( Collider2D other )
	{
		if( other.CompareTag("player") )
		{
			if( coinsHolder != null )
			{
				coinsHolder.RemoveCoin( this );
				coinsHolder = null;
			}
			ScoreManager.AddScore( scoreForCoin );
			EffectsManager.PlayScoreEffect( transf.position, scoreForCoin );
			EffectsManager.PlayEffectAtPos( starts.getEffectId, transf.position );
			CoinsManager.AddCoin();
			obj.SetActive( false );
		}
	}

	
	override public void OnPause()
	{
		if (_animator != null) {
			_animator.enabled = false;
		}
	}
	
	override public void OnResume()
	{
		if (_animator != null) {
			_animator.enabled = true;
		}
	}

	private Vector3 moveDir;
	void InMagneticField()
	{
		magneted = true;
	}

	void Update()
	{
		if( GlobalManager.isGameActive )
		{
			if( magneted )
			{
				moveDir = GlobalManager.GetPlayerPos - transf.position;
				transf.position += moveDir.normalized * MAGNETED_SPEED * Time.deltaTime;
			}
		}
	}
}
