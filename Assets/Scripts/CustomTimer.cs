using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CTimer
{
	public		bool active;
	public		float time;
	protected	float curTime;
	public		float w;

	public CTimer()
	{
		Stop();
	}

	public float getCurTime
	{
		get{ return curTime; }
	}

	public CTimer( float set_time, bool need_start )
	{
		if ( need_start )
		{
			Start( set_time );
		}
		else
		{			
			time = set_time;
			Stop();
		}
	}
	
	public CTimer( float set_time )
	{
		Start( set_time );
	}
	
	public virtual void Start( float set_time )
	{
		active	= true;
		time	= set_time;
		curTime	= set_time;
		w		= 0.0f;
	}
	
	public virtual void Start()
	{
		active = time >= 0.0;
		if ( active )
		{
			curTime	= time;
			w = 0.0f;
		}
	}
	
	public virtual void Stop()
	{
		active	= false;
		curTime	= -1.0f;
	}
	
	public virtual bool Update( float dt )
	{
		if ( active )
		{
			curTime -= dt;
			w = 1.0f - ( curTime / time );
			
			if ( curTime < 0.0f )
			{
				w = 1.0f;
				active = false;
				return true;
			}
		}
		
		return false;
	}
}

public class CLerpTimer : CTimer
{
	public	float a;
	public	float b;
	public	float val;
	
	public CLerpTimer( float set_a, float set_b, float set_time, bool need_start ) : base( set_time, need_start )
	{
		a = set_a;
		b = set_b;
	}
	
	public CLerpTimer( float set_a, float set_b, float set_time ) : base( set_time )
	{
		a = set_a;
		b = set_b;
	}
	
	public override void Start( float set_time )
	{
		base.Start(set_time);
		val = a;
	}
	
	public override void Start()
	{
		base.Start();
		val = a;
	}
	
	public override void Stop()
	{
		base.Stop();		
	}
	
	public override bool Update( float dt )
	{		
		if ( active )
		{
			curTime -= dt;
			// timer goes from 1.0 to 0.0, but lerp needs from 0.0 to 1.0
			w = 1.0f - ( curTime / time );
			val = Mathf.Lerp( a, b, w );
			
			if ( curTime < 0.0 )
			{
				val = b;
				w = 1.0f;
				active = false;
				return true;
			}
		}
		
		return false;
	}
}