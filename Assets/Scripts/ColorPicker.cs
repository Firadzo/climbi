﻿using UnityEngine;
using System.Collections;

public class ColorPicker : MonoBehaviour {
# if UNITY_EDITOR
	public 	Color			baseColor;
	public	Color			rendererColor;
	public 	bool				setColor;
	private SpriteRenderer	sprite_Renderer;
	

	void OnDrawGizmosSelected()
	{
		if( setColor )
		{
			if( sprite_Renderer == null )
			{
				sprite_Renderer = GetComponent<SpriteRenderer>();
			}
			setColor = false;
			sprite_Renderer.color = baseColor * rendererColor;
		}
	}
#endif
}
