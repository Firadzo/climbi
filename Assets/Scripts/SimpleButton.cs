﻿using UnityEngine;
using System.Collections;

public class SimpleButton : ButtonBase {

	public	Transform		text;
	public 	Sprite			up;
	public 	Sprite			down;
	public	bool 			animate = true;
	public 	GameObject		reciever;
	public 	string			sendingMessage;
	private SpriteRenderer	spriteRenderer;
	private bool			clicked;
	private Vector3			textUpPos;
	private	Vector3			textDownPos;

	const 	float 			TEXT_OFFSET	= -2f;

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		if( animate )
		{
			textUpPos = text.localPosition;
			textDownPos = textUpPos;
			textDownPos.y += TEXT_OFFSET;
		}
		if( reciever == null )
		{
			reciever = gameObject;
		}
	}

	protected override void OnButtonDown ()
	{
		base.OnButtonDown();
		if( animate )
		{
			spriteRenderer.sprite = down;
			text.localPosition = textDownPos;
		}
		clicked = true;
	}

	protected override void OnButtonUp ()
	{
		if( animate )
		{
			spriteRenderer.sprite = up;
			text.localPosition = textUpPos;
		}
		clicked = false;
		reciever.SendMessage( sendingMessage );
	}

	void Update()
	{
		if( clicked )
		{
			if( !GlobalManager.isMouseClicked )
			{
				clicked = false;
				if( animate )
				{
					spriteRenderer.sprite = up;
				}
			}
		}
	}
}
