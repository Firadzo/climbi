﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SwitchParams
{
#if UNITY_EDITOR
	[HideInInspector]
	public string name;
#endif
	public Sprite			sprite;
	public string 			sendingMessage;
}

public class SwitchButton : ButtonBase {

	private SpriteRenderer	spriteRenderer;
	private bool 			clicked;
	public SwitchParams		normalParams;
	public SwitchParams		clickedParams;
	public GameObject		objReceiver;
	public bool				workOnlyInGame;
	// Use this for initialization
	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	protected override void OnButtonDown ()
	{
		//base.OnButtonDown();
		if( workOnlyInGame && !GlobalManager.isGameStarted )
		{
			return;
		}
		clicked = !clicked;
		spriteRenderer.sprite = clicked ? clickedParams.sprite : normalParams.sprite;
		objReceiver.SendMessage( clicked ? clickedParams.sendingMessage : normalParams.sendingMessage );
	}

	public void SetClicked( bool _clicked )
	{
		clicked = _clicked;
		spriteRenderer.sprite = clicked ? clickedParams.sprite : normalParams.sprite;
	}

/*#if UNITY_EDITOR
	void OnDrawGizmosSelected()
	{
		for( int i = 0; i < switchParams.Length; i++ )
		{
			switchParams[ i ].name = switchParams[ i ].sendingMessage;
		}
	}
#endif*/

}
