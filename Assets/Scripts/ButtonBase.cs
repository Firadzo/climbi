﻿using UnityEngine;
using System.Collections;

public class ButtonBase : MonoBehaviour {

	virtual protected void OnButtonDown()
	{
		SoundManager.PlayClickSound();
	}

	virtual protected void OnButtonUp()
	{
		
	}
}
