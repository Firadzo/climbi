﻿using UnityEngine;
using System.Collections;

public class SpeedUpBonus : BonusBase {

	public	Vector2		size;
	public 	float 		power;
	public 	Vector2 	forceVector = new Vector2( 0, 1f );
	public 	float		reUseTime	= 1f;

	protected 	float					halfWidth;	
	protected 	CTimer 					reUseTimer;
	protected 	ParticleSystem 			_particles;
	protected 	BoxCollider2D			_collider;
	//protected	AudioSource				audioSource;

	protected float getHalfWidth
	{
		get{
			if( halfWidth == 0 )
			{
				halfWidth = size.x / 2f;
			}
			return halfWidth;
		}
	}

	override public float getObjWidth
	{
		get { return size.x; }
	}
	
	override public Vector2 getXOccupied
	{
		get { return new Vector2( transf.position.x - getHalfWidth, transf.position.x + getHalfWidth ); }
	}

	protected ParticleSystem getParticles
	{
		get{
			if( _particles == null )
			{
				_particles = GetComponentInChildren<ParticleSystem>();
			}
			return _particles;
		}
	}

	override protected void Awake()
	{
		base.Awake();
		reUseTimer = new CTimer( reUseTime , false );
		_collider = GetComponent<BoxCollider2D>();
		//audioSource = GetComponent< AudioSource >();
		//SoundManager.RegisterAudioSource( audioSource );
	}

	override public void ResetObject () {
		base.ResetObject();
		reUseTimer.Stop();
		_collider.enabled = true;
	}

	override public void Spawn( float upBorder, float botBorder, float minXPos, float maxXPos )
	{
		obj.SetActive( true );
		float angle = Random.Range( 0, 180f );
		getParticles.startColor = Color.Lerp( Color.green, Color.red, Mathf.InverseLerp( 0, 180f, angle ) );
		float forceAngle = angle;
		forceVector.y = 1;
		forceVector.x = -1f;
		if( angle > 90f )
		{
			forceAngle = 180f - angle;
			forceVector.y = -1f;
		}
		forceVector.x *= Mathf.InverseLerp( 0, 90f, forceAngle );
		forceVector.y *= Mathf.InverseLerp( 90f, 0, forceAngle );
		float dir = Random.value > 0.5f ? 1f : -1f;
		angle *= dir;
		forceVector.x *= dir;
		Vector3 spawnPos = transf.localEulerAngles;
		spawnPos.z = angle;
		transf.localEulerAngles = spawnPos;
		getParticles.startRotation = -angle * Mathf.PI/180f;
//		Debug.Log( angle+"  "+getParticles.startRotation );
		spawnPos.x = Random.Range( minXPos + getHalfWidth,  maxXPos - getHalfWidth );
		spawnPos.y = Random.Range( botBorder + size.y / 2f, upBorder - size.y / 2f );
		spawnPos.z = zPos;
		transf.position = spawnPos;

	}

	void OnTriggerEnter2D( Collider2D other )
	{
		if( other.CompareTag("player") )
		{
		//	audioSource.Play();
			PlayerOnSpringJoint.GetRigidBody.AddForce( forceVector * power, ForceMode2D.Impulse );
			_collider.enabled = false;
			reUseTimer.Start();
		}
	}

	override protected void Update()
	{
		base.Update();
		if( GlobalManager.isGameActive )
		{
			if( reUseTimer.Update( Time.deltaTime ) )
			{
				_collider.enabled = true;
			}
		}
	}
}
