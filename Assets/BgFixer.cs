﻿using UnityEngine;
using System.Collections;

public class BgFixer : MonoBehaviour {

#if UNITY_EDITOR
	public Transform[]	transforms;
	public bool fix;

	void OnDrawGizmosSelected()
	{
		if( fix )
		{
			fix = false;
			Transform tr = GameObject.Find("Bg").transform;
			tr.name += "  ";
			tr.localScale += new Vector3( 2, 2, 0 );
			Debug.Log( tr );
		}
	}
#endif
}
