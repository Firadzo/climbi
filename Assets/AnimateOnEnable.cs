﻿using UnityEngine;
using System.Collections;

public class AnimateOnEnable : MonoBehaviour {
	public string stateName;
	public string onDisableState;

	private Animator _animator;

	void OnEnable()
	{
		if( _animator == null )
		{
			_animator = GetComponent<Animator>();
		}
		_animator.Play( stateName ,0 ,0 );
	}

	void OnDisable()
	{
		if( _animator == null )
		{
			_animator = GetComponent<Animator>();
		}
		
		_animator.Play( onDisableState ,0 ,0 );
	}

}
