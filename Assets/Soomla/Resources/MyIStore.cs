using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla.Store;

public class MyIStore : IStoreAssets {

		/// <summary>
		/// see parent.
		/// </summary>
		public int GetVersion() {
			return 2;
		}
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualCurrency[] GetCurrencies() {
			return new VirtualCurrency[]{COINS };
		}
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualGood[] GetGoods() {
			return new VirtualGood[] {  };
		}
		
		public VirtualCurrencyPack[] GetCurrencyPacks()
		{
			return new VirtualCurrencyPack[] {  };
		}
			
		
		/// <summary>
		/// see parent.
		/// </summary>
		public VirtualCategory[] GetCategories() {
		return new VirtualCategory[]{};
		}
		
//		public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory(
//		"General", new List<string>( new string[] { MIG, DISABLE_ADS } )
//		);
		/** Static Final Members **/
	#if UNITY_ANDROID || UNITY_EDITOR 
		//public const string MIG     				= "plane_mig";
		//public const string DISABLE_ADS     		= "remove_ads";

//	#elif UNITY_IPHONE
//		public const string FIVE_THOUSANDS_COINS    = "COINS__5000";
//		public const string TWELVE_THOUSANDS_COINS  = "COINS_12000";
//		public const string MIG     				= "plane_mig";
//		public const string DISABLE_ADS     		= "REMOVE_ADS";
//
//		public VirtualCurrencyPack[] GetCurrencyPacks() {
//			return new VirtualCurrencyPack[] {FIVE_THOUSANDS_COINS_PACK, TWELVE_THOUSANDS_COINS_PACK };
//		}
//	#elif UNITY_WP8
//		public const string FIVE_THOUSANDS_COINS    = "coin_1000";
//		public const string SEVEN_THOUSANDS_COINS   = "coins_7000";
//		public const string TEN_THOUSANDS_COINS     = "coins_10000";
//		public const string MIG     				= "plane_mig";
//		public const string DISABLE_ADS     		= "remove_ads";
	#endif
		public const string	COINS_ID = "coins";
//		public const string	MIG_ITEM = "mig";
//		public const string	NO_ADS_ITEM = "no_ads";

		/** Virtual Currencies **/
		
		public static VirtualCurrency COINS = new VirtualCurrency(
			"Coins",										// name
			"",												// description
			COINS_ID							// item id
			);
		/** LifeTimeVGs **/
		// Note: create non-consumable items using LifeTimeVG with PuchaseType of PurchaseWithMarket
//		public static LifetimeVG NO_ADS_GOOD = new LifetimeVG(
//			"No Ads", 														// name
//			"No More Ads!",				 									// description
//			NO_ADS_ITEM,													// item id
//		new PurchaseWithVirtualItem (DISABLE_ADS, 0.99));	// the way this virtual good is purchased
//
//		public static VirtualGood MIG_PLANE_GOOD = new LifetimeVG(
//		"Mig plane", 														// name
//		"Buy MIG!",				 									// description
//		MIG_ITEM,													// item id
//		new PurchaseWithVirtualItem ( MIG, 0.99));	// the way this virtual good is purchased
}
