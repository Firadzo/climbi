﻿using UnityEngine;
using System.Collections;

public class FPSCounter : MonoBehaviour {

	private TextMesh 	textMesh;
	public 	float		frequency;//частота обновления в секундах
	private float		timer;
	private int			frameCount = 0;
	// Use this for initialization
	void Awake () {
		textMesh = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		frameCount ++;
		if( timer >= frequency )
		{
			timer = 0;
			textMesh.text = Mathf.RoundToInt( frameCount/ frequency ) +"";
			frameCount = 0;
		}
	}
}
