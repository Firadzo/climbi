﻿using UnityEngine;
using System.Collections;

public class MyTapjoy : MonoBehaviour {

	private bool connected;

	public static bool checkEarnings;

	void Awake ()
	{
		#if UNITY_ANDROID
		// Attach our thread to the java vm; obviously the main thread is already attached but this is good practice..
		if (Application.platform == RuntimePlatform.Android)
			UnityEngine.AndroidJNI.AttachCurrentThread();
		#endif
		
		// Connect to the Tapjoy servers.
		if (Application.platform == RuntimePlatform.Android)
		{
			TapjoyPlugin.RequestTapjoyConnect("0630f2a1-cc5d-4a81-a16e-4dd0460de5af",			// YOUR APP ID GOES HERE
			                                  "hByohYYGVT1zLueRjZu7");							// YOUR SECRET KEY GOES HERE
			TapjoyPlugin.connectCallSucceeded += () => { connected = true; TapjoyPlugin.GetTapPoints(); };
			TapjoyPlugin.connectCallFailed += () => { connected = false; };
			TapjoyPlugin.viewClosed += 
			(TapjoyViewType obj) => 
			{ 
				if( obj == TapjoyViewType.OFFERWALL || obj == TapjoyViewType.VIDEO_AD ) 
				{
					QueryPoints();
				} 
			};
			TapjoyPlugin.getTapPointsSucceeded += 
				(int obj) => 
				{
					if( obj != 0 )
					{
						CoinsManager.SpendCoins( -obj );
						TapjoyPlugin.SpendTapPoints( obj );
					}
				};
		}
	}


	void QueryPoints()
	{
		if( connected )
		{
			coins = TapjoyPlugin.QueryTapPoints();
			if( coins != 0 )
			{
				TapjoyPlugin.SpendTapPoints( coins );
				CoinsManager.SpendCoins( -coins );
			}
		}
		else
		{
			TapjoyPlugin.RequestTapjoyConnect( "0630f2a1-cc5d-4a81-a16e-4dd0460de5af", "hByohYYGVT1zLueRjZu7" );
		}
	}

	public void ShowOffers()
	{
		TapjoyPlugin.ShowOffers();
	}

	const float DELAY = 1f;
	float timer;
	int coins;
	void Update()
	{
		if( checkEarnings )
		{
			timer += Time.deltaTime;
			if( timer >= DELAY )
			{
				timer = 0;
				QueryPoints();
			}
		}
	}
}
