﻿using UnityEngine;
using System.Collections;


public class SetVertexColor : MonoBehaviour {

	public Color color;

	private Mesh  mesh;

	void Awake()
	{
		mesh = GetComponent<MeshFilter> ().mesh;
	}

	public void SetColor()
	{
		Color[] colors = new Color[ mesh.vertices.Length ];
		for( int i = 0; i < colors.Length; i++ )
		{
			colors[ i ] = color;
			Debug.Log( colors[ i ] );
		}
		mesh.colors = colors;
	}

#if UNITY_EDITOR
	public bool setColor;
	void OnDrawGizmosSelected()
	{
		if (setColor) {
		
			setColor = false;
			if( mesh == null )
			{
				mesh = GetComponent<MeshFilter> ().mesh;
			}
			SetColor();
		}
	}

#endif
}


